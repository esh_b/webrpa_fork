from accounts.models import User
from django.db import models


class Payment(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    pay_method = models.CharField(max_length=1, blank=True)
    pay_card_token = models.CharField(max_length=32, blank=True)
    pay_company_name = models.CharField(max_length=30, blank=True)
    pay_post_code = models.CharField(max_length=10, blank=True)
    pay_company_address = models.CharField(max_length=50, blank=True)
    pay_date = models.DateTimeField()
    pay_status = models.IntegerField(null=True)
    pay_error = models.IntegerField(null=True)


class SubscriptionPlan(models.Model):
    plan_type = models.CharField(max_length=32, blank=True)
    plan_period = models.IntegerField(null=True)
    plan_datalimit = models.IntegerField(null=True)
    plan_number_project = models.IntegerField(null=True)


class UserSubscription(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    plan = models.ForeignKey(SubscriptionPlan, on_delete=models.PROTECT)
    payment = models.ForeignKey(Payment, on_delete=models.PROTECT)
