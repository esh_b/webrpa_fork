from django import forms
from .models import Payment, SubscriptionPlan

class PaymentForm(forms.ModelForm):
    PAY_METHOD_CHOICE = ((1, 'クレジットカード払い'), (2, '銀行振込(請求書払い)'))

    pay_method          = forms.ChoiceField(required=False, widget=forms.RadioSelect(attrs={'class': 'choice'}), choices= PAY_METHOD_CHOICE)
    pay_card_token      = forms.CharField(required=False, max_length=32, widget=forms.HiddenInput(attrs={'class': 'form-control', 'placeholder': "Credit Number"}))
    # pay_card_password   = forms.CharField(required=False, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    pay_company_name    = forms.CharField(required=False, max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Company"}))
    pay_post_code       = forms.CharField(required=False, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "1234567", 'pattern': '\d{3}\d{4}'}))
    pay_tel_number      = forms.CharField(required=False, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "0312345678", 'pattern': '\d{2,4}\d{3,4}\d{3,4}'}))
    pay_company_address = forms.CharField(required=False, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Tokyo"}))

    class Meta:
        model = Payment
        # fields = ('pay_method', 'pay_card_token', 'pay_card_password', 'pay_company_name', 'pay_post_code', 'pay_tel_number', 'pay_company_address')
        fields = ('pay_method', 'pay_card_token', 'pay_company_name', 'pay_post_code', 'pay_tel_number', 'pay_company_address')

class SubscriptionPlanForm(forms.ModelForm):
    PLAN_TYPE_CHOICE = ((1, 'Freeプラン'), (2, 'Businessプラン'))

    plan_type = forms.ChoiceField(widget=forms.RadioSelect(attrs={'class': 'choice'}), choices= PLAN_TYPE_CHOICE)
    class Meta:
        model = SubscriptionPlan
        fields = ('plan_type',)

