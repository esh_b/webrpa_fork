from django import forms

class StartForm(forms.Form):
    start_url = forms.CharField(max_length=2000, widget=forms.TextInput(attrs={'class': 'form-control', 'id':'start_url', 'placeholder': "http://example.com"}), required=True)
    is_login = forms.BooleanField(required=False, label="check this if login required")
