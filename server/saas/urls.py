from . import views as core_views
from django.conf.urls import url

app_name = 'saas'

urlpatterns = [
    url(r'^saas/start/', core_views.start, name='start'),
    url(r'^saas/getLogin', core_views.get_login, name='get_login'),
    url(r'^saas/content', core_views.url_content_sel, name='url_content_sel'),
    url(r'^saas/procedure', core_views.get_procedure, name='get_procedure'),
    url(r'^saas/', core_views.process, name='process'),
    url(r'^pigdata-saas/', core_views.pigdata, name='pigdata'),
]
