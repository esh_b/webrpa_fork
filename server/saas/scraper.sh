#!/bin/sh
stripped_url=`echo $1`
target_folder=`echo $2`
mkdir -p $target_folder
wget -O $target_folder`echo /index.html` -e robots=off -H -nd -nc -np --level=0 --accept jpg,jpeg,png,gif --convert-links -N --limit-rate=200k --wait 1.0 $stripped_url
