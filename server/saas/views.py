import json
import os
import codecs
import sys
import subprocess

from .forms import StartForm
from .xpath import Extraction
from accounts.models import User
from api_v1.scraping.views import saas_save_project
from bs4 import BeautifulSoup as B
from django.conf import settings
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from internal.rpalib.scraping import Project, ActionInfo
from internal.rpalib.internal import as_jsonable
from internal.rpalib.core import Session
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from urllib.parse import urlparse
import requests
import random
from lxml.html import fromstring

download_folder = settings.SCRAPING_DOWNLOAD_DIR

@csrf_exempt
def pigdata(request):
    # form = StartForm()
    # return render(request, 'start.html', {'form': form})
    return render(request, 'pigdata-saas.html')

@csrf_exempt
def start(request):
    # form = StartForm()
    # return render(request, 'start.html')
    return render(request, 'login-index.html')

@csrf_exempt
def process(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        # form = StartForm(request.POST)
        # # check whether it's valid:
        # if form.is_valid():
        start_url = request.POST.get('start_url')

        if request.POST.get('is_login'):
            print('login-flag: set')
            login_url = request.POST['login_url']
            print('start url:' + start_url + 'login url :' + login_url + ' user id: ' + str(request.user.id) + '\n')
            request.session['login_url'] = login_url
            return render(request, 'pigdata-saas.html', {'start_url': start_url, 'login_url': login_url})
        else:
            print('login-flag: unset')
            print('start url:' + start_url + ' user id: ' + str(request.user.id) + '\n')
            return render(request, 'login-index.html', {'start_url': start_url})

    return HttpResponseRedirect('/saas/start/')

def get_proxies():
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = []
    for i in parser.xpath('//tbody/tr')[:10]:
        if i.xpath('.//td[7][contains(text(),"yes")]'):
            proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            proxies.append(proxy)
    return proxies

def get_browser():
    options = Options()
    options.add_argument("--headless")
    # options.add_argument('--no-sandbox')  # # Bypass OS security model
    # options.add_argument('--disable-gpu')  # applicable to windows os only
    options.add_argument('start-maximized')
    options.add_argument('disable-infobars')
    options.add_argument("--disable-extensions")
    options.add_argument("--window-size=1920x1080")
    # proxy = random.choice(get_proxies())
    # proxy = '194.44.34.6:41833'
    # print(proxy)
    # options.add_argument("--proxy-server=" + proxy)

    # prefs = {'profile.managed_default_content_settings.images': 2, 'disk-cache-size': 4096}
    # options.add_experimental_option('prefs', prefs)
    chrome_driver = 'internal/chromedriver'

    browser = webdriver.Chrome(chrome_options=options, executable_path=chrome_driver)
    return browser

@csrf_exempt
def url_content_sel(request):
    if request.is_ajax() and request.method == 'POST':
        params = json.loads(request.body.decode())
        url = params['url']
        print('url request:' + url)
        uid = str(request.user.id)
        domainURL = '{uri.scheme}://{uri.netloc}/'.format(uri=urlparse(url))
        browser = get_browser()
        browser.get(url)
        soup = B(browser.page_source, 'html.parser')
        browser.quit()
        head = soup.find("head")
        base = soup.new_tag('base', href=domainURL)
        soup.head.insert(0, base)
        # meta = soup.new_tag('meta', charset='utf-8')
        # soup.head.insert(1, meta)
        ajax = soup.new_tag('script', src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js")
        soup.head.append(ajax)
        inspector = soup.new_tag('script', type="text/javascript")
        with open('static/js/inspector.js', 'r') as myfile: 
            inspector.string = myfile.read()
            
        soup.head.append(inspector)

    return HttpResponse(json.dumps(soup.prettify()), content_type="application/json")

@csrf_exempt
def get_procedure(request):
    if request.is_ajax() and request.method == 'POST':
        params = json.loads(request.body.decode())
        project = Project.from_dict(params['procedure'])
        # project = params['procedure']
        uid = request.user.id
        user = User.objects.get(id=uid)
        params = {}
        project = as_jsonable(project)
        params['user'] = user
        params['project'] = project
        params['alert_type'] = [False, False, False]
        params['keyword'] = ''
        params['sched_type'] = '0'
        params['sched_val'] = '0'
        # session = requests.Session()
        # session.post('http://192.168.20.152:8000/api/v1/scraping/save_project', {'project': project})
        kwargs = {'procedure_file': params['project'], 'usr': params['user'], 'alert_type': params['alert_type'],
                  'keyword': params['keyword'], 'sched_type': params['sched_type'], 'sched_val': params['sched_val']}
        saas_save_project(**kwargs)

    return HttpResponseRedirect('/project/list/')

@csrf_exempt
def get_login(request):
    if request.is_ajax() and request.method == 'POST':
        params = json.loads(request.body.decode())
        # start_url = params['start_url']
        # login_info = params['login_info']
       
        json_var = {
            "product_url": {}, 
            "login_details": [], 
            "project_name": "", 
            "project_type": 1, 
            "start_url": "", 
            "page_list": []
        }
        json_var["start_url"] = params['start_url']
        json_var["login_details"] = params['login_info']
        try:    
            page = {
                "current_page_url": params['start_url'],
                "action_list": params["fill_click_list"],
                "num_pages": 2,
                "pagination": []
            }
            json_var["page_list"].append(page)
        except KeyError:
            pass
        # json_var["page_list"]["current_page_url"] = start_url
        # json_var["page_list"]["action_list"] = params['action_list']

        print(json_var)

        domainURL = '{uri.scheme}://{uri.netloc}/'.format(uri=urlparse(params['start_url']))
        html_data = Extraction().html_page_source(Project.from_dict(json_var), '', 0, params['flag'])
        soup = B(html_data, 'html.parser')
        head = soup.find("head")
        base = soup.new_tag('base', href=domainURL)
        soup.head.insert(0, base)
        # meta = soup.new_tag('meta', charset='utf-8')
        # soup.head.insert(1, meta)
        ajax = soup.new_tag('script', src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js")
        soup.head.append(ajax)
        inspector = soup.new_tag('script', type="text/javascript")
        with open('static/js/inspector.js', 'r') as myfile: 
            inspector.string = myfile.read()
        soup.head.append(inspector)

        file = open("index_saas.html", "w")
        file.write(soup.prettify())
        file.close()

        return HttpResponse(json.dumps(soup.prettify()), content_type="application/json")