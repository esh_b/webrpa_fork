from django.contrib.auth import login
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect
import os
import requests
from .models import User
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
import mimetypes
from wsgiref.util import FileWrapper
from .forms import UserForm, ProfileForm
from sales.views import PaymentForm, SubscriptionPlanForm
from email_sys.tokens import account_activation_token
from django.core.mail import send_mail
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from .models import Profile
from sales.models import SubscriptionPlan, UserSubscription
from datetime import datetime
from Crypto.PublicKey import RSA
from django.contrib.auth.views import login
from django.conf import settings

from django.db import transaction

from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth import update_session_auth_hash

def home(request):
    # return HttpResponseRedirect('https://services.sms-datatech.co.jp/pig-data/')
    return render(request, 'home.html')
def product(request):
    return render(request, 'product.html')
def learn(request):
    return render(request, 'learn.html')
def about(request):
    return render(request, 'about.html')
def contact(request):
    return render(request, 'contact.html')
def pricing(request):
    return render(request, 'pricing.html')

# global views
def global_home(request):
    return render(request, 'global-home.html')
def global_login(request):
    return render(request, 'global-login.html')
def global_contact(request):
    return render(request, 'contacts.html')

def get_client_ip(request: HttpRequest):
    """Function to get the client IP address
    
    Args:
        request (HttpRequest): Description
    
    Returns:
        TYPE: Description
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', None)
    if(x_forwarded_for):
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_user_timezone(request: HttpRequest):
    """Function to get the user timezone from IP address
    
    Args:
        request (HttpRequest): Description
    
    Returns:
        TYPE: Description
    """
    client_ip = get_client_ip(request)
    resp = requests.get('http://freegeoip.app/json/{0}'.format(client_ip))
    resp_json = resp.json()
    return resp_json['time_zone']

def global_signup(request):
    user_form    = UserForm()
    profile_form = ProfileForm()
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        profile_form = ProfileForm(request.POST)
        user_timezone = None

        try:
            user_timezone = get_user_timezone(request)
        except Exception as e:
            print("SIGNUP TIMEZONE ERROR:", e)

        if user_form.is_valid() and profile_form.is_valid() and user_timezone is not None:
            try:
                with transaction.atomic():
                    user = user_form.save(commit=False)
                    user.is_active = False
                    user.save()

                    profile = profile_form.save(commit=False)
                    profile.timezone = user_timezone
                    profile.user = user
                    profile.save()

                return render(request, 'account_activation_sent.html')
            except Exception as e:
                print(e)
                return render(request, 'error.html')
        else:
            print(user_form.errors)
            print(profile_form.errors)
            print("User timezone:", user_timezone)
            return render(request, 'global-sign-up.html', {'user_form': user_form, 'profile_form': profile_form})

    return render(request, 'global-sign-up.html', {'user_form': user_form, 'profile_form': profile_form})

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def logout_view(request, *args, **kwargs):
    auth_views.logout(request)
    request.session.flush()
    domain = get_current_site(request).domain
    #production
    if domain == "pig-data.sms-datatech.co.jp":
        return HttpResponseRedirect('https://services.sms-datatech.co.jp/pig-data/')
    #develop
    else:
        return HttpResponseRedirect('/login/')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def login(request, *args, **kwargs):
    return auth_views.login(request, 'login.html')

def signup(request):
    print( 'hello')
    print(request.body)
    user_form = UserForm()
    profile_form = ProfileForm()
    if request.method == 'POST' and 'signup' in request.POST:
        print('i ma here')
        user_form = UserForm(request.POST)
        if user_form.is_valid():
            try:
                #user
                user = user_form.save(commit=False)
                user.is_active = False
                user.save()
            except Exception as e:
                print(e)
                return render(request, 'error.html')

            profile_form = ProfileForm(request.POST)
            if profile_form.is_valid():
                try:
                    #profile
                    profile = profile_form.save(commit=False)
                    profile.user = user
                    key_pair = RSA.generate(1024)
                    profile.private_key = key_pair.exportKey()
                    profile.public_key = key_pair.publickey().exportKey()
                    profile.save()
                    return render(request, 'account_activation_sent.html')
                except Exception as e:
                    print(e)
                    return render(request, 'error.html')

            else:
                print(profile_form.errors)
                return render(request, 'signup.html', {'user_form': user_form, 'profile_form': profile_form})
        else:
            print(user_form.errors)
            return render(request, 'signup.html', {'user_form': user_form, 'profile_form': profile_form})
    elif request.method == 'POST' and 'edit' in request.POST:
        user_form    = UserForm(request.POST)
        profile_form = ProfileForm(request.POST)
    return render(request, 'signup.html', {'user_form': user_form, 'profile_form': profile_form})

def signupConfirm(request):
    if request.method == 'POST':
        user_form    = UserForm(request.POST)
        profile_form = ProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid() :
            return render(request, 'signup-confirm.html', {'user_form': user_form, 'profile_form': profile_form})
        else:
            return render(request, 'signup.html', {'user_form': user_form, 'profile_form': profile_form })

    return redirect('accounts:signup')

def account_activation_sent(request):
    return render(request, 'account_activation_sent.html')

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save(update_fields=['is_active'])

        #Send activation email
        """
        current_site = get_current_site(request)
        subject = '【PigData】本登録完了のお知らせ（株式会社 SMSデータテック）'
        message = render_to_string('account_activation_done_email.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': force_text(urlsafe_base64_encode(force_bytes(user.pk))),
            'token': account_activation_token.make_token(user),
        })
        send_mail(subject, message, 'no-reply@sms-datatech.co.jp', [user.email])
        """
        return redirect('accounts:account_activation')
    else:
        return render(request, 'account_activation_invalid.html')

def account_activation(request):
    return render(request, 'account_activation.html')

def error(request):
    return render(request, 'error.html')

def downloadtool(request: HttpRequest):
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
    	
    path = "tool/PigData_1.0.3.exe"
    #return HttpResponse(serve(request, os.path.basename(path), os.path.dirname(path)))

    file_path = os.path.join(os.path.dirname(BASE_DIR), 'server', 'tool', 'PigData_1.0.3.exe')
    file_wrapper = FileWrapper(open(file_path, 'rb'))
    file_mimetype = mimetypes.guess_type(file_path)
    response = HttpResponse(file_wrapper, content_type=file_mimetype)
    response['X-Sendfile'] = file_path
    response['Content-Length'] = os.stat(file_path).st_size
    response['Content-Disposition'] = 'attachment; filename="PigData.exe"'
    return response


def postal_json(request, value):
    file_name = '%i.json' % int(value)
    target_path = os.path.join("static", "assets", "json", file_name)
    return HttpResponse(open(target_path, 'r'), content_type='application/json; charset=utf8')

"""
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def social_signup_set_password(request: HttpRequest):
    if request.method == 'POST':
        form = SetPasswordForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()

            update_session_auth_hash(request, user)
            return redirect(settings.LOGIN_REDIRECT_URL)
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = SetPasswordForm(request.user)
    return render(request, 'social_signup_set_password.html', {
        'form': form
    })
"""