from . import views
from . import views as core_views
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.urls import path

app_name = 'accounts'

urlpatterns = [
    url(r'^$', core_views.home, name='home'),
    url(r'products/$', core_views.product, name='product'),
    url(r'learn/$', core_views.learn, name='learn'),
    url(r'about/$', core_views.about, name='about'),
    # url(r'contact/$', core_views.contact, name='contact'),
    #url(r'testing/(?P<pid>[^/]+)$', core_views.testing, name='testing'),
    url(r'pricing/$', core_views.pricing, name='pricing'),
    url(r'^login/', views.login, name='login'),
    #url(r'^$', auth_views.logout, {'next_page': 'home'}, name='logout'),
    url(r'logout/$', views.logout_view, name='logout'),
    url(r'^signup/$', core_views.signup, name='signup'),
    url(r'^signup-confirm/$', core_views.signupConfirm, name='signupConfirm'),
    url(r'^account_activation_sent/$', core_views.account_activation_sent, name='account_activation_sent'),
    url(r'activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        core_views.activate, name='activate'),
    url(r'^error/$', core_views.error, name='error'),
    url(r'downloadtool/$', views.downloadtool, name='downloadtool'),
    url(r'json/(?P<value>\d{3}).json', views.postal_json, name='postal_json_data'),


    url(r'account_activation/$', core_views.account_activation, name='account_activation'),
    # ---------------Password Reset------------------------------

    url(r'^password_reset/$', auth_views.password_reset,
        {'email_template_name': 'registration/password_reset_email.html',
         'subject_template_name': 'registration/password_reset_subject.txt',
         'post_reset_redirect': 'accounts:password_reset_done',
         'from_email': 'no-reply@sms-datatech.co.jp',
         }, name='password_reset'),

    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),

    url(r'^password_reset/done/$', auth_views.password_reset_done,
        {'template_name': 'registration/password_reset_done.html'}, name='password_reset_done'),

    url(r'^reset/(?P<uidb64>[^/]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,
        {'post_reset_redirect': 'accounts:password_reset_complete'}, name='password_reset_confirm'),

    #     global
    url(r'global-home/$', core_views.global_home, name='global_home'),
    url(r'global-login/$', core_views.global_login, name='global_login'),
    url(r'global-contact/$', core_views.global_contact, name='global_contact'),
    url(r'global-signup/$', core_views.global_signup, name='global_signup'),

    #Social signup set password URL
    #url(r'set_password/$', core_views.social_signup_set_password, name='social_signup_set_password'),    

]
