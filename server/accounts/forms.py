from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Profile
from .models import User


# user: signup.html
class UserForm(UserCreationForm):
    #username   = forms.CharField(max_length=20, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email      = forms.EmailField(label="Email", max_length=256, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_email', 'placeholder': "pigdata@example.com", 'pattern': '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'}))
    password1  = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'id_password1', 'placeholder': 'Password'}))
    password2  = forms.CharField(label="Password Confirm", widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'id_password2', 'placeholder': 'Retype password'}))

    class Meta:
        model = User
        fields = (
            'email',
            'password1',
            'password2',
        )

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.username = user.email

        if commit:
            user.save()
        return user


# user_profile: signup.html
class ProfileForm(forms.ModelForm):
    YES_NO_CHOICE     = ((1, 'はい'), (0, 'いいえ'))

    first_name = forms.CharField(label="First Name", max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_first_name', 'style ': "margin-left: -50px;", 'placeholder': 'First Name'}))
    last_name  = forms.CharField(label="Last Name", max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_last_name', 'style':'margin-left: -50px;', 'placeholder': 'Last Name'}))

    first_name_kana   = forms.CharField(label="First Name (kana)", max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_first_name_kana', 'style ': "margin-left: -50px;"}), required=False)
    last_name_kana    = forms.CharField(label="Last Name (kana)", max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_last_name_kana', 'style ': "margin-left: -50px;"}), required=False)

    company_name      = forms.CharField(label="Company", max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_company_name', 'placeholder': "会社名"}), required=False)
    company_name_kana = forms.CharField(label="Company (kana)", max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_company_name_kana', 'placeholder': "会社名(カナ)"}), required=False)
    department        = forms.CharField(label="Department", max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_department', 'placeholder': "部署名"}), required=False)
    job_title         = forms.CharField(label="Job Title", max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_job_title', 'placeholder': "役職名"}), required=False)
    tel_number        = forms.CharField(label="Phone Number", max_length=18, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_tel_number', 'placeholder': "0300000000", 'pattern': '\d{2,4}\d{3,4}\d{3,4}'}), required=False)
    post_code         = forms.CharField(label="", max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_post_code', 'placeholder': "1234567", 'pattern': '\d{3}\d{4}'}), required=False)
    address           = forms.CharField(label="", max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_address', 'placeholder': "東京都中央区湊3-5-10"}), required=False)

    is_agree_user_pol     = forms.ChoiceField(widget=forms.RadioSelect(attrs={'class': 'choice'}), choices=YES_NO_CHOICE, initial=0, required=False)
    is_agree_privacy_pol  = forms.ChoiceField(widget=forms.RadioSelect(attrs={'class': 'choice'}), choices=YES_NO_CHOICE, initial=0, required=False)
    is_agree_security_pol = forms.ChoiceField(widget=forms.RadioSelect(attrs={'class': 'choice'}), choices=YES_NO_CHOICE, initial=0, required=False)

    class Meta:
        model = Profile
        fields = ('first_name', 'last_name', 'last_name_kana', 'first_name_kana', 'company_name', 'company_name_kana', 'department', 'job_title', 'tel_number', 'post_code', 'address', 'is_agree_user_pol', 'is_agree_privacy_pol', 'is_agree_security_pol')

#profile_change.html - Not used
class UserChangeForm(forms.ModelForm):
    username   = forms.CharField(max_length=20, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "pigdata@example.com", 'disabled':True}), required=False)
    first_name = forms.CharField(max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_first_name'}))
    last_name  = forms.CharField(max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_last_name'}))

    class Meta:
        model = User
        #fields = ['username', 'first_name', 'last_name']
        fields = ['first_name', 'last_name']


# user_profile: profile_change.html
class ProfileChangeForm(forms.ModelForm):
    first_name        = forms.CharField(max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_first_name'}))
    last_name         = forms.CharField(max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_last_name'}))
    first_name_kana   = forms.CharField(max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_first_name_kana'}))
    last_name_kana    = forms.CharField(max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_last_name_kana'}))
    company_name      = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_company_name', 'placeholder': "会社名"}))
    company_name_kana = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_company_name_kana', 'placeholder': "会社名(カナ)"}))
    department        = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_department', 'placeholder': "部署名"}), required=False)
    job_title         = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_job_title', 'placeholder': "役職名"}), required=False)
    tel_number        = forms.CharField(max_length=18, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_tel_number', 'placeholder': "0300000000", 'pattern': '\d{2,4}\d{3,4}\d{3,4}'}), required=False)
    post_code         = forms.CharField(max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_post_code', 'placeholder': "1234567", 'pattern': '\d{3}\d{4}'}), required=False)
    address           = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_address', 'placeholder': "東京都中央区湊3-5-10"}), required=False)

    class Meta:
        model = Profile
        fields = ('first_name', 'last_name', 'first_name_kana', 'last_name_kana', 'company_name', 'company_name_kana', 'department', 'job_title', 'tel_number', 'post_code', 'address')

    def save(self, commit=True, update_fields=None):
        profile_change_form = super(ProfileChangeForm, self).save(commit=False)

        if commit:
            profile_change_form.save(update_fields=update_fields)
        return profile_change_form

