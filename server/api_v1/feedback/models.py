from django.db import models
from datetime import datetime
from accounts.models import User


class Feedback(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    start_url = models.TextField(null=True)
    browser = models.TextField(null=True)
    comment = models.TextField(null=True)
    timestamp = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return '%s %s %s %s' % (self.user.email, self.category, self.comment, self.timestamp)
