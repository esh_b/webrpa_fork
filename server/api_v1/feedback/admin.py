from django.contrib import admin
from .models import Feedback
# Register your models here.

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('user', 'start_url', 'browser', 'comment', 'timestamp')

admin.site.register(Feedback, FeedbackAdmin)