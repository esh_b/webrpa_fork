from django.urls.conf import path

from . import sessions
import api_v1.scraping.views as scraping

urlpatterns = [
    path('login', sessions.login),
    path('logout', sessions.logout),
    path('user_type', scraping.user_type),
    path('get_version', scraping.get_version),
    path('project_created', scraping.project_created),
    path('user_storage', scraping.user_storage),
]
