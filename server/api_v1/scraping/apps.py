from django.apps import AppConfig


class ScrapingConfig(AppConfig):
    name = 'api_v1.scraping'
