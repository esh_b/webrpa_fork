from __future__ import unicode_literals
from accounts.models import User
from django.db import models
from django.conf import settings
import os
import sys
import uuid

from django_celery_results.models import TaskResult
from django_celery_beat.models import PeriodicTask

project_folder = settings.SCRAPING_PROJECT_DIR

class Project(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=256)
    date_created = models.DateTimeField()
    last_executed = models.DateTimeField(null=True)
    status = models.IntegerField(null=True)
    category = models.CharField(max_length=256, null=True, default="No Category")
    start_url = models.CharField(max_length=1024, null=True, default=None)
    project_size = models.FloatField(null=True, blank=True, default=None)
    keyword = models.CharField(max_length=1024, null=True, default=None)
    mail_alert = models.BooleanField(default=False)
    data_alert = models.BooleanField(default=False)
    keyword_alert = models.BooleanField(default=False)
    periodic_task = models.ForeignKey(PeriodicTask, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.name

    def get_url(self):
        project = Project.objects.get(id = self.id)
        return project.start_url

    def user_name(self):
        return self.user.username

    def full_name(self):
        return self.user.profile.last_name + " " + self.user.profile.first_name

"""
class Execution(models.Model):
    id = models.CharField(max_length=36, primary_key=True)
    project = models.ForeignKey(Project, on_delete=models.PROTECT)
    date_executed = models.DateTimeField()
    status = models.IntegerField()
    error = models.IntegerField()
    celery_id = models.CharField(max_length=36, blank=True)
"""

class Execution(models.Model):
    task = models.ForeignKey(TaskResult, on_delete=models.PROTECT, to_field="task_id")
    project = models.ForeignKey(Project, on_delete=models.PROTECT)
    date_executed = models.DateTimeField()
    end_time = models.DateTimeField()
    exec_time = models.DurationField()
    status = models.BooleanField()

class BehindLogin(models.Model):
    project = models.ForeignKey(Project, on_delete=models.PROTECT)
    salt = models.CharField(max_length=256, blank=True)


class CeleryTask(TaskResult):
    def get_project_name(self):
        execution = Execution.objects.get(celery_id = self.task_id)
        project = Project.objects.get(id = execution.project_id)
        return project.name

    get_project_name.short_description = 'project name'

    def get_user_name(self):
        execution = Execution.objects.get(celery_id = self.task_id)
        project = Project.objects.get(id = execution.project_id)
        user = User.objects.get(id = project.user_id)
        return user.profile.last_name + ' '+ user.profile.first_name
    get_user_name.short_description = 'User name'

    def get_user_email(self):
        execution = Execution.objects.get(celery_id = self.task_id)
        project = Project.objects.get(id = execution.project_id)
        user = User.objects.get(id = project.user_id)
        return user.email
    get_user_email.short_description = 'email'

    def get_start_url(self):
        execution = Execution.objects.get(celery_id = self.task_id)
        project = Project.objects.get(id = execution.project_id)
        return project.start_url
    get_start_url.short_description = 'start url'

    class Meta:
        proxy = True




