from __future__ import absolute_import, unicode_literals
import io
import json
import logging
import os
import time

from .models import Project
from api_v1.scraping.models import Execution
from api_v1.scraping.models import BehindLogin
from celery import shared_task
from dashboard.convert import Download
from django.conf import settings
from internal import load_json_request, HTTPException
from internal.xpath import Extraction
from internal.rpalib.scraping import ActionInfo, PageInfo, Project as ProjClass

data_folder = settings.SCRAPING_DATA_DIR


def run_xpath(project_tmp, key, pid, retry_no, timer):
    try:        
        out = Extraction().process(project_tmp, key, timer)
        
        if retry_no == 0 and out[2] == -1:
            return run_xpath(project_tmp, key, pid, 1, 5)
        elif retry_no == 1 and out[2] == -1:
            return run_xpath(project_tmp, key, pid, 2, 10)
        elif retry_no == 2 and out[2] == -1:
            return out
        else:
            return out  
    
    except Exception as e:
        if retry_no == 0:
            time.sleep(5)
            run_xpath(project_tmp, key, pid, 1, 0)
        else:
            return None

def get_procedure_file(pid, retry_no):
    try:
        project = Project.objects.get(id=pid)

        data_path = os.path.join(settings.SCRAPING_DATA_DIR, str(project.user.id), project.id)
        os.makedirs(data_path, exist_ok=True)

        project_path = os.path.join(settings.SCRAPING_PROJECT_DIR, str(project.user.id), project.id + '.json')
        with open(project_path, 'r') as fp:
            return json.load(fp)
    except Exception as e:
        if retry_no == 0:
            time.sleep(5)
            return get_procedure_file(pid, 1)
        else:
            print(e.message)
            return None

@shared_task(bind=True)
def task_scrape(self, uid, pid):
    eid = self.request.id
    print("----inside task scrape--------")
    project = Project.objects.get(
        id=pid)

    data_path = os.path.join(settings.SCRAPING_DATA_DIR, str(project.user.id), str(pid))
    os.makedirs(data_path, exist_ok=True)

    project_path = os.path.join(settings.SCRAPING_PROJECT_DIR,
                                str(project.user.id),
                                str(pid) + '.json')

    if os.path.isfile(project_path):
        with open(project_path, 'r') as fp:
            project = json.load(fp)
    else:
        print('\nSorry. File Not Found!\n')
        return

    project_tmp = ProjClass.from_dict(project)
    output = []

    if project_tmp.login_details and project_tmp.project_type != 5:
        salt = BehindLogin.objects.filter(project_id=pid)
        key = salt[0].salt
        output = Extraction().process(project_tmp, key=key)
    else:
        output = Extraction().process(project_tmp, "", 0)

    if output:
        data_path = os.path.join(data_path, eid + '.json')
        with io.open(data_path, 'w', encoding='utf8') as fp:
            json.dump(output, fp, ensure_ascii=False)

        ## call function for download images
        if output[2] == 1 or output[2] == 2:
            folder_path = os.path.join(settings.SCRAPING_DATA_DIR, str(uid), str(pid), str(eid))
            os.makedirs(folder_path, exist_ok=True)
            folder_name = pid + '_' + eid + '/'
            Download().download_folder(output[1], str(folder_path + '/' + folder_name))

        total_size = 0
        start_path = data_folder + str(uid) + '/' + str(pid) + '/'
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        print("-------")
        print(total_size)
        project = Project.objects.get(id=pid)
        project.project_size = total_size
        project.save()
    return