from . import internal
from enum import Enum
from .core import Session

class Feedback():
    def __init__(self, category='', comment=''):
        self.category = category
        self.comment = comment


def post_feedback(session: Session, feedback: Feedback):
    """
    Saves feedback from user.

    :param session: The session.
    :param category: feedback cateory to categorize the users and problems.
    :param comment: The user comment or feedback.
    :param feedback_time: Time when feedback is stored.
    :return: status_code
    :rtype: int
    """

    feedback = internal.as_jsonable(feedback)
    return session._invoke_method('feedback/post_feedback', feedback=feedback)
