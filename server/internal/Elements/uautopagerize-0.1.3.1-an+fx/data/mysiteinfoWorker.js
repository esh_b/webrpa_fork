var MY_SITEINFO = [];
var MICROFORMAT = [];
var EXCLUDE = [];

onmessage = function (event) {
  try {
    eval(event.data.code);
  } catch (e) {
    console.error(e);
    return postMessage({});
  }
  let json = {
    MY_SITEINFO, MICROFORMAT, EXCLUDE
  };
  json = JSON.stringify(json, '', '');
  postMessage({ code: json });
};
