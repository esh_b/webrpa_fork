'use strict';
var userAgent = window.navigator.userAgent.toLowerCase();
var IS_EDGE = userAgent.includes('edge');
var IS_FIREFOX = userAgent.includes('firefox');
var IS_CHROME = userAgent.includes('chrome');
var IS_OPERA = userAgent.includes('opera');

var code = '';
var editor = null;
var jsbOpts = {
  indent_size: 2,
  brace_style: 'collapse'
};

chrome.storage.local.get('mySiteinfoCode', function (changes, areaName) {
  code = changes.mySiteinfoCode || '';
  $(document).ready(onReady);
});

function onReady() {
  if (!IS_FIREFOX) {
    let iframe = document.createElement('iframe');
    iframe.src = 'data/sandbox.html';
    document.body.append(iframe);
  }

  editor = ace.edit('editor');
  var JavaScriptMode = ace.require('ace/mode/javascript').Mode;
  editor.getSession().setMode(new JavaScriptMode());
  editor.getSession().setTabSize(jsbOpts.indent_size);
  editor.getSession().setValue(code);

  editor.commands.addCommand({
    Name: 'save',
    bindKey: {
      win: 'Ctrl-S',
      mac: 'Command-S'
    },
    exec: function (editor) {
      onSave();
    }
  });

  $('#save-myinfo').on('click', onSave);
  $('#format-myinfo').on('click', onFormat);
}

function onSave() {
  let session = editor.getSession();
  let code = session.getValue();
  chrome.storage.local.set({
    mySiteinfoCode: code
  });
  loadMySiteinfo(code);
  mes('saved', 3000);
}

function onFormat() {
  let session = editor.getSession();
  session.doc.setValue(js_beautify(session.getValue(), jsbOpts));
}

var statusTimer = null;
function mes(text, time = 0) {
  clearTimeout(statusTimer);
  let statusbar = $('#statusbar');
  statusbar.text(text);
  if (time && text) {
    statusTimer = setTimeout(() => {
      statusbar.text('');
    }, time);
  }
}

function loadMySiteinfo(code) {
  if (IS_FIREFOX) {
    let worker = new Worker('data/mysiteinfoWorker.js');
    worker.addEventListener('message', loadMySiteinfoCallback, { once: true });
    worker.postMessage({ code });
  } else {
    let iframe = $('iframe')[0];
    window.addEventListener('message', loadMySiteinfoCallback, { once: true, captrue: true });
    iframe.contentWindow.postMessage({ code }, '*');
  }
}

function loadMySiteinfoCallback(event) {
  try {
    let obj = JSON.parse(event.data.code);
    let MY_SITEINFO = obj.MY_SITEINFO;
    let MICROFORMAT = obj.MICROFORMAT;
    MY_SITEINFO.forEach((info) => info.myinfo = true);
    MICROFORMAT.forEach((info) => info.myinfo = true);
    console.log('loadMySiteinfo: OK', MY_SITEINFO, MICROFORMAT);
    chrome.storage.local.set({ MY_SITEINFO, MICROFORMAT });
  } catch (e) {
    console.error('loadMySiteinfo: error', e);
    mes(e);
  }
}
