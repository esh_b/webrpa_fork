// uAutoPagerize
// loading next page and inserting into current page.
// http://d.hatena.ne.jp/Griver/
//
// this script based on
// AutoPagerize(http://autopagerize.net/).
//
// Released under the GPL license
// http://www.gnu.org/copyleft/gpl.html
'use strict';

function AutoPager(info) {
  this.pageNum = 1;
  this.state = 'enable';
  this.timer = null;
  this.requestInterval = null;
  this.lastRequestTime = 0;
  this.info = info;

  this.requestURL = this.getNextURL(document, location.href);
  if (!this.requestURL) {
    debug('getNextURL returns null.', this.info.nextLink);
    return;
  }

  this.setInsertPoint();
  if (!this.insertPoint) {
    debug('insertPoint not found.', this.info.pageElement);
    return;
  }
  this.setRemainHeight();
  this.initIcon();
  // this.initContextmenu();

  this.loadedURLs = {};
  this.loadedURLs[location.href] = true;
  this.addListener();
  this.setState('enable');
  this.scroll();
  if (getScrollHeight() == window.innerHeight)
    document.body.style.minHeight = (window.innerHeight + 1) + 'px';
}

AutoPager.prototype.setState = function (state) {
  if (this.state !== 'terminated' && this.state !== 'error') {
    if (this.state === 'loading' && state === 'disable') {
      this.abort();
    } else if (state === 'terminated' || state === 'error') {
      this.removeListener();
      setTimeout(() => {
        this.icon.style.setProperty('display', 'none', '');
      }, 2000);
    }
    Array.from(document.getElementsByClassName('autopagerize_icon'), (elem) => {
      elem.setAttribute('title', state);
      elem.dataset.state = state;
    });
    this.state = state;
  }
  return state;
};

AutoPager.prototype.stateToggle = function () {
  this.setState(this.state == 'disable' ? 'enable' : 'disable');
};

AutoPager.prototype.destroy = function (isRemoveAddPage) {
  this.setState('disable');
  this.removeListener();
  this.abort();

  if (isRemoveAddPage) {
    var separator = document.querySelector('.autopagerize_page_separator, .autopagerize_page_info');
    if (separator) {
      var range = new Range();
      range.setStartBefore(separator);
      range.setEndBefore(this.insertPoint);
      range.deleteContents();
    }
  }
  Array.from(document.getElementsByClassName('autopagerize_icon'), (elem) => {
    elem.remove();
  });
};

AutoPager.prototype.addListener = function () {
  window.addEventListener('scroll', this, { passive: true });
  document.addEventListener('AutoPagerizeToggleRequest', this, false);
  document.addEventListener('AutoPagerizeEnableRequest', this, false);
  document.addEventListener('AutoPagerizeDisableRequest', this, false);
};

AutoPager.prototype.removeListener = function () {
  window.removeEventListener('scroll', this, false);
  document.removeEventListener('AutoPagerizeToggleRequest', this, false);
  document.removeEventListener('AutoPagerizeEnableRequest', this, false);
  document.removeEventListener('AutoPagerizeDisableRequest', this, false);
};

AutoPager.prototype.handleEvent = function (event) {
  switch (event.type) {
    case 'scroll':
      if (this.timer)
        clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        this.scroll();
        this.timer = null;
      }, 200);
      break;
    case 'click':
      if (!event.isTrusted || event.button === 2) return;
      if (event.target.classList.contains('autopagerize_icon')) {
        this.stateToggle();
        return;
      }
      if (event.target.dataset.command === 'show_log') {
        console.log(this);
        ['filters', 'documentFilters', 'requestFilters', 'responseFilters', 'fragmentFilters'].map(f => {
          if (apfilter[f].length)
            console.log(f + ':\n', apfilter[f].join('\n'));
        });
        return;
      }
      break;
    case 'AutoPagerizeToggleRequest':
      this.stateToggle();
      break;
    case 'AutoPagerizeEnableRequest':
      this.setState('enable');
      break;
    case 'AutoPagerizeDisableRequest':
      this.setState('disable');
      break;
  }
};

AutoPager.prototype.scroll = function () {
  if (this.state !== 'enable') return;
  var remain = getScrollHeight() - window.innerHeight - window.scrollY;
  if (remain < this.remainHeight) {
    this.request();
  }
};

AutoPager.prototype.abort = function () {
  if (this.state === 'loading') {
    this.lastRequestData.abort();
    this.setState('error');
  }
};

AutoPager.prototype.request = function () {
  if (!this.requestURL || this.loadedURLs[this.requestURL]) return;
  if (this.requestInterval) return;
  if (this.state !== 'enable') return;

  var now = new Date().getTime();
  var remaining = now - this.lastRequestTime;
  if (remaining < 1000) {
    this.requestInterval = setTimeout(() => {
      this.requestInterval = null;
      this.scroll();
    }, 1000 - remaining);
    return;
  } else {
    this.lastRequestTime = now;
  }
  // Google, YouTube で検索結果が変更されていたらリセットする。対策として不完全。
  if ((location.host.endsWith('.youtube.com') || /\.google\.(com|[a-z]{2}|co\.(jp|kr))$/.test(location.host)) &&
    this.insertPoint.compareDocumentPosition(document) >= 32) {
    // let info = this.info;
    // this.destroy();
    // setTimeout(() => {
    //   ap = null;
    //   launchAutoPager([info]);
    //   debug('Reset');
    // }, 500);
    this.setState('error');
    return;
  }

  this.lastRequestURL = this.requestURL;
  var opt = {
    url: this.requestURL,
    method: 'get',
    headers: {},
    timeout: settings.XHR_TIMEOUT,
    responseType: 'document',
    overrideMimeType: 'text/html; charset=' + document.characterSet
  };
  apfilter.requestFilters.forEach(function (i) { i(opt); }, this);

  if (this.requestURL != opt.url)
    this.requestURL = opt.url;
  this.setState('loading');

  opt.onload = res => {
    var ev = new Event('AutoPagerize_RequestLoad', { bubbles: true });
    document.dispatchEvent(ev);
    this.requestLoad(res.target);
  };
  opt.onerror = res => {
    var ev = new Event('AutoPagerize_RequestError', { bubbles: true });
    document.dispatchEvent(ev);
    this.setState('error');
    debug('Response Error.', this.lastRequestData);
  };
  this.lastRequestData = GM_xmlhttpRequest(opt);
  var ev = new Event('AutoPagerize_Request', { bubbles: true });
  document.dispatchEvent(ev);
  debug('Request:', this.lastRequestURL, this.lastRequestData);
};

AutoPager.prototype.requestLoad = function (res) {
  var urlo = new URL(res.responseURL);
  if (location.origin != urlo.origin) {
    let r = /[^.]{3,}\.([a-z]{2,3}|[a-z]{2}\.[a-z]{2})$/;
    let a = location.origin.match(r);
    let b = urlo.origin.match(r);
    if (a && b && a[0] === b[0]) {
      debug(location.origin + 'から' + urlo.origin + 'は許可します');
    } else {
      console.log(location.origin + 'から' + urlo.origin + 'は読み込めません。uAutoPagerize を停止します。');
      this.setState('error');
      return;
    }
  }

  apfilter.responseFilters.forEach(function (i) { i(res, this.requestURL); }, this);
  if (this.requestURL != res.responseURL)
    this.requestURL = res.responseURL;
  var htmlDoc = res.response;
  apfilter.documentFilters.forEach(function (i) { i(htmlDoc, this.requestURL, this.info); }, this);

  if (this.loadedURLs[this.requestURL]) {
    debug('page is already loaded.', this.requestURL, this.info.nextLink);
    this.setState('terminated');
    return;
  }

  var page, url;
  try {
    page = getElementsByXPath(this.info.pageElement, htmlDoc);
    url = this.getNextURL(htmlDoc, res.responseURL);
  } catch (e) {
    this.setState('error');
    return;
  }
  if (!page || !page[0]) {
    debug('pageElement not found.', this.info.pageElement);
    this.setState('terminated');
    return;
  }
  this.loadedURLs[this.requestURL] = true;

  // fix insertPoint
  if (this.insertPoint.compareDocumentPosition(document) >= 32) {
    this.setInsertPoint();
    if (!this.insertPoint) {
      debug('insertPoint not found.', this.info.pageElement);
      this.setState('terminated');
      return;
    }
    this.setRemainHeight();
  }

  ++this.pageNum;
  page = this.addPage(htmlDoc, page);

  // 継ぎ足し後にスクロールし過ぎていたら戻す 
  try {
    let range = new Range();
    range.setStartBefore(page[0]);
    range.setEndAfter(page[page.length - 1]);
    let rect = range.getBoundingClientRect();
    if (settings.FIX_SCROLL_OVER) {
      if (rect.top < 0) {
        let sep = Array.from(document.querySelectorAll('.autopagerize_page_info')).pop();
        sep.scrollIntoView(true);
        debug('FIX_SCROLL_OVER');
      }
    } else if (rect.bottom < 0) {
      let sep = Array.from(document.querySelectorAll('.autopagerize_page_info')).pop();
      sep.scrollIntoView(true);
      debug('FIX_SCROLL_OVER', 'fix scroll-anchoring.');
    }
  } catch (e) {
    console.error(e);
  }

  this.requestURL = url;
  if (!url) {
    debug('nextLink not found.', this.info.nextLink, htmlDoc);
    this.setState('terminated');
  } else {
    this.setState('enable');
  }

  var ev = new Event('GM_AutoPagerizeNextPageLoaded', { bubbles: true });
  document.dispatchEvent(ev);

  apfilter.filters.forEach(function (i) { i(page); });
};

AutoPager.prototype.addPage = function (htmlDoc, page) {
  var fragment = document.createDocumentFragment();
  page.forEach(function (i) { fragment.appendChild(i); });
  apfilter.fragmentFilters.forEach(function (i) { i(fragment, htmlDoc, page); }, this);

  // fix relative urls
  var getdir = function (url) {
    return url.replace(/[?#].+$/, '').replace(/[^/]+$/, '');
  };
  var newBaseURI = htmlDoc.baseURI === document.baseURI ?
    this.requestURL :
    htmlDoc.baseURI;
  // log(
  //  '\n baseURL...:' + document.baseURI + 
  //  '\n requestURL:' + this.requestURL + 
  //  '\n newBaseURL:' + newBaseURI
  // );
  var cdir = getdir(document.baseURI);
  var rdir = getdir(newBaseURI);
  if (cdir != rdir) {
    for (var elem of fragment.querySelectorAll('a[href]:not([href^="javascript"]):not([href^="#"]):not([href^="//"])')) {
      elem.href = new URL(elem.getAttribute('href'), newBaseURI).href;
    }
    for (var elem of fragment.querySelectorAll('img[src]:not([src^="//"])')) {
      elem.src = new URL(elem.getAttribute('src'), newBaseURI).href;
    }
  }

  var hr = document.createElement('hr');
  hr.setAttribute('class', 'autopagerize_page_separator');
  var p = document.createElement('p');
  p.append(this.createIcon());
  p.setAttribute('class', 'autopagerize_page_info');
  var a = document.createElement('a');
  a.href = this.requestURL;
  a.setAttribute('class', 'autopagerize_link');
  a.append('page: ' + this.pageNum);
  p.append(a);

  var insertParent = this.insertPoint.parentNode;
  if (page[0] && page[0].tagName == 'TR') {
    var colNodes = getElementsByXPath('child::tr[1]/child::*[self::td or self::th]', insertParent);
    var colums = 0;
    for (var i = 0, l = colNodes.length; i < l; i++) {
      var col = colNodes[i].getAttribute('colspan');
      colums += parseInt(col, 10) || 1;
    }
    var td = document.createElement('td');
    td.appendChild(hr);
    td.appendChild(p);
    var tr = document.createElement('tr');
    td.setAttribute('colspan', colums);
    tr.appendChild(td);
    fragment.insertBefore(tr, fragment.firstChild);
  } else {
    fragment.insertBefore(p, fragment.firstChild);
    fragment.insertBefore(hr, fragment.firstChild);
  }

  insertParent.insertBefore(fragment, this.insertPoint);
  page.forEach(function (pe) {
    // 要素を付加すると event.detail が Web ページ側から取得できなくなる
    var ev = new CustomEvent('AutoPagerize_DO' + 'MNodeI' + 'nserted', {
      bubbles: true,
      detail: {
        // relatedNode: this.insertPoint.parentNode,
        newValue: this.lastRequestURL
      }
    });
    pe.dispatchEvent(ev);
  }, this);
  return page;
};

AutoPager.prototype.getNextURL = function (doc, baseURL) {
  var nextLink = getFirstElementByXPath(this.info.nextLink, doc);
  if (!nextLink) return '';
  // nextLinkFilters の最初に返された値を利用する
  // href, action を取れば<base>を考慮した値が取れるはず
  // data, value を取れば #text, <input>, @data-xxx の値も利用できるかもしれない
  // getAttribute は<a>以外に @href が付いているケースがあるかもしれない(XMLなど)
  var values = apfilter.nextLinkFilters.map((func) => func(nextLink, baseURL, doc));
  var nextValue = values.find(url => url) ||
    nextLink.href || nextLink.action || nextLink.data || nextLink.value ||
    nextLink.getAttribute('href') || nextLink.getAttribute('action');
  if (!nextValue) return '';
  if (!/^http(s)?:/.test(nextValue)) {
    return new URL(nextValue, baseURL).href;
  }
  return nextValue;
};

AutoPager.prototype.getPageElementsBottom = function () {
  var elem = getElementsByXPath(this.info.pageElement, document).pop();
  return elem ? getNodeBottom(elem) : null;
};

AutoPager.prototype.setInsertPoint = function () {
  var insertPoint = null;
  if (this.info.insertBefore) {
    insertPoint = getFirstElementByXPath(this.info.insertBefore, document);
  }
  if (!insertPoint) {
    let lastPageElement = getElementsByXPath(this.info.pageElement, document).pop();
    if (lastPageElement) {
      insertPoint = lastPageElement.nextSibling ||
        lastPageElement.parentNode.appendChild(document.createTextNode(' '));
    }
  }
  this.insertPoint = insertPoint;
};

AutoPager.prototype.setRemainHeight = function () {
  var scrollHeight = getScrollHeight();
  var bottom = getNodeTop(this.insertPoint) ||
    this.getPageElementsBottom() ||
    Math.round(scrollHeight * 0.8);
  this.remainHeight = scrollHeight - bottom + settings.BASE_REMAIN_HEIGHT;
};

AutoPager.prototype.initIcon = function () {
  this.icon = this.createIcon();
  // if (window == window.top)
  //   this.icon.style.setProperty('display', 'none', '');
  this.icon.setAttribute('id', 'uAutoPagerize-icon');
  this.icon = document.body.appendChild(this.icon);
};

AutoPager.prototype.createIcon = function () {
  var icon = document.createElement('span');
  icon.setAttribute('class', 'autopagerize_icon');
  icon.setAttribute('title', this.state);
  icon.setAttribute('contextmenu', 'uAutoPagerize-menu');
  icon.dataset.state = this.state;
  icon.addEventListener('click', this, false);
  return icon;
};

AutoPager.prototype.initContextmenu = function () {
  var image = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAElBMVEX///8m3yYL9gs7wTs9oz0zjDP0NQheAAAABnRSTlMA//////96eeD+AAAALUlEQVQI12NggAEmJQVBQWZjA3wMOGAUFDA2ZnFxwMeAA6BOFxfW0AB8DBgAAEedDdGQfErpAAAAAElFTkSuQmCC';
  var menu = document.createElement('menu');
  menu.setAttribute('id', 'uAutoPagerize-menu');
  menu.setAttribute('type', 'context');

  var item = menu.appendChild(document.createElement('menuitem'));
  item.setAttribute('label', 'console.log(this)');
  item.setAttribute('icon', image);
  item.dataset.command = 'show_log';
  item.addEventListener('click', this, false);

  document.body.appendChild(menu);
  this.menu = menu;
};

apfilter.fragmentFilters.push(function (df) {
  if (!settings.FORCE_TARGET_WINDOW) return;
  for (var elem of df.querySelectorAll('a[href]:not([href^="mailto:"]):not([href^="javascript:"]):not([href^="#"])')) {
    elem.setAttribute('target', '_blank');
  }
});

for (let attr of ['data-src', 'data-original', 'data-lazy-src', 'data-thumb', 'data-image', 'data-mediumthumb', 'ajax']) {
  if (document.querySelector('img[' + attr + ']')) {
    apfilter.fragmentFilters.push(function (df) {
      if (!settings.FIX_LAZYLOAD) return;
      Array.from(df.querySelectorAll('img[' + attr + ']'), function (img) {
        img.setAttribute('src', img.getAttribute(attr));
      });
    });
  }
}

var ev = new Event('GM_AutoPagerizeLoaded', { bubbles: true });
document.dispatchEvent(ev);

function launchAutoPager(list) {
  if (!list || list.length === 0) return;
  if (ap) return;
  // var s = new Date().getTime();
  for (var info of list) {
    try {
      if (!location.href.match(info.url)) continue;
      if (!getFirstElementByXPath(info.nextLink)) {
        // FIXME microformats case detection.
        // limiting greater than 12 to filter microformats like SITEINFOs.
        if (info.url.length > 12) {
          debug('nextLink not found.', info);
        }
        continue;
      }
      if (!getFirstElementByXPath(info.pageElement)) {
        if (info.url.length > 12) {
          debug('pageElement not found.', info);
        }
        continue;
      }
      // log(new Date().getTime() - s + 'ms', info);
      ap = new AutoPager(info);
      if (info.eventinfo) {
        console.log('uAutoPagerize がイベントから起動しました', ap);
      } else {
        debug('launched:', ap);
      }
      return ap;
    } catch (e) { }
  }
}

function log(...args) {
  console.log.apply(console, args);
}

function debug(...args) {
  if (settings.DEBUG) {
    args.unshift('debug:');
    console.log.apply(console, args);
  }
}

function getElementsByXPath(xpath, node) {
  var nodesSnapshot = getXPathResult(xpath, node,
    XPathResult.ORDERED_NODE_SNAPSHOT_TYPE || 7);
  var data = [];
  for (var i = 0; i < nodesSnapshot.snapshotLength; i++) {
    data.push(nodesSnapshot.snapshotItem(i));
  }
  return data;
}

function getFirstElementByXPath(xpath, node) {
  var result = getXPathResult(xpath, node,
    XPathResult.FIRST_ORDERED_NODE_TYPE || 9);
  return result.singleNodeValue;
}

function getXPathResult(xpath, node = document, resultType) {
  var doc = node.ownerDocument || node;
  var resolver = doc.createNSResolver(node.documentElement || node);
  // Use |node.lookupNamespaceURI('')| for Opera 9.5
  // A workaround for bugs of Node.lookupNamespaceURI(null)
  // https://bugzilla.mozilla.org/show_bug.cgi?id=693615
  // https://bugzilla.mozilla.org/show_bug.cgi?id=694754
  var defaultNS = null;
  try {
    // This follows the spec: http://www.w3.org/TR/DOM-Level-3-Core/namespaces-algorithms.html#lookupNamespaceURIAlgo
    if (node.nodeType == node.DOCUMENT_NODE) {
      defaultNS = node.documentElement.lookupNamespaceURI(null);
    } else {
      defaultNS = node.lookupNamespaceURI(null);
    }
  } catch (e) { }

  if (defaultNS) {
    const defaultPrefix = '__default__';
    xpath = addDefaultPrefix(xpath, defaultPrefix);
    var defaultResolver = resolver;
    resolver = function (prefix) {
      return (prefix == defaultPrefix) ? defaultNS : defaultResolver.lookupNamespaceURI(prefix);
    };
  }
  return doc.evaluate(xpath, node, resolver, resultType, null);
}

function addDefaultPrefix(xpath, prefix) {
  const tokenPattern = /([A-Za-z_\u00c0-\ufffd][\w\-.\u00b7-\ufffd]*|\*)\s*(::?|\()?|(".*?"|'.*?'|\d+(?:\.\d*)?|\.(?:\.|\d+)?|[\)\]])|(\/\/?|!=|[<>]=?|[\(\[|,=+-])|([@$])/g;
  const TERM = 1,
    OPERATOR = 2,
    MODIFIER = 3;
  var tokenType = OPERATOR;
  prefix += ':';

  function replacer(token, identifier, suffix, term, operator, modifier) {
    if (suffix) {
      tokenType =
        (suffix == ':' || (suffix == '::' &&
          (identifier == 'attribute' || identifier == 'namespace'))) ? MODIFIER : OPERATOR;
    } else if (identifier) {
      if (tokenType == OPERATOR && identifier != '*') {
        token = prefix + token;
      }
      tokenType = (tokenType == TERM) ? OPERATOR : TERM;
    } else {
      tokenType = term ? TERM : operator ? OPERATOR : MODIFIER;
    }
    return token;
  }
  return xpath.replace(tokenPattern, replacer);
}

function getNodeTop(elem) {
  let rect;
  if (typeof 'getBoundingClientRect' !== 'function') {
    let range = new Range();
    range.selectNode(elem);
    range.setEndAfter(elem.parentNode);
    rect = range.getBoundingClientRect();
  } else {
    rect = elem.getBoundingClientRect();
  }
  return window.scrollY + rect.top;
}

function getNodeBottom(elem) {
  let rect;
  if (typeof 'getBoundingClientRect' !== 'function') {
    let range = new Range();
    range.selectNode(elem);
    range.setStartBefore(elem.parentNode);
    rect = range.getBoundingClientRect();
  } else {
    rect = elem.getBoundingClientRect();
  }
  return window.scrollY + rect.bottom;
}

function getElementPosition(elem) {
  var offsetTrail = elem;
  var offsetLeft = 0;
  var offsetTop = 0;
  while (offsetTrail) {
    offsetLeft += offsetTrail.offsetLeft;
    offsetTop += offsetTrail.offsetTop;
    offsetTrail = offsetTrail.offsetParent;
  }
  offsetTop = offsetTop || null;
  offsetLeft = offsetLeft || null;
  return {
    left: offsetLeft,
    top: offsetTop
  };
}

function getElementBottom(elem) {
  var c_style = document.defaultView.getComputedStyle(elem, '');
  var height = 0;
  var prop = ['height', 'borderTopWidth', 'borderBottomWidth',
    'paddingTop', 'paddingBottom',
    'marginTop', 'marginBottom'
  ];
  prop.forEach(function (i) {
    var h = parseInt(c_style[i]);
    if (typeof h == 'number') {
      height += h;
    }
  });
  var top = getElementPosition(elem).top;
  return top ? (top + height) : null;
}

function getScrollHeight() {
  return Math.max(document.documentElement.scrollHeight, document.body.scrollHeight);
}

function createDocumentFragmentByString(str) {
  var range = document.createRange();
  range.setStartAfter(document.body);
  return range.createContextualFragment(str);
}

function escapeHTML(str) {
  return (str + '').replace(/[&"<>]/g, (m) => ({ '&': '&amp;', '"': '&quot;', '<': '&lt;', '>': '&gt;' })[m]);
}

function GM_xmlhttpRequest(opt) {
  var req = new XMLHttpRequest();
  req.open(opt.method || 'get', opt.url, true);

  if (opt.headers && typeof opt.headers == 'object') {
    for (var i in opt.headers) {
      req.setRequestHeader(i, opt.headers[i]);
    }
  }

  for (var k of ['timeout', 'responseType', 'onload', 'onerror', 'onreadystatechange']) {
    if (opt[k])
      req[k] = opt[k];
  }

  if (opt.overrideMimeType)
    req.overrideMimeType(opt.overrideMimeType);

  req.send(opt.data || null);
  return req;
}


