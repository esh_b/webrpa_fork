(function(document) {
	var last;
	var sim_data = [];
	function cssPath(el) {
		var fullPath    = 0,  // Set to 1 to build ultra-specific full CSS-path, or 0 for optimised selector
		    useNthChild = 0,  // Set to 1 to use ":nth-child()" pseudo-selectors to match the given element
		    cssPathStr = '',
		    testPath = '',
		    parents = [],
		    parentSelectors = [],
		    tagName,
		    cssId,
		    cssClass,
		    tagSelector,
		    vagueMatch,
		    nth,
		    i,
		    c;
		
		// Go up the list of parent nodes and build unique identifier for each:
		while ( el ) {
			vagueMatch = 0;

			// Get the node's HTML tag name in lowercase:
			tagName = el.nodeName.toLowerCase();
			
			// Get node's ID attribute, adding a '#':
			cssId = ( el.id ) ? ( '#' + el.id ) : false;
			
			// Get node's CSS classes, replacing spaces with '.':
			cssClass = ( el.className ) ? ( '.' + el.className.replace(/\s+/g,".") ) : '';

			// Build a unique identifier for this parent node:
			if ( cssId ) {
				// Matched by ID:
				tagSelector = tagName + cssId + cssClass;
			} else if ( cssClass ) {
				// Matched by class (will be checked for multiples afterwards):
				tagSelector = tagName + cssClass;
			} else {
				// Couldn't match by ID or class, so use ":nth-child()" instead:
				vagueMatch = 1;
				tagSelector = tagName;
			}
			
			// Add this full tag selector to the parentSelectors array:
			parentSelectors.unshift( tagSelector );

			// If doing short/optimised CSS paths and this element has an ID, stop here:
			// if ( cssId && !fullPath )
			// 	break;
			
			// Go up to the next parent node:
			el = el.parentNode !== document ? el.parentNode : false;
			
		} // endwhile
		
		
		// Build the CSS path string from the parent tag selectors:
		for ( i = 0; i < parentSelectors.length; i++ ) {
			cssPathStr += ' ' + parentSelectors[i];// + ' ' + cssPathStr;
			
			// If using ":nth-child()" selectors and this selector has no ID / isn't the html or body tag:
			if ( useNthChild && !parentSelectors[i].match(/#/) && !parentSelectors[i].match(/^(html|body)$/) ) {
				
				// If there's no CSS class, or if the semi-complete CSS selector path matches multiple elements:
				if ( !parentSelectors[i].match(/\./) || $( cssPathStr ).length > 1 ) {
					
					// Count element's previous siblings for ":nth-child" pseudo-selector:
					for ( nth = 1, c = el; c.previousElementSibling; c = c.previousElementSibling, nth++ );
					
					// Append ":nth-child()" to CSS path:
					cssPathStr += ":nth-child(" + nth + ")";
				}
			}
			
		}
		
		// Return trimmed full CSS path:
		return cssPathStr.replace(/^[ \t]+|[ \t]+$/, '');
	}

	function simplify(str1) {
		var arr = str1.split("#");
		var xarr = [];
		var tagName = '';
		var idVal = '';
		var className = '';
		if(arr.length < 2) {
			var arr2 = arr[0].split(".");
			tagName = arr2[0];
			xarr = arr2;
		}
		else { 
			tagName = arr[0];
			var arr2 = arr[1].split(".");
			idVal = arr2[0];
			xarr = arr2;
		}
		if(xarr.length < 2) {
			className = ' ';
		}
		else if(xarr.length < 3) {
			className = xarr[1] + ' ';
		}
		else {
			for (k = 1; k < xarr.length; k++) {
				className = className + xarr[k] + ' ';
			}
		}
		className = className.slice(0, -1);
		return [tagName, idVal, className];
	}

	function pathNode(el, strPath, depth) {
		// var xpath = strPath.split(" ");
		// result = ""
		var val = simplify(strPath[depth]);
		if(el.nodeName.toLowerCase() == val[0].toLowerCase())
		{
			if(depth == (strPath.length - 1) )
			{
				// el.style.outline = '2px solid #090';
				// result = result + el.textContent.replace(/^\s+|\s+$/g, "");
				sim_data.push(el.textContent.replace(/^\s+|\s+$/g, ""));
			}
			else {
				depth = depth + 1;
				var children = el.childNodes;
				for (var i = 0; i < children.length; i++) {
					if(children[i].nodeType == 1){
						// result = result + pathNode(children[i], strPath, depth);
						pathNode(children[i], strPath, depth);
					}
				}
			}
		}
		// return result;
	}

	function pathToData(el, strPath, depth) {
		var val = simplify(strPath[depth]);
		var result = "";
		var regClass = el.className.replace(/\s\s+/g, ' ');
		// var regClass = el.className.replace(/  +/g, ' ');
		if( (el.nodeName.toLowerCase() == val[0].toLowerCase()) && (el.id == val[1]) && (regClass == val[2]) )
		{
			// console.log(val[0], val[1], val[2]);
			if(depth == (strPath.length - 1) ) {
				var stringx = el.textContent.replace(/^\s+|\s+$/g, "");
				result = result + stringx;
			}
			else {
				var children = el.childNodes;
				for (var i = 0; i < children.length; i++) {
					if(children[i].nodeType == 1) {
						result = result + pathToData(children[i], strPath, depth + 1);
					}
				}
			}
		}
		return result;
	}

	function pathToUrl(el, strPath, depth) {
		var val = simplify(strPath[depth]);
		var result = "";
		var regClass = el.className.replace(/\s\s+/g, ' ');
		// var regClass = el.className.replace(/  +/g, ' ');
		if( (el.nodeName.toLowerCase() == val[0].toLowerCase()) && (el.id == val[1]) && (regClass == val[2]) )
		{
			if(depth == (strPath.length - 1) ) {
				var resultx = "";
				if (el.querySelector('a')){
					resultx = el.querySelector('a').href;
					result = result + resultx;
				}
				else if(el.querySelector('img')){
					resultx = el.querySelector('img').src;
					result = result + resultx;
				}
				else{
					el = el.parentNode;
					while(el.nodeName.toLowerCase() !== 'html'){
						if((el.nodeName.toLowerCase() == 'a') && (el.href != "")){
							result = result + el.href;
							break;
						}
						else if((el.nodeName.toLowerCase() == "img") && (el.src != "")) {
							result = result + el.src;
							break;
						}
						else{
							el = el.parentNode;
						}
					}
				}
			}
			else {
				var children = el.childNodes;
				for (var i = 0; i < children.length; i++) {
					if(children[i].nodeType == 1) {
						result = result + pathToUrl(children[i], strPath, depth + 1);
					}
				}
			}
		}
		return result;
	}

	function getURL(el){
		var result = "";
		if (el.querySelector('a')){
			result = el.querySelector('a').href;
		}
		else if(el.querySelector('img')){
			result = el.querySelector('img').src;
		}
		else{
			while(el.nodeName.toLowerCase() !== 'html'){
				if((el.nodeName.toLowerCase() == 'a') && (el.href != "")){
					result = el.href;
					break;
				}
				else if((el.nodeName.toLowerCase() == "img") && (el.src != "")) {
					result = el.src;
					break;
				}
				else{
					el = el.parentNode;
				}
			}
		}
		return result;
	}

	/**
	 * MouseOver action for all elements on the page:
	*/
	function inspectorMouseOver(e) {
		// NB: this doesn't work in IE (needs fix):
		// var element = e.target;
		// var tagSelector;
		// Set outline:
		
		// If it is not green then change it to red
		if (e.target.style.outline != 'rgb(0, 153, 0) solid 2px')
		{
			e.target.style.outline = '2px solid #f00';
		}
		// If it is green then change it to yellow
		else {
			e.target.style.outline = '2px solid #ffff00';
		}
		// Set last selected element so it can be 'deselected' on cancel.
		// last = element;
		
		// tagSelector.tagName.showModalDialog()
	}
	
	
	/**
	 * MouseOut event action for all elements
	 */
	function inspectorMouseOut(e) {
		// Remove outline from element:

		// If it is Yellow change it to green when mouse out
		if(e.target.style.outline == 'rgb(255, 255, 0) solid 2px')
		{
			e.target.style.outline = '2px solid #090';
		}
		// Else if it is anything except green(which is red of course) then make it none
		else if (e.target.style.outline != 'rgb(0, 153, 0) solid 2px')
		{
			e.target.style.outline = '';
		}
		// e.target.style.outline = '2px solid #090';
	}
	
	/**
	 * Click action for hovered element
	 */
	function inspectorOnClick(e) {
		e.preventDefault();
		// If it is either yellow or green then do deselect by changing the color to ''.
		if( (e.target.style.outline == 'rgb(255, 255, 0) solid 2px') || (e.target.style.outline == 'rgb(0, 153, 0) solid 2px') ) {
			e.target.style.outline = '';
		}
		else {	
			// Color change to green
			e.target.style.outline = '2px solid #090';
			var cars = cssPath(e.target);
			// console.log(cars);

			// var array = [];
			// array.push(cars);
			var htmlTarget = document.getElementsByTagName("html")[0];
			xc = cars.split(" ");
			var xdata = e.target.textContent.replace(/^\s+|\s+$/g, "");
			// xdata = pathToData(htmlTarget, xc, 0);
			// array.push(xdata);

			// xurl = pathToUrl(htmlTarget, xc, 0);			
			// array.push(getURL(e.target));
			var xurl = getURL(e.target);
			// array.push(xurl);
			sim_data = [];
			pathNode(htmlTarget, xc, 0);
			var res_data = "";
			for (var i = 0; i < sim_data.length; i++) {
				if(sim_data[i] != ""){
					res_data = res_data + sim_data[i] + "&nsfw";
				}
			}
			res_data = res_data.slice(0, -5); 
			// array.push(res_data);
			// console.log(array);
			var final = "";
			final = cars + "&nspb" + xdata + "&nspb" + xurl + "&nspb" + res_data;
			console.log(final);
		}

		return false;
	}

	/**
	 * Add event listeners for DOM-inspectorey actions
	 */
	if ( document.addEventListener ) {
		document.addEventListener("mouseover", inspectorMouseOver, true);
		document.addEventListener("mouseout", inspectorMouseOut, true);
		document.addEventListener("click", inspectorOnClick, true);
		// document.addEventListener("keydown", inspectorCancel, true);
	} else if ( document.attachEvent ) {
		document.attachEvent("mouseover", inspectorMouseOver);
		document.attachEvent("mouseout", inspectorMouseOut);
		document.attachEvent("click", inspectorOnClick);
		// document.attachEvent("keydown", inspectorCancel);
	}

})(document);