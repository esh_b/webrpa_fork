"""Summary
"""
import os
import json

from django.core import mail
from django.apps import apps
from django.utils import timezone

from django.test import TestCase
from django.test import override_settings

from django.db.models.signals import post_save

from model_mommy import mommy

from email_sys.models import project_status_changed
from email_sys.models import user_details_changed
from email_sys.models import user_data_order_confirmed
from email_sys.models import user_profile_details_changed

from unittest.mock import MagicMock, patch, DEFAULT, mock_open

@override_settings(EMAIL_BACKEND='django.core.mail.backends.locmem.EmailBackend')
@override_settings(CELERY_TASK_ALWAYS_EAGER=True)
class TestEmail(TestCase):

    """Summary
    """

    @classmethod
    def setUpClass(cls):
        """Summary
        """
        super(TestEmail, cls).setUpClass()
        
        module_dir = os.path.dirname(__file__)
        json_filepath = os.path.join(module_dir, 'mappings.json')
        cls.mappings = {}
        with open(json_filepath, encoding="utf-8") as f:
            cls.mappings = json.load(f)        

    def test_account_activation(self):
        """Test ACCOUNT_ACTIVATION email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=user_details_changed)
            post_save.connect(mocked_handler, sender='accounts.User')

            # Assert the mock (mock of `user_details_changed`) was not called
            # before a new entry was added in `accounts_user` table
            self.assertFalse(mocked_handler.called)

            # Assert there are no pending emails in the outbox
            self.assertEqual(len(mail.outbox), 0)

            mommy.make('accounts.User', is_active=0)
            
            # Assert mock was called after new `User` model was saved
            self.assertTrue(mocked_handler.called)

            # Assert the arg `created` is True 
            #(implying mock was called only on row insert in the table)
            self.assertTrue(mocked_handler.call_args[1]['created'])

            # Assert the instance is the `User` Model
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='accounts', model_name='User'))

            # Assert there was an email sent
            self.assertEqual(len(mail.outbox), 1)

            # Assert whether the correct type of email was sent (in this case, ACCOUNT_ACTIVATION email)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['ACCOUNT_ACTIVATION']['sub'])

            post_save.disconnect(mocked_handler, sender='accounts.User')

    def test_registration_complete(self):
        """Test REGISTRATION_COMPLETE email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=user_details_changed)

            user = mommy.make('accounts.User', is_active=0)
            post_save.connect(mocked_handler, sender='accounts.User')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            user.is_active = 1
            user.save(update_fields=['is_active'])
            
            self.assertTrue(mocked_handler.called)
            self.assertFalse(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='accounts', model_name='User'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['REGISTRATION_COMPLETE']['sub'])

            post_save.disconnect(mocked_handler, sender='accounts.User')

    def test_password_changed(self):
        """Test PASSWORD_CHANGED email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=user_details_changed)

            user = mommy.make('accounts.User', is_active=1)
            post_save.connect(mocked_handler, sender='accounts.User')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            user.save(update_fields=['password'])
            
            self.assertTrue(mocked_handler.called)
            self.assertFalse(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='accounts', model_name='User'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['PASSWORD_CHANGED']['sub'])

            post_save.disconnect(mocked_handler, sender='accounts.User')

    def test_account_activation_email_change(self):
        """Test ACCOUNT_ACTIVATION email (when email was changed)
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=user_details_changed)

            user = mommy.make('accounts.User', is_active=1)
            post_save.connect(mocked_handler, sender='accounts.User')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            user.is_active = 0
            user.save(update_fields=['is_active'])
            
            self.assertTrue(mocked_handler.called)
            self.assertFalse(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='accounts', model_name='User'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['ACCOUNT_ACTIVATION']['sub'])

            post_save.disconnect(mocked_handler, sender='accounts.User')

    def test_email_changed(self):
        """Test EMAIL_CHANGED email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=user_details_changed)

            user = mommy.make('accounts.User', is_active=0)
            post_save.connect(mocked_handler, sender='accounts.User')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            user.is_active = 1
            user.old_email = "abc123@example.com"
            user.save(update_fields=['is_active', 'old_email'])
            
            self.assertTrue(mocked_handler.called)
            self.assertFalse(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='accounts', model_name='User'))
            self.assertEqual(len(mail.outbox), 2)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['EMAIL_CHANGED']['sub'])

            post_save.disconnect(mocked_handler, sender='accounts.User')

    def test_account_info_changed(self):
        """Test ACCOUNT_INFO_CHANGED email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=user_profile_details_changed)

            user_profile = mommy.make('accounts.Profile')
            post_save.connect(mocked_handler, sender='accounts.Profile')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            user_profile.first_name = "First"
            user_profile.last_name = "Last"
            user_profile.save(update_fields=['first_name', 'last_name'])
            
            self.assertTrue(mocked_handler.called)
            self.assertFalse(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='accounts', model_name='Profile'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['ACCOUNT_INFO_CHANGED']['sub'])

            post_save.disconnect(mocked_handler, sender='accounts.Profile')

    def test_data_order_confirmed(self):
        """Test DATA_ORDER_CONFIRMED email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=user_data_order_confirmed)
            post_save.connect(mocked_handler, sender='accounts.Data_Order')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            mommy.make('accounts.Data_Order', email="abc123@example.com", created_at=timezone.now())
            
            self.assertTrue(mocked_handler.called)
            self.assertTrue(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='accounts', model_name='Data_Order'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['DATA_ORDER_CONFIRMED']['sub'])

            post_save.disconnect(mocked_handler, sender='accounts.Data_Order')

    def test_exec_status_failure(self):
        """Test EXEC_STATUS_FAILURE email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=project_status_changed)
            post_save.connect(mocked_handler, sender='scraping.Execution')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            project = mommy.make('scraping.Project', mail_alert=True)
            mommy.make('scraping.Execution', status=0, project=project)
            
            self.assertTrue(mocked_handler.called)
            self.assertTrue(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='scraping', model_name='Execution'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['EXEC_STATUS_FAILURE']['sub'])

            post_save.disconnect(mocked_handler, sender='scraping.Execution')

    def test_exec_status_success(self):
        """Test EXEC_STATUS_SUCCESS email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=project_status_changed)
            post_save.connect(mocked_handler, sender='scraping.Execution')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            project = mommy.make('scraping.Project', mail_alert=True, keyword='')
            sample_output_file_str = "[1, [[{\"key\": [\"a\", \"b\"]}]], 0]"
            with patch('email_sys.views.open', new=mock_open(read_data=sample_output_file_str)) as m_open:
                with patch('email_sys.views.os.path.getsize', return_value=10) as m_os:
                    execution = mommy.make('scraping.Execution', status=True, project=project)
            
            self.assertTrue(mocked_handler.called)
            self.assertTrue(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='scraping', model_name='Execution'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['EXEC_STATUS_SUCCESS']['sub'])

            post_save.disconnect(mocked_handler, sender='scraping.Execution')

    def test_keyword_hitting(self):
        """Test ALERT_KEYWORD_HITTING email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=project_status_changed)
            post_save.connect(mocked_handler, sender='scraping.Execution')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            project = mommy.make('scraping.Project', mail_alert=True, keyword_alert=True, keyword='football')
            sample_output_file_str = "[1, [[{\"key\": [\"football\", \"b\"]}]], 0]"
            with patch('internal.xpath.open', new=mock_open(read_data=sample_output_file_str)) as m_open:
                with patch('email_sys.views.os.path.getsize', return_value=10) as m_os:
                    execution = mommy.make('scraping.Execution', status=True, project=project)
            
            self.assertTrue(mocked_handler.called)
            self.assertTrue(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='scraping', model_name='Execution'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['ALERT_KEYWORD_HITTING']['sub'])

            post_save.disconnect(mocked_handler, sender='scraping.Execution')

    def test_data_difference(self):
        """Test ALERT_DATA_DIFFERENCE email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=project_status_changed)

            project = mommy.make('scraping.Project', mail_alert=True, data_alert=True, keyword='')
            execution_1 = mommy.make('scraping.Execution', status=True, project=project, date_executed=timezone.now())
            post_save.connect(mocked_handler, sender='scraping.Execution')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            sample_output_1 = "[1, [[{\"key\": [\"a\", \"b\"]}]], 0]"
            sample_output_2 = "[1, [[{\"key\": [\"c\", \"b\"]}]], 0]"
            with patch('internal.xpath.open', new_callable=mock_open, 
                                    read_data=str.encode(sample_output_1)) as m_open:
                m_open.side_effect = (m_open.return_value, mock_open(read_data=str.encode(sample_output_2)).return_value,)
                with patch('email_sys.views.os.path.getsize', return_value=10) as m_os:
                    execution_2 = mommy.make('scraping.Execution', status=True, project=project, date_executed=timezone.now())
            
            self.assertTrue(mocked_handler.called)
            self.assertTrue(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='scraping', model_name='Execution'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['ALERT_DATA_DIFFERENCE']['sub'])

            post_save.disconnect(mocked_handler, sender='scraping.Execution')

    def test_keywordhit_and_datadiff(self):
        """Test ALERT_DATA_KEYWORD email
        """
        with patch('email_sys.tests.post_save.receivers', []) as receivers:
            mocked_handler = MagicMock(side_effect=project_status_changed)

            project = mommy.make('scraping.Project', mail_alert=True, data_alert=True, keyword_alert=True, keyword='football')
            execution_1 = mommy.make('scraping.Execution', status=True, project=project, date_executed=timezone.now())
            post_save.connect(mocked_handler, sender='scraping.Execution')

            self.assertFalse(mocked_handler.called)
            self.assertEqual(len(mail.outbox), 0)

            sample_output_1 = "[1, [[{\"key\": [\"a\", \"b\"]}]], 0]"
            sample_output_2 = "[1, [[{\"key\": [\"football\", \"b\"]}]], 0]"
            with patch('internal.xpath.open', new_callable=mock_open, read_data=str.encode(sample_output_1)) as m_open:
                m_open.side_effect = (m_open.return_value, mock_open(
                    read_data=str.encode(sample_output_2)).return_value, mock_open(read_data=sample_output_2).return_value,)
                with patch('email_sys.views.os.path.getsize', return_value=10) as m_os:
                    execution_2 = mommy.make('scraping.Execution', status=True, project=project, date_executed=timezone.now())
            
            self.assertTrue(mocked_handler.called)
            self.assertTrue(mocked_handler.call_args[1]['created'])
            self.assertIsInstance(mocked_handler.call_args[1]['instance'], 
                        apps.get_model(app_label='scraping', model_name='Execution'))
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 
                    self.mappings['ALERT_DATA_KEYWORD']['sub'])

            post_save.disconnect(mocked_handler, sender='scraping.Execution')
