"""Summary

Attributes:
    Sched_type (Enum): Description
"""
import json
import pytz
import croniter
from enum import Enum
from datetime import datetime, timedelta

from django.utils import timezone
from django.db import transaction
from dateutil.relativedelta import relativedelta

from api_v1.scraping.models import Project
from django_celery_beat.models import CrontabSchedule, PeriodicTask, IntervalSchedule

from api_v1.scraping.tasks import task_scrape

Sched_type = Enum('Sched_type', [('NO_SCHED', '0'),
                                 ('EVERY_N_DAYS', '1'),
                                 ('EVERY_WEEK', '2'),
                                 ('EVERY_MONTH', '3'),
                                ])

def get_next_run_cron(cron: CrontabSchedule) -> datetime:
    """Get next run given a cron
    
    Args:
        cron (CrontabSchedule): Description
    
    Returns:
        datetime: Description
    """
    cron_str = ' '.join([cron.minute, cron.hour, cron.day_of_month, cron.month_of_year, cron.day_of_week])
    cur_time = timezone.now().astimezone(cron.timezone)
    next_time = croniter.croniter(cron_str, cur_time).get_next(datetime)
    return next_time.astimezone(timezone.get_current_timezone())

def get_curdate_given_time(hour: int, minute: int) -> datetime:
    """Function returning `last_run_at` which is always today at the given hour and minute
    Assumption:
        `every n days` will start n days from today. 
        So `last_run_at` for Interval schedule is always today at given time.
    
    Args:
        hour (int): Hour of scheduled time
        minute (int): Minute of scheduled time
    
    Returns:
        datetime: The last_run_at datetime for the current schedule
    """
    return timezone.localtime().replace(hour=hour, minute=minute, second=0, microsecond=0)

def update_or_create_schedule(user_id: int, pid: str, sched_type: str, 
                                    sched_value: str, sched_time: str) -> bool:
    """Function to update or create scheduling tasks (if any) using crontab and intervalschedule for a project
    
    Args:
        user_id (int): User id
        pid (str): Project id
        sched_type (str): schedule type
        sched_value (str): The scheduled value (every n days or week-index or month-date)
        sched_time (str): scheduled time (str) of the format(HH:MM)
    
    Returns:
        bool: Returns True if schedule was setup successfully
    """
    is_scheduled = True
    project = Project.objects.get(id=pid)

    #Set the cron field values according the schedule type
    if(sched_type == Sched_type.NO_SCHED.value):
        is_scheduled = False
    elif(sched_type == Sched_type.EVERY_N_DAYS.value):
        hour, minute = [int(val) for val in sched_time.split(":")]
        cron_day_of_week = '*'
        cron_day_of_month = '*'
        last_run_at = get_curdate_given_time(hour, minute)
        interval, created = IntervalSchedule.objects.get_or_create(every=int(sched_value), period=IntervalSchedule.DAYS)
        crontab, created = CrontabSchedule.objects.get_or_create(minute=minute, hour=hour,
                                                        day_of_week=cron_day_of_week, day_of_month=cron_day_of_month,
                                                        timezone=timezone.get_current_timezone())
    elif(sched_type == Sched_type.EVERY_WEEK.value):
        hour, minute = [int(val) for val in sched_time.split(":")]
        cron_day_of_week = int(sched_value)
        last_run_at = None
        cron_day_of_month = '*'
        interval = None
        crontab, created = CrontabSchedule.objects.get_or_create(minute=minute, hour=hour,
                                                        day_of_week=cron_day_of_week, day_of_month=cron_day_of_month,
                                                        timezone=timezone.get_current_timezone())
    elif(sched_type == Sched_type.EVERY_MONTH.value):
        hour, minute = [int(val) for val in sched_time.split(":")]
        cron_day_of_week = '*'
        cron_day_of_month = int(sched_value)
        last_run_at = None
        interval = None
        crontab, created = CrontabSchedule.objects.get_or_create(minute=minute, hour=hour,
                                                        day_of_week=cron_day_of_week, day_of_month=cron_day_of_month,
                                                        timezone=timezone.get_current_timezone())
    else:
        return False

    if(is_scheduled):
        #Periodic task (scheduled task) is named as "<userid>_<projectid>"
        periodic_task_name = str(user_id) + "_" + pid
        task_args = [user_id, pid]

        try:
            with transaction.atomic():
                #Create or retrieve the existing PeriodicTask instance for the given user and project
                periodic_task, created = PeriodicTask.objects.get_or_create(name=periodic_task_name, task=task_scrape.name, 
                                                                args=json.dumps(task_args))

                periodic_task.crontab = crontab
                periodic_task.interval = interval
                periodic_task.last_run_at = last_run_at
                #Add the date_changed auto_now field in the update_fields to update the changed date accordingly.
                periodic_task.save(update_fields={'crontab', 'interval', 'last_run_at', 'date_changed'})

                #Reference the PeriodicTask instance to the current project
                project.periodic_task = periodic_task
                project.save(update_fields={'periodic_task'})
        except Exception as e:
            print("SCHEDULE EXCEPTION:", e)
            return False
    else:
        if(project.periodic_task is None):
            return True

        try:
            #Set the periodic_task field in the Project as NULL and delete the corresponding PeriodicTask instance
            with transaction.atomic():
                periodic_task_name = project.periodic_task.name
                project.periodic_task = None
                project.save(update_fields={'periodic_task'})
                PeriodicTask.objects.get(name=periodic_task_name).delete()
        except Exception as e:
            print("SCHEDULE EXCEPTION:", e)
            return False
    return True
