import re





def child_tags(a):

    tag = []

    c = 0

    for i in a:

        b = str(a[c])

        if '<' == b[0] and '!' != b[1]:

            temp = re.split(r'[^\w]', b)

            tag.append(temp[1])

        c = c + 1

    return tag





def immediate_tags(a):

    tag = []

    c = 0

    for i in a:

        b = str(a[c])

        if '<' == b[0] and '!' != b[1]:

            tag.append(a[c])

        c = c + 1

    return tag





def get_tag(tag_data):

    b = str(tag_data)

    temp = re.split(r'[^\w]', b)

    return temp[1]





def row_function(row_tag):

    temp = immediate_tags(row_tag.contents)

    table_data = []

    ind = []

    for i in range(0, len(temp)):

        if get_tag(temp[i]) == 'th':

            try:

                col = int(temp[i]["colspan"])

            except (ValueError, KeyError) as e:

                col = 1

            for j in range(0,col):

                term = ''

                for string in temp[i].stripped_strings:

                    term = term + repr(string)

                table_data.append(term)

#                table_data.append(temp[i].string)

                ind.append(0)

        elif get_tag(temp[i]) == 'td':

            term = ''

            for string in temp[i].stripped_strings:

                term = term + repr(string)

            table_data.append(term)

#            table_data.append(temp[i].string)

            ind.append(1)



    return table_data, ind





def description_list_data(list_code):

    list_contents = immediate_tags(list_code.contents)

    term = []

    term_data = []

    for i in range(0, len(list_contents)):

        if get_tag(list_contents[i]) == 'dt':

            for string in list_contents[i].stripped_strings:

                term.append(repr(string))

#            term.append(list_contents[i].string)

        elif get_tag(list_contents[i]) == 'dd':

            for string in list_contents[i].stripped_strings:

                term_data.append(repr(string))

#            term_data.append(list_contents[i].string)



    return term, term_data





def unordered_list_data(list_code):

    list_contents = immediate_tags(list_code.contents)

    data = []

    for i in range(0, len(list_contents)):

        if get_tag(list_contents[i]) == 'li':

            for string in list_contents[i].stripped_strings:

                data.append(repr(string))

#            data.append(list_contents[i].string)

    return data