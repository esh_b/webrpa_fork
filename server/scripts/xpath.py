import os
import time, sys
import traceback

from lxml import etree
from lxml.etree import _Element as Element
from io import StringIO
from bs4 import Tag, BeautifulSoup as B
from cryptography.fernet import Fernet
from rpalib.scraping import Project, PageInfo, ActionInfo
from urllib.parse import urljoin, urlparse
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from rpalib.scraping import Project, PageInfo, ActionInfo, save_result, get_project, scrape_schedule
from rpalib import PigDataException
from rpalib.scheduling import get_upcoming_projects
from rpalib.core import Session
import json


class Extraction():
    def __init__(self):
        # # Driver initiation
        options = Options()
        options.add_argument("--headless")  # Runs Chrome in headless mode.
        options.add_argument('--no-sandbox')  # # Bypass OS security model
        # options.add_argument('--disable-gpu')  # applicable to windows os only
        options.add_argument('start-maximized')
        options.add_argument('disable-infobars')
        options.add_argument("--disable-extensions")
        options.add_argument("--window-size=1920x1080")

        prefs = {'profile.managed_default_content_settings.images': 2, 'disk-cache-size': 4096}
        options.add_experimental_option('prefs', prefs)
        chrome_driver = 'internal/chromedriver'

        self.driver = webdriver.Chrome(chrome_options=options, executable_path=chrome_driver)
        self.delay = 4  # seconds

        self.domain_url = ''
        self.domain_url2 = ''
        self.words = []
        # self.soup = B('', 'html.parser')
        self.tree = etree.parse(StringIO('<html></html>'))
        self.table_flag = 0
        self.multi_table = 0
        self.output_list = []
        self.num = 0
        self.result = []

        self.sim_depth = -1
        self.sim_words = []
        self.download_flag = 0
        self.text_flag = 0
        self.mimeval = ''
        self.goback_url = ''
        self.goback_active = 0

        # -------- New Variables --------
        self.path1 = []
        self.path2 = []
        self.class_attrib = []
        self.sim_xpath = ''
        self.current_page_url = ''
        self.pagination_flag = 0

    def _find_ele(self, xpath: str, flag: int):
        """
        Function which find the element from the given xpath with EC delay attribute.

        :param xpath: XPATH is a path to a certain node in a tree representation
        :param flag: Depth to track the inputDOM path
        :param flag:    flag= 0 => For Fill-in type,
                        flag= 1 => For Click Action
        :return: Outputs an element node found by traversing the tree using the given XPATH.
        :rtype: WebElement (selenium.webdriver.remote.webelement.WebElement)
        """
        try:
            # element = WebDriverWait(self.driver, self.delay).until(EC.presence_of_element_located((By.XPATH, xpath)))
            # element = WebDriverWait(self.driver, self.delay).until(EC.visibility_of_element_located((By.XPATH, xpath)))
            if flag == 0:
                element = WebDriverWait(self.driver, self.delay).until(
                    EC.visibility_of_element_located((By.XPATH, xpath)))
            else:
                # element = WebDriverWait(self.driver, self.delay).until(EC.element_to_be_clickable((By.XPATH, xpath)))
                element = WebDriverWait(self.driver, self.delay).until(
                    EC.presence_of_element_located((By.XPATH, xpath)))

        except NoSuchElementException as e:  # The element node is not found, as in it does not exists in the given page orthe page isn't loaded properly
            print(traceback.print_exc())
            return False

        except TimeoutException as ex:  # Timeout exception is reached in finding for an element node
            print(traceback.print_exc())
            return False

        return element

    def _fill_action(self, xpath: str, text: str):
        """
        Function which fills the given text into the node from the given xpath traversal.

        :param xpath: XPATH is a path to a certain node in a tree representation
        :param text: Simple text string
        """
        element = self._find_ele(xpath, 0)
        if element:
            element.send_keys(text)

    def _click_action(self, xpath: str):
        """
        Function which activates click action on an element from xpath traversal

        :param xpath: XPATH is a path to a certain node in a tree representation
        """
        element = self._find_ele(xpath, 1)
        # element = driver._find_element(By.XPATH, xpath)
        if element:
            try:
                # print("Normal click!", xpath)
                element.click()
                return True
            except:
                try:
                    # print("Click using Javascript")
                    self.driver.execute_script("arguments[0].click();", element)
                    return True
                except:
                    try:
                        # print("Keyboard Enter")
                        return False
                        element.send_keys(Keys.RETURN)
                    except:
                        print(traceback.print_exc())
                        return False
        else:
            return False

    def _filter_text(self, text: str):

        """
        Function which returns formatted text by removing an unnecessary encodings

        :param text: Input text string
        :return: Formatted string
        :rtype: str
        """
        if text == None:
            text = ''
        else:
            text = text.replace(u'\\u3000', u' ')
            text = text.replace(u'\\xa0', u' ')
            text = text.replace(u'\\n', u' ')
            text = ' '.join(text.split())

        return text

    def _domain_name(self, link: str):
        """
        Function which returns formatted link/ URL by adding the domain name if it's not present.

        :param link: Extracted URL
        :return: Formatted URL
        :rtype: str
        """

        # Absolute URL
        if (link.startswith("//")):
            return "http:" + link

        parsed_url = urlparse(link)

        # If the link is http/https relative URL, urljoin with the page URL
        if (not (parsed_url.scheme == 'data') and not (parsed_url.netloc)):
            link = urljoin(self.current_page_url, link)
        return link

        """
        if link[0:5] == 'data:':
            return link
        if link[0:4] != 'http' and link[0] == '/' and link != '':
            link = self.domain_url + link[1:]
        elif link[0:4] != 'http' and link[0] != '/' and link != '':
            link = self.domain_url + link

        return link
        """

    def _get_text(self, r: Element):
        """
        Function which extracts text from the given node.

        :param r: Node element (lxml.etree._Element)
        """
        self.output_list.append(self._filter_text(''.join(r.itertext())))

    def _get_url(self, r: Element):
        """
        Function which extracts URL from the given node.

        :param r: Element Node (lxml.etree._Element)
        """
        try:
            href = r.attrib['href']
            self.output_list.append(self._domain_name(href))
        except:
            href_flag = 0
            for a in r.findall('a'):
                try:
                    href = a.attrib['href']
                    self.output_list.append(self._domain_name(href))
                    href_flag = 1
                    break
                except:
                    pass
            if href_flag == 0:
                node = r
                while 1:
                    node = node.getparent()
                    if node.tag == 'a':
                        try:
                            href = node.attrib['href']
                            self.output_list.append(self._domain_name(href))
                            break
                        except:
                            pass
                    if node.tag == 'html' or node.tag == 'body' or node.tag == 'head':
                        print("Top of tree reached, URL cannot be found!!!")
                        break

    def _get_imgurl(self, r: Element):
        """
        Function which extracts IMG_URL from the given node.

        :param r: Element Node (lxml.etree._Element)
        """
        try:
            src = r.attrib['src']
            self.output_list.append(self._domain_name(src))
        except:
            for img in r.findall('img'):
                try:
                    src = img.attrib['src']
                    self.output_list.append(self._domain_name(src))
                except:
                    pass

    def _get_download_link(self, r: Element):
        """
        Function which extracts download link URL from the given node.

        :param r: Element Node (lxml.etree._Element)
        """
        try:
            src = r.attrib['src']
            self.output_list.append(self._domain_name(src))
        except:
            try:
                href = r.attrib['href']
                self.output_list.append(self._domain_name(href))
            except:
                for img in r.findall('img'):
                    try:
                        src = img.attrib['src']
                        self.output_list.append(self._domain_name(src))
                        return
                    except:
                        pass

                self._get_url(r)

    def _get_path_list(self, xpath: str):
        """
        Function splits xpath string into list

        :param xpath: XPATH is a path to a certain node in a tree representation
        :return: List of strings, splitting xpath by '/'.
        :rtype: list
        """
        if xpath == None:
            return []
        elif xpath == '':
            return []
        else:
            path = []
            text = ''
            for val in xpath.split("/"):
                if val == '':
                    text += '/'
                else:
                    path.append(text + val)
                    text = ''
            return path

    def _depth_count(self):
        """
        Function calculates the depth to the common node from specific id (or) root of the tree.

        :return: Integer depth count for common node of self.path1 and self.path2
        :rtype: int
        """
        count = 0
        if len(self.path1) == len(self.path2):
            self.class_attrib = [None] * len(self.path1)
            for i in range(0, len(self.path1)):
                if self.path1[i] == self.path2[i]:
                    count += 1
                else:
                    break
        else:
            print("Given paths are not similar")
            count = -1
        return count

    def _get_class_attrib(self, recur_path1: str, recur_path2: str, depth: int):
        """
        Recursive function call to store all common classes b/w recur_path1 and recur_path2, in a dictionary variable self.class_attrib.

        :param recur_path1: Incremental path of self.path1 from common node to the end node.
        :param recur_path2: Incremental path of self.path2 from common node to the end node.
        :param depth: Depth variable to track the path from common node to the end node.
        """
        if len(self.path1) != depth:
            recur_path1 = recur_path1 + '/' + self.path1[depth]
            recur_path2 = recur_path2 + '/' + self.path2[depth]

            path1_class = self.tree.xpath(recur_path1)[0].get("class")
            if path1_class != None:
                self.class_attrib[depth] = path1_class if path1_class == self.tree.xpath(recur_path2)[0].get(
                    "class") else None
            else:
                self.class_attrib[depth] = None
            self._get_class_attrib(recur_path1, recur_path2, depth + 1)

    def _get_similar_path(self, node: Element, recur_path: str, depth: int, flag: int):
        """
        Function to traverse the etree recurrsively to find similar items from the common node.

        :param node: Current node being traversed
        :param recur_path: Incremental path of self.path1 from common node to the end node.
        :param depth: Depth variable to track the path from common node to the end node.
        :param flag:    flag= 4 => Similar Text,
                        flag= 5 or 10 => Similar URL,
                        flag= 6 => Similar IMG_URL
        """
        if self.sim_depth == len(self.path1):  # Impossible case
            x = '/'.join(i for i in self.path1)
            xnode = self.tree.xpath(x)
            for val in xnode:
                if flag == 4:  # Node Text Extraction
                    self._get_text(val)
                elif flag == 5 or flag == 10:  # Node URL Extraction
                    self._get_url(val)
                elif flag == 6:  # Node IMG_URL extraction
                    self._get_imgurl(val)
        elif len(self.path1) == depth:  # Extraction Node reached
            if flag == 4:  # Node Text Extraction
                self._get_text(node)
            elif flag == 5 or flag == 10:  # Node URL Extraction
                self._get_url(node)
            elif flag == 6:  # Node IMG_URL extraction
                self._get_imgurl(node)
        else:
            recur_path = recur_path + '/' + self.path1[depth]
            recur_node = self.tree.xpath(recur_path)[0]
            for ele in node.iterchildren("*"):
                if ele.tag == recur_node.tag and (
                        self.class_attrib[depth] == None or ele.get("class") == self.class_attrib[depth]):
                    self._get_similar_path(ele, recur_path, depth + 1, flag)

    def _get_similar_download(self, node: Element, recur_path: str, depth: int, flag: int):
        """
        Function to traverse the etree recurrsively to find similar items from the root node.

        :param node: Current node being traversed
        :param recur_path: Incremental path of self.path1 from root node(html) to the end node.
        :param depth: Depth variable to track the path from root node to the end node.
        :param flag:    flag= 12 => Similar Download Link/ URL,
        """
        if len(self.path1) == depth:  # Extraction Node reached
            # Something
            if flag == 12:  # Node Download_URL Extraction
                self._get_download_link(node)
        else:
            recur_path = recur_path + '/' + self.path1[depth]
            recur_node = self.tree.xpath(recur_path)[0]
            for ele in node.iterchildren("*"):
                if ele.tag == recur_node.tag:
                    self._get_similar_download(ele, recur_path, depth + 1, flag)

        # ------------------------------------------- TABLE IMPLEMENTATION

    def _get_row(self, row: Element):
        """
        Function to get the row data of a given row _Element.

        :param row: Given row Element for which the column values is to be extracted
        :return: Outputs list of extracted column data based on the given row _Element.
        :rtype: list
        """
        output_row = []
        for col in row.iterchildren("*"):
            if col.tag == 'th' or col.tag == 'td':
                col_table = col.findall('table')
                if len(col_table) > 0:
                    temp = []
                    for trow in col.iter('tr'):
                        temp.append(self._get_row(trow))
                    if len(temp) <= 1:
                        for i in temp[0]:
                            output_row.append(self._filter_text(i))
                    else:
                        try:
                            zip_val = list(zip(*temp))
                            zip_len = len(zip_val[0])
                            for i in range(0, len(zip_val[0])):
                                output_row.append(' '.join(qwe[i] for qwe in zip_val))
                        except:
                            for i in temp[0]:
                                output_row.append(self._filter_text(i))
                else:
                    output_row.append(self._filter_text(''.join(col.itertext())))
                try:
                    colspan = int(col.attrib['colspan'])
                    for i in range(0, colspan - 1):
                        output_row.append('')
                except:
                    pass

        return output_row

    def _get_full_table(self, node: Element):
        """
        Function to get the row data of a given row _Element.

        :param node: Table node _Element for which data is to be extracted.
        :return: Outputs list of extracted row data based on the given table node _Element.
        :rtype: list
        """
        output_table = []
        while node.tag.lower() != "html":
            if node.tag.lower() == 'table':
                for ele in node.iterchildren("*"):
                    if ele.tag == 'thead' or ele.tag == 'tbody' or ele.tag == 'tfoot':
                        for row in ele.iterchildren("*"):
                            if row.tag == 'tr':
                                output_table.append(self._get_row(row))
                    elif ele.tag == 'tr':
                        output_table.append(self._get_row(ele))
                break
            else:
                node = node.getparent()
        return output_table

        # --------------------------END

    def _abs_to_relative(self, xpath):
        node = self.tree.xpath(xpath)[-1]

        xpath_list = self._get_path_list(xpath)
        path_count = len(xpath_list)

        relative_path = ''
        while path_count != 0:
            if self._get_idval(node.get('id')) != '':
                # something
                relative_path = '//*[@id="' + self._get_idval(node.get('id')) + '"]' + '/' + relative_path
                break
            else:
                node = node.getparent()
                relative_path = xpath_list[path_count - 1] + '/' + relative_path
                path_count -= 1
        return relative_path[:-1]

    def _get_class(self, class_name: str):
        if class_name == None:
            return ""
        else:
            return (' ').join(class_name.split())

    def _get_idval(self, id_name: str):
        if id_name == None:
            return ""
        else:
            return id_name

    def _get_tag_attr(self, str1: str):
        """
        Function on self.full_tag string input gives all the node attributes such as tag_name, id_name and class_name
        :param str1: A string as an input (Eg: div#uiop.qwe.cvb.asd).
        :return: A list of tag_name, id_name and class_name => ["div", "uiop", "qwe cvb asd"].
        :rtype: list
        """
        arr = str1.split("#")
        xarr = []
        if len(arr) < 2:
            id_name = ''
            arr2 = arr[0].split(".")
            tag_name = arr2[0]
            xarr = arr2
        else:
            tag_name = arr[0]
            arr2 = arr[1].split(".")
            id_name = arr2[0]
            xarr = arr2
        class_name = ''
        if len(xarr) < 2:
            class_name = ' '
        elif len(xarr) < 3:
            class_name = xarr[1] + ' '
        else:
            for k in range(1, len(xarr)):
                class_name = class_name + xarr[k] + ' '
        class_name = class_name[:-1]
        return [tag_name, id_name, class_name]

    def _verify_path(self, xpath: str, action_type: int):
        if action_type == 9:
            return None
        else:
            r = self.tree.xpath(xpath)
            if len(r) == 0:
                print("PATH verification: Failed!!")
                return None
            elif len(r) == 1:
                return r[0]
            elif len(
                    r) > 1:  # Multiple paths are found but we proceed with r[0] -> first element (last element in case of pagination)
                print("PATH verification: Multiple paths exists")
                return r[0]

    def _node_lookup(self, node: Element, csspath_list: list, xpath_list: list, depth: list, action_type: int):
        # Top to Bottom: Tree recursion
        if depth == len(csspath_list) - 1:
            return self.tree.getpath(node)
        else:
            css_node = self._get_tag_attr(csspath_list[depth + 1])
            try:
                if action_type != 9:
                    if depth > (len(csspath_list) - len(xpath_list) - 2):
                        xval = xpath_list[depth - (len(csspath_list) - len(xpath_list)) + 1]
                        child_list = node.findall('.' + xval) if "//*[@id=" in xval else node.xpath('.//' + xval)
                        if len(child_list) != 0:
                            if action_type == 9:
                                child = child_list[-1]
                            else:
                                child = child_list[0]
                            if (child.tag.lower() == 'html' and css_node[0].lower() == 'html') or (
                                    child.tag.lower() == 'body' and css_node[0].lower() == 'body'):
                                return self._node_lookup(child, csspath_list, xpath_list, depth + 1, action_type)
                            elif self._get_class(child.get('class')) == css_node[2]:
                                return self._node_lookup(child, csspath_list, xpath_list, depth + 1, action_type)
            except:
                print("XPATH ERROR: child cannot be found!")

            ele_list = []
            for ele in node.iterchildren("*"):
                if (ele.tag.lower() == 'html' and css_node[0].lower() == 'html') or (
                        ele.tag.lower() == 'body' and css_node[0].lower() == 'body'):
                    return self._node_lookup(ele, csspath_list, xpath_list, depth + 1, action_type)
                elif (ele.tag.lower() == css_node[0].lower()) and (
                        self._get_class(ele.get('class')) == css_node[2]) and (
                        self._get_idval(ele.get('id')) == css_node[1]):
                    ele_list.append(ele)
            if len(ele_list) > 0:
                if action_type == 9:
                    return self._node_lookup(ele_list[-1], csspath_list, xpath_list, depth + 1, action_type)
                else:
                    return self._node_lookup(ele_list[0], csspath_list, xpath_list, depth + 1, action_type)
            else:
                ele_list = []
                for ele in node.iterchildren("*"):
                    if (ele.tag.lower() == css_node[0].lower()) and (self._get_class(ele.get('class')) == css_node[2]):
                        ele_list.append(ele)
                if len(ele_list) > 0:
                    if action_type == 9:
                        return self._node_lookup(ele_list[-1], csspath_list, xpath_list, depth + 1, action_type)
                    else:
                        return self._node_lookup(ele_list[0], csspath_list, xpath_list, depth + 1, action_type)
                else:
                    print("Unknown error: node not found in _node_lookup()!")
                return ""

    def _find_path(self, ele_list: list, csspath_list: list, xpath_list: list, depth: int, action_type: int):
        # Bottom to Top: Tree recursion
        node_list = []
        if len(ele_list) == 0:
            print("Path not found!!")
            return ""
        if len(ele_list) == 1:
            return self._node_lookup(ele_list[0], csspath_list, xpath_list, depth, action_type)
        elif depth == 0:
            if action_type == 9:
                return self._node_lookup(ele_list[-1], csspath_list, xpath_list, depth, action_type)
            else:
                return self._node_lookup(ele_list[0], csspath_list, xpath_list, depth, action_type)
        else:
            css_node = self._get_tag_attr(csspath_list[depth - 1])
            for ele in ele_list:
                parent = ele.getparent()
                if (parent.tag.lower() == 'html' and css_node[0].lower() == 'html') or (
                        parent.tag.lower() == 'body' and css_node[0] == 'body'):
                    node_list.append(parent)
                elif parent.tag.lower() == css_node[0].lower():  # Making sure nodes have same tag_name
                    if self._get_class(parent.get('class')) == css_node[2]:  # Making sure nodes have same class_name
                        node_list.append(parent)

            return self._find_path(node_list, csspath_list, xpath_list, depth - 1, action_type)

    def _get_xpath(self, csspath: str, xpath: str, action_type: int):
        node = self._verify_path(xpath, action_type)
        if node != None:
            return self.tree.getpath(node)
        else:
            csspath_list = csspath.split()
            xpath_list = self._get_path_list(xpath)
            root = self.tree.getroot()

            if action_type != 9:
                if len(xpath_list) != len(csspath_list) and len(xpath_list) > 1:
                    if xpath_list[0] != '/html':  # Starting is an 'id'
                        nodex = root.findall('.//' + '/'.join(xpath_list[1:]))
                        if len(nodex) != 0:
                            for node in nodex:
                                idnode = node.getparent()
                                attr = self._get_tag_attr(csspath[len(csspath_list) - len(xpath_list)])
                                if idnode.tag == attr[0]:  # Why not equate class_name also?
                                    return self.tree.getpath(idnode)

                    else:
                        print("It's not an `id` and yet length of xpath and csspath are not same!!")
                        return ""
            # # Use of CSSPath to compute XPATH
            nodex = self._get_tag_attr(csspath_list[-1])

            e_list = root.findall(".//" + nodex[0])
            ele_list = []
            for e in e_list:
                if self._get_class(e.get('class')) == nodex[2]:
                    ele_list.append(e)
            if len(ele_list) > 0:
                return self._find_path(ele_list, csspath_list, xpath_list, len(csspath_list) - 1, action_type)
            return self._find_path(e_list, csspath_list, xpath_list, len(csspath_list) - 1, action_type)

        # --------------------------END

    def _classify_action(self, act: ActionInfo):
        """
        Function call to perform an action on a given page.

        :param page_url: URL of the given page.
        :param action: ActionInfo() class instance. Contains information on the action to be performed on the given page.
        :return: Outputs list of extracted data based on the info from ActionInfo().
        :rtype: list
        """
        self.output_list = []

        if act.action_type < 15:

            if act.action_type != 10 and (act.action_type < 4 or act.action_type > 6):
                r_xpath = self._get_xpath(act.path[0], act.path[1], act.action_type)
                if r_xpath != '':
                    self.text_flag = 1
                    r = self.tree.xpath(r_xpath)
                    if act.action_type == 1:  # Simple Text Extraction
                        self._get_text(r[0])
                    elif act.action_type == 2:  # Simple URL Extraction
                        self._get_url(r[0])
                    elif act.action_type == 3:  # Simple IMG_URL Extraction
                        self._get_imgurl(r[0])

                    elif act.action_type == 7 or act.action_type == 8:
                        out_table = self._get_full_table(r[0])
                        out_val = list(zip(*out_table))
                        if act.action_type == 7:  # Ful Table Extraction
                            final = []
                            for val in out_val:
                                final.append({"": list(val)})
                            return final
                        elif act.action_type == 8:  # Colwise Table Extraction
                            final = []
                            for i in act.column_list:
                                final.append({"": list(out_val[i])})
                            return final

                    elif act.action_type == 9:  # Pagination Link
                        # print("Pagination path", r_xpath)
                        if self._click_action(r_xpath):
                            time.sleep(2)
                            self.tree = etree.parse(StringIO(self.driver.page_source), etree.HTMLParser())
                            self.pagination_flag = 1
                            self.output_list.append("1")
                        else:
                            r_xpath = self._abs_to_relative(r_xpath)
                            # print("Relative  path is: ", r_xpath)
                            if self._click_action(r_xpath):
                                time.sleep(2)
                                self.tree = etree.parse(StringIO(self.driver.page_source), etree.HTMLParser())
                                self.pagination_flag = 1
                                self.output_list.append("1")
                            else:
                                print("Pagination page cannot be found")

                    elif act.action_type == 11:  # Simple Download Link
                        self.download_flag = 1
                        self._get_download_link(r[0])
                    elif act.action_type == 12:  # Similar Download Link
                        self.download_flag = 1
                        self.path1 = self._get_path_list(self.tree.getpath(r[0]))
                        root = self.tree.getroot()
                        self._get_similar_download(root, '/' + root.tag, 1, 12)

                    elif act.action_type == 13:  # Click Action
                        self.goback_active = 1
                        self._click_action(act.path[1])
                        time.sleep(2)
                        self.tree = etree.parse(StringIO(self.driver.page_source), etree.HTMLParser())
                    elif act.action_type == 14:  # Fill Action
                        self._fill_action(act.path[1], act.fill_value)
                else:
                    print("Still!! Path cannot be found!")

            elif act.action_type == 10 or (act.action_type > 3 and act.action_type < 7):
                self.path1 = self._get_path_list(self._get_xpath(act.path[0], act.path[1], act.action_type))
                self.path2 = self._get_path_list(
                    self._get_xpath(act.similar_path[0], act.similar_path[1], act.action_type))
                if self.path1 != [] and self.path2 != []:
                    self.text_flag = 1
                    self.sim_depth = self._depth_count()

                    if self.sim_depth == -1:
                        return []
                    elif self.sim_depth == 0:
                        self.path1 = self._get_path_list(self.tree.getpath('/'.join(self.path1)))
                        self.path2 = self._get_path_list(self.tree.getpath('/'.join(self.path2)))
                        self.sim_depth = self._depth_count()

                        if self.sim_depth == 0:
                            print("Paths found, but are not similar")
                            print(self.path1)
                            print(self.path2)
                            return []

                    if self.sim_depth == len(self.path1):
                        self.sim_xpath = '/html'
                        self.sim_depth = 1
                    else:
                        self.sim_xpath = '/'.join(i for i in self.path1[:self.sim_depth])
                    self._get_class_attrib(self.sim_xpath, self.sim_xpath, self.sim_depth)
                    sim_node = self.tree.xpath(self.sim_xpath)
                    self._get_similar_path(sim_node[0], self.sim_xpath, self.sim_depth, act.action_type)

                else:
                    print("Still!! Path cannot be found!")

        elif act.action_type == 15:
            self._page_source(self.goback_url)
        elif act.action_type > 15:
            print("ActionType Error: Invalid action type, " + act.action_type)

        return self.output_list

    def _page_source(self, url: str):
        """
        Function to get the url source code and create the etree using XLML HTML parser.

        :param url: Current URL to be requested
        """
        try:
            self.driver.get(url)
            # time.sleep(10)
            self.tree = etree.parse(StringIO(self.driver.page_source), etree.HTMLParser())
            if self.goback_active == 0:
                self.goback_url = url
        except:
            self.driver.quit()
            print(traceback.print_exc())
            print("Cannot retrive any page. Chrome crashed!!")
            sys.exit()

        # file = open("websites_test/test123.html","w")
        # file.write(str(self.driver.page_source))
        # file.close()

    def _main_function(self, url: str, page: PageInfo):
        """
        Performs all the extractions per page. Also includes pagination implementation on the given page.

        :param url: URL of the given page.
        :param page: PageInfo() class instance from the page_list variable in project. Contains information based on the page to be extracted.
        :return: Corresponding output of the given page.
        :rtype: list
        """
        # Set the current page's url (used in _domain_name() method)
        self.current_page_url = url

        # self.domain_url = urljoin(url, '/')
        # path1, filename1 = os.path.split(url)
        # self.domain_url2 = path1

        self._page_source(url)

        self.result = []
        if len(page.pagination) == 0:
            for act in page.action_list:
                if act.action_type == 13 or act.action_type == 14:
                    self._classify_action(act)
                elif act.action_type == 7 or act.action_type == 8:
                    self.result.extend(self._classify_action(act))
                else:
                    self.result.append({act.column_name: self._classify_action(act)})
        else:
            out = []
            c = 0
            while c < page.num_pages:
                resultx = []
                if url != '':
                    for act in page.action_list:
                        if c == 0 and (act.action_type == 13 or act.action_type == 14 or act.action_type == 15):
                            self._classify_action(act)
                            resultx.append("")
                        elif act.action_type == 7 or act.action_type == 8:
                            resultx.extend(self._classify_action(act))
                        elif act.action_type != 13 and act.action_type != 14 and act.action_type != 15:
                            resultx.append(self._classify_action(act))
                        else:
                            resultx.append("")

                    act1 = ActionInfo()
                    act1.action_type = 9
                    act1.path = page.pagination
                    if self._classify_action(act1) == []:
                        c = page.num_pages
                    # if self._click_action(page.pagination[1]):
                    #   time.sleep(2)
                    #   self.tree = etree.parse(StringIO(self.driver.page_source), etree.HTMLParser())
                    #   self.pagination_flag = 1
                    # else:
                    #   c = page.num_pages
                c += 1
                if len(out) == 0:
                    out = resultx
                else:
                    for i in range(len(resultx)):
                        out[i] += resultx[i]

            for i in range(len(page.action_list)):
                if page.action_list[i].action_type != 13 and page.action_list[i].action_type != 14 and page.action_list[
                    i].action_type != 15:
                    self.result.append({page.action_list[i].column_name: out[i]})

        return self.result

    def _recur_page(self, pages: list, depth: int):
        """
        Fucnction call for Multi-level Project. Recursively gets all next level pages and calls _main_function on each page

        :param pages: List of PageInfo() class instances.
        :param depth: Variable to keep track of Page level(in the pages list)
        :return: List of output in each page of a level (Output of _main_function())
        :rtype: list
        """
        output1 = []
        current = pages[depth]
        self.goback_active = 0
        out1 = self._main_function(current.current_page_url, current)
        output1.append(out1)
        if len(pages) != depth + 1:
            count = 0
            domain_url = urljoin(current.current_page_url, '/')
            for i in range(0, len(current.action_list)):
                if current.action_list[i].action_type == 10:
                    for key, value in out1[i - count].items():
                        similar_list = value
                    for i in range(0, len(similar_list)):
                        if similar_list[i][0:4] != 'http' and similar_list[i][0:4] != 'file':
                            similar_list[i] = domain_url + similar_list[i]

                    final = []
                    for similar_page in similar_list:
                        pages[depth + 1].current_page_url = similar_page
                        final.append(self._recur_page(pages, depth + 1))
                    output1.append(final)
                elif current.action_list[i].action_type == 13 or current.action_list[i].action_type == 14 or \
                        current.action_list[i].action_type == 15:
                    count += 1

        return output1

    def _login_process(self, login_info: dict, key):
        """
        Function to login into a website using the decrypted credentials given in the login info

        :param login_info: Dictionary which has login information like Login URL and Login Actions(Fill/ Click)
        :param key: Key required to decrypt the encrypted login credentials
        """
        try:
            self.driver.get(login_info.login_url)
        except:
            self.driver.quit()
            print(traceback.print_exc())
            print("Cannot retrive any page. Chrome crashed!!")
            sys.exit()

        for act in login_info.action_list:
            if act.action_type == 13:
                self._click_action(act.path[1])
            elif act.action_type == 14:
                self._fill_action(act.path[1],
                                  Fernet(key.encode("utf-8")).decrypt(bytes(act.fill_value, 'utf-8')).decode("utf-8"))

    def process(self, project: Project, key):
        """
        Function to extract data based on the procedure json file information

        :param project: Project class instance, containing the procedure json file information
        :return: List, consisting of three values:
                    1. Project Type value
                    2. Output list
                    3. Download Type either -1, 0, 1 or 2
        :rtype: list
        """
        if project.login_details and project.project_type != 5:
            self._login_process(project.login_details[0], key)

        output = []
        if project.project_type == 1 or project.project_type == 2:
            for page in project.page_list:
                output.append(self._main_function(page.current_page_url, page))

        elif project.project_type == 3:
            output = self._recur_page(project.page_list, 0)

        elif project.project_type == 4:
            for key, value in project.product_url.items():
                output.append([{"URL_List": value}])
                final = []
                for page in value:
                    final.append([self._main_function(page, project.page_list[0])])
                output.append(final)

        elif project.project_type == 5:
            final1 = []
            final2 = []
            for key, value in project.product_url.items():
                final1.append(key)
                xlist = []
                for i in range(0, len(value)):
                    xlist.append([self._main_function(value[i], project.page_list[i])])
                final2.append([[{"URL_List": value}], xlist])
            output.append([{"Product": final1}])
            output.append(final2)
        self.driver.quit()
        out_flag = -1
        if self.text_flag == 1:
            if self.download_flag == 1:
                out_flag = 2
            else:
                out_flag = 0
        elif self.text_flag == 0:
            if self.download_flag == 1:
                out_flag = 1
            else:
                out_flag = -1
        return [project.project_type, output, out_flag]


if __name__ == '__main__':
    # TODO: command line processing
    # session = Session('http://localhost:8080')
    session = Session('http://192.168.20.152:9000')
    # TODO: use dedicated account or one specified in command line.
    if not session.login('kataoka.ch@sms-datatech.co.jp', 'sms12345'):
        sys.exit()

    try:
        projects_string = get_upcoming_projects(session)
        prj = json.loads(projects_string['message'])
        print (prj)
        for project in prj:
            print('\n====Project to be scheduled====\n')
            print(project['pk'] + '\n')
            project_id = project['pk']
            try:
                print('====Scheduling a project=====\n')
                print(project_id)
                scrape_schedule(session, project_id)
            except PigDataException as ex:
                print("===Scheduling ERROR !!====")
                save_result(session, project_id, error=ex.message)
    finally:
        session.logout()
