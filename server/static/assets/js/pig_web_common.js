
$(function() {
	
	$('input[name=plan_type]:radio').on('change', function() {
		if( $("#id_plan_type_0").prop('checked') ){
			$("#business_payment_outer").slideUp();
		} else if ($("#id_plan_type_1").prop('checked')) {
			$("#business_payment_outer").slideDown();
		}
	});
	
	$('input[name=pay_method]:radio').on('change', function() {
		if( $("#id_pay_method_0").prop('checked') ){
			$('.payment_invoice_info').slideUp();
			$('.payment_card_info').slideDown();
		} else if ($('#id_pay_method_1').prop('checked')) {
			$('.payment_card_info').slideUp();
			$('.payment_invoice_info').slideDown();
		} 
	});
	
	$('.mouse_over').hover(
		function(e) {
			$(this).css('background-color', '#dedddd');
		},
		function(e) {
			if ( !$(this).find('input').prop('checked') ) {
				$(this).css('background-color', '#F8F8F8');
			}
		}
	);
	
	$('#free_plan').click( function(e) {
		$(this).addClass('radio_selected');
		$(this).css('background-color', '#dedddd');
		$('#id_plan_type_0').prop('checked', true);
		$('input:radio[name=plan_type]:checked').change();
		removeSelected();
	});
	
	$('#business_plan').click( function(e) {
		$(this).addClass('radio_selected');
		$(this).css('background-color', '#dedddd');
		$('#id_plan_type_1').prop('checked', true);
		$('input:radio[name=plan_type]:checked').change();
		removeSelected();
	});
	
	$('#pay_method_card').click( function(e) {
		$(this).addClass('radio_selected');
		$(this).css('background-color', '#dedddd');
		$('#id_pay_method_0').prop('checked', true);
		$('input:radio[name=pay_method]:checked').change();
		removeSelected();
	});
	
	$('#pay_method_invoice').click( function(e) {
		$(this).addClass('radio_selected');
		$(this).css('background-color', '#dedddd');
		$('#id_pay_method_1').prop('checked', true);
		$('input:radio[name=pay_method]:checked').change();
		removeSelected();
	});
	
	$('#copy_comapny_address').change( function() {
		if( $(this).prop('checked') ) {

			var companyName = $('input[name="company_name"]').val();
			var companyPostCode = $('input[name="post_code"]').val();
			var companyPhoneNo = $('input[name="tel_number"]').val();
			var companyAddress = $('input[name="address"]').val();
			
			$('input[name="pay_company_name"]').val(companyName);
			$('input[name="pay_post_code"]').val(companyPostCode);
			$('input[name="pay_tel_number"]').val(companyPhoneNo);
			$('input[name="pay_company_address"]').val(companyAddress);
		}
	});

	(function() {
		var number = document.querySelector('#temp_card_number'),
			cvc = document.querySelector('#temp_security_no'),
			exp_month = document.querySelector('#temp_month'),
			exp_year = document.querySelector('#temp_year');

		//document.querySelector('.fill').addEventListener('click', function(e) {
		//	e.preventDefault();
		//	number.value = '4242424242424242';
		//	cvc.value = '123';
		//	exp_month.value = '12';
		//	exp_year.value = '2020';
		//});
		document.querySelector('#submit_button').addEventListener('click', function(e) {
			e.preventDefault();
			Payjp.setPublicKey('pk_test_0f8708342d1a0e3444ead35f');

			var card = {
				number: number.value,
				cvc: cvc.value,
				exp_month: exp_month.value,
				exp_year: exp_year.value
			};

			Payjp.createToken(card, function(s, response) {
				var userstoken = response.id;
				document.getElementById('id_pay_card_token').value = userstoken;
//				$('#id_pay_card_token').val() = userstoken;

				$('#submit_form').submit();

//				submit_data (userstoken);

//				alert("Token: " + userstoken);
				//var amount = 39800;
				//
				// Re-direct to payment page.
				// SHOULD NOT use Token with GET method. This is only on example
				//window.location.href = location.protocol
				//					   + "/pay-finish.html"
				//					   + "?token=" + userstoken
				//					   + ";amount=" + amount;
			});
		});
	})();

	function submit_data (userstoken) {

		//	e.preventDefault();

		/*		//setupKakureContents

		*/

		var username = $('input[name="username"]').val();

		var last_name = $('input[name="last_name"]').val();
		var first_name = $('input[name="first_name"]').val();
		var last_name_kana = $('input[name="last_name_kana"]').val();
		var first_name_kana = $('input[name="first_name_kana"]').val();

		var company_name = $('input[name="company_name"]').val();
		var company_name_kana = $('input[name="company_name_kana"]').val();
		var department = $('input[name="department"]').val();
		var job_title = $('input[name="job_title"]').val();
		var tel_number = $('input[name="tel_number"]').val();
		var post_code = $('input[name="post_code"]').val();
		var address = $('input[name="address"]').val();

		var password1 = $('input[name="password1"]').val();
		//var password2 = $('input[name="password1"]').val();

		var plan_type = $("input[name='plan_type']:checked").val();
		var pay_method = $("input[name='pay_method']:checked").val();

		var is_agree_user_pol = $("input[name='is_agree_user_pol']:checked").val();
		var is_agree_privacy_pol = $("input[name='is_agree_privacy_pol']:checked").val();
		var is_agree_security_pol = $("input[name='is_agree_security_pol']:checked").val();


		console.log(
			'E-Mail: ' + username + '\n'
			+ '姓(漢字): ' + last_name + '\n'
			+ '名(漢字): ' + first_name + '\n'
			+ '姓(カナ): ' + last_name_kana + '\n'
			+ '名(カナ): ' + first_name_kana + '\n'
			+ '会社名: ' + company_name + '\n'
			+ '会社名(カナ): ' + company_name_kana + '\n'
			+ '部署名: ' + department + '\n'
			+ '役職名: ' + job_title + '\n'
			+ '電話番号: ' + tel_number + '\n'
			+ '郵便番号: ' + post_code + '\n'
			+ '住所: ' + address + '\n'
			+ 'password1/password2: ' + password1 + '\n'
			+ 'プラン: ' + plan_type + '\n'
			+ '支払方法: ' + pay_method + '\n'
			+ '利用規約: ' + is_agree_user_pol + '\n'
			+ 'プライバシー: ' + is_agree_privacy_pol + '\n'
			+ 'セキュリティー: ' + is_agree_security_pol + '\n'
			+ 'Card Token: ' + userstoken
		);

		$('#submit_form').submit();


/*		$.ajax({
			type: 'POST',
			url: '/signup-confirm/',
			data: $('#submit_form').serialize()
		})
        .done(function (data, textStatus, jqXHR) {
        	console.log('textStatus: ' + textStatus);
        	if ( textStatus == 'success' ) {
                console.log('OK');
                location.href="/signup-confirm/";
			}
        }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
			console.log('readyState:' + XMLHttpRequest.readyState);
			console.log('status:' + XMLHttpRequest.status);
			console.log(errorThrown);
    	});
*/
	}
});

function removeSelected() {
	$('.mouse_over').each( function(i) {
		var target = $(this).find('input:radio');
		if ( !($(target).prop('checked')) ) {
			$(target).removeClass('radio_selected');
			$(this).css('background-color', '#F8F8F8');
		}
	});
}

