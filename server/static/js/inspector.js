"use strict";
(function(document) {

    var csrftoken = '';

    var action = {
        "column_list": [],
        "action_type": 0,
        "fill_value": "",
        "row_column": [],
        "column_name": "",
        "path": [],
        "similar_path": []
    };

    // Flags and Types
    var disable_flag = 1;
    var action_type = 0;
    var sim_depth = -1, similar_flag = 0;
    var table_flag = 0, index = -1, col_type = 0;
    // Main variables
    var lastAction = action;
    var last_val = '';
    var lastNode;
    var lastPath;
    var curr_table;
    
    var similarNode, similarPath;
    var similar_data = [];
    var class_attrib = [];
    var login_active = 0;
    
    var sendMessage = function (msg) {                          // Send a message to the parent
        window.parent.postMessage(msg, '*');
    };
    sendMessage({'iframe_content2': {'loaded': 1}});

    var urls_dict = {
        "pdf" : "application/pdf",
        "jpg" : "image/jpeg",
        "jpeg": "image/jpeg",
        "png" : "image/png",
        "gif": "image/gif",
        "bmp" : "image/bmp",
        "svg": "image/svg+xml",
        "json": "application/json",
        "xls" : "application/vnd.ms-excel",
        "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "ppt": "application/vnd.ms-powerpoint",
        "pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "doc": "application/msword",
        "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "csv": "text/csv",
        "txt" : "text/plain",
        "zip" : "application/zip",
        "zip" : "application/x-zip-compressed",
        "bz": "application/x-bzip",
        "rar": "application/x-rar-compressed",
        "tar": "application/x-tar",
        "7z": "application/x-7z-compressed",
        "css": "text/css",
        "xml" : "application/xml"
    }
    
    function bindEvent(element, eventName, eventHandler) {      // addEventListener support for IE8
        if (element.addEventListener){
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }

    function clear_table(el){
        el.style.border = '';
        var children = el.childNodes;
        for (var i = 0; i < children.length; i++) {
            if(children[i].nodeType == 1){
                clear_table(children[i]);
            }
        }
    }

    function clear_js(){
        // Remove green
        if(similarNode !== undefined && similarNode !== null){
            similarNode.style.border = '';
            // similarNode.target.style.border = '';
        }
        // Remove red
        if(lastPath !== undefined && lastPath !== null){
            let htmlTarget = document.getElementsByTagName("html")[0];
            pathNode(htmlTarget, lastPath[0].split(" "), 0, 2);
        }
        if( (lastNode != undefined && lastNode !== null) ){
            // lastNode.target.style.border = '';
            lastNode.style.border = '';
        }
        // Removing Table
        if(action_type == 7){
            if((curr_table.style.border == '2px solid rgb(0, 153, 0)')){
                curr_table.style.border = '';
            }
        }
        else if(action_type == 8){
            clear_table(curr_table);
        }
        else if(action_type == 12){
            let htmlTarget = document.getElementsByTagName("html")[0];
            perform_similar(htmlTarget, lastPath[0].split(" "), 0, 2);
        }

        // Flags and Types
        action_type = 0;
        sim_depth = -1;
        similar_flag = 0;
        table_flag = 0;
        index = -1;
        col_type = 0;
        // Main variables
        lastNode = null;
        lastPath = null;
        curr_table = null;
        class_attrib = [];
        similar_data = [];
        // Clearing Action
        action["column_list"] = [];
        action["action_type"] = 0;
        action["fill_value"] = "";
        action["row_column"] = [];
        action["column_name"] = "";
        action["path"] = [];
        action["similar_path"] = [];
        lastAction = action;
        last_val = '';
    }
    

    // Creating action
    function getAction(act){
        return {"column_list": act["column_list"], "action_type": act["action_type"], "fill_value": act["fill_value"], "row_column": act["row_column"], "column_name": act["column_name"], "path":act["path"], "similar_path": act["similar_path"]};
    }

    function get_lastVal(el, type){
        if (type == 1){
            let xdata = el.textContent.replace(/^\s+|\s+$/g, "");
            if (xdata != ''){
                last_val = xdata;
            }
        } else if(type == 2){
            if (el.nodeName.toLowerCase() == 'a'){
                let xdata = getURL(el);
                if (xdata != ''){
                    last_val = xdata;
                }
            }
            else{
                let el = el;
                while(el.nodeName.toLowerCase() != 'html') {
                    if((el.nodeName.toLowerCase() == 'a')) {
                        let xdata = getURL(el);
                        if (xdata != ''){
                            last_val = xdata;
                            break;
                        }
                    }
                    else {
                        el = el.parentNode;
                    }
                }
            }
        } else if(type == 3){
            if((el.nodeName.toLowerCase() == "img")){
                let xdata = getImg(el);
                if (xdata != ''){
                    last_val = xdata;
                }
            }
        }
    }

    bindEvent(window, 'message', function (e) {
        let data = e.data;
        if(data['action_data'] != undefined){
            let action_data = data['action_data'];
            if(action_data['login_type'] != undefined){
                login_active = action_data['login_type'];
            }
            // else if(action_data['back'] != undefined){
            //  page_info["action_list"].pop();
            // }
            else if(action_data['action_type'] != undefined){ 
                // Can receive action_type = 1, 2, 3 , 4, 5, 6, 7, 8, 9, 11, 12, 13, 14
                action_type = action_data['action_type'];
                csrftoken = action_data['csrftoken'];
                if(action_type < 15){
                    disable_flag = 0;
                }
            }
            else if(action_data['fill_confirm'] != undefined){
                // lastNode.target.style.border = '2px solid #090';
                lastNode.style.border = '2px solid #090';
                disable_flag = 1;
            }
            else if(action_data['action_fill_next'] != undefined){
                lastNode.value = action_data['action_fill_next'];
                lastNode.style.border = '2px solid #090';

                // lastNode.target.value = action_data['action_fill_next'];
                // lastNode.target.style.border = '2px solid #090';
                lastAction["action_type"] = 14;
                lastAction["path"] = lastPath;
                lastAction["fill_value"] = action_data['action_fill_next'];
                sendMessage({'iframe_content2': {'5': {"page_action": getAction(lastAction)}}});

                lastNode = null;
                lastPath = null;
                // Clearing Action
                action["column_list"] = [];
                action["action_type"] = 0;
                action["fill_value"] = "";
                action["row_column"] = [];
                action["column_name"] = "";
                action["path"] = [];
                action["similar_path"] = [];
                lastAction = action;
                action_type = 13;
            }
            else if(action_data['click_confirm'] != undefined){
                lastNode.style.border = '2px solid #090';
                // lastNode.target.style.border = '2px solid #090';
                lastAction["action_type"] = 13;
                lastAction["path"] = lastPath;
                sendMessage({'iframe_content2': {'5': {"page_action": getAction(lastAction)}}});

                clear_js();
                disable_flag = 1;
            }

            else if(action_data['simple_confirm'] != undefined){
                // Confirmation on First path, similar path, etc.
                if(action_type > 3 && action_type < 7 && similar_flag == 1){
                    lastAction["similar_path"] = lastPath;
                }
                else{
                    if(action_type < 4 && action_type > 0){
                        disable_flag = 1;
                        lastNode.style.border = '2px solid #090';
                        // lastNode.target.style.border = '2px solid #090';
                        // Check the last Node length
                        let htmlTarget = document.getElementsByTagName("html")[0];
                        perform_similar(htmlTarget, lastPath[0].split(" "), 0, 3);
                        if(similar_data.length > 1){
                            sendMessage({'iframe_content2': {'similarcheck': lastPath, 'last_val': ['#Column name', last_val], 'flag': 1}});
                        }
                        else{
                            sendMessage({'iframe_content2': {'singlecheck': lastPath, 'last_val': ['#Column name', last_val], 'flag': 1}});
                        }
                        similar_data = [];
                    }
                }
            }
            else if(action_data['similar'] != undefined){
                // Similar Yes or No button
                if(action_data['similar'] == 1){
                    disable_flag = 0;
                    similar_flag = 1;

                    lastAction["path"] = lastPath;
                    similarNode = lastNode;
                    similarPath = lastPath;

                    if (action_type == 1){
                        action_type = 4;
                        lastAction["action_type"] = 4;
                    }
                    else if(action_type == 2){
                        action_type = 5;
                        lastAction["action_type"] = 5;
                    }
                    else if(action_type == 3){
                        action_type = 6;
                        lastAction["action_type"] = 6;
                    }
                    lastNode = null;
                    lastPath = null;
                }
                else if(action_data['similar'] == 0){
                    lastAction["action_type"] = action_type;
                    lastAction["path"] = lastPath;
                    sendMessage({'iframe_content2': {'1': {"no_col_name": ['#Column name', last_val]}}});
                }
            }
            else if(action_data['similar_confirm'] != undefined){
                disable_flag = 1;
                lastAction["action_type"] = action_type;
                lastAction["similar_path"] = lastPath;
                sendMessage({'iframe_content2': {'similarcheck2': lastPath}});
                // Put in preview
                let temp_data = similar_data.slice();
                temp_data.unshift('#Column name');
                sendMessage({'iframe_content2': {'1': {"no_col_name": temp_data}}});
            }
            else if(action_data['action_table_confirm'] != undefined){
                disable_flag = 1;
                curr_table.style.border = '2px solid #090';
            }
            else if(action_data['action_full_confirm'] != undefined){
                lastAction["action_type"] = action_type;
                lastAction["path"] = [cssPath(curr_table), getElementXPath(curr_table)];
                sendMessage({'iframe_content2': {'5': {"page_action": getAction(lastAction)}}});
                table_flag = 1;
                
                let result = [];
                for (let i = 0, row; row = curr_table.rows[i]; i++) {
                    let temp = [];
                    for (let j = 0, col; col = row.cells[j]; j++) {
                        for (let k = 0; k < col.colSpan; k++){
                            temp.push(col.textContent.replace(/^\s+|\s+$/g, ""));
                        }
                    }
                    result.push(temp);
                }
                sendMessage({'iframe_content2': {'1': result}});

                clear_js();

            }
            else if(action_data['action_col_confirm'] != undefined){
                table_flag = 2;
            }
            else if(action_data['action_add_col'] != undefined){
                let col_data = add_column();
                sendMessage({'iframe_content2': {'1': col_data}});
            }
            else if(action_data['action_col_next'] != undefined){
                clear_js();
                disable_flag = 1;
            }
            else if(action_data['action_file_confirm'] != undefined){
                disable_flag = 1;
                lastAction["path"] = lastPath;
                lastNode.style.border = '2px solid #090';
                // lastNode.target.style.border = '2px solid #090';
            }
            else if(action_data['file_no'] != undefined){
                lastAction["action_type"] = action_type;
                sendMessage({'iframe_content2': {'5': {"page_action": getAction(lastAction)}}});
                clear_js();
            }
            else if(action_data['file_yes'] != undefined){
                action_type = 12;
                // similar_data = [];
                let htmlTarget = document.getElementsByTagName("html")[0];
                perform_similar(htmlTarget, lastPath[0].split(" "), 0, 4);
            }
            else if(action_data['file_cancel'] != undefined){
                clear_js();
            }
            else if(action_data['file_next'] != undefined){
                lastAction["action_type"] = action_type;
                sendMessage({'iframe_content2': {'5': {"page_action": getAction(lastAction)}}});
                clear_js();
            }
            else if(action_data['column_name'] != undefined){
                // Column Name button
                lastAction["column_name"] = action_data['column_name'];
                // Put in preview
                if(action_type < 4){
                    sendMessage({'iframe_content2': {'1': {"result": [action_data['column_name'], last_val]}}});
                }
                else if(action_type < 7){
                    similar_data.unshift(action_data['column_name']);
                    sendMessage({'iframe_content2': {'1': {"result": similar_data}}});
                }
                sendMessage({'iframe_content2': {'5': {"page_action": getAction(lastAction)}}});
                // Clear code variables and buttons
                clear_js();
            }
            else if(action_data['pagination_confirm'] != undefined){
                sendMessage({'iframe_content2': {'5': {"pagination": lastPath}}});
                disable_flag = 1;
            }
            else if(action_data['pagination_skip'] != undefined){
                sendMessage({'iframe_content2': {'5': {"pagination": []}}});
                disable_flag = 1;
                clear_js();
            }
            else if(action_data['pagination_next'] != undefined){
                clear_js();
            }

            // Login Actions
            else if(action_data['action_login_confirm'] != undefined){
                disable_flag = 1;
            }
            else if(action_data['action_login_next'] != undefined){
                if(lastNode !== undefined && lastNode !== null){    
                    lastNode.value = action_data['action_login_next'];
                    lastNode.style.border = '2px solid #090';
                    // lastNode.target.value = action_data['action_login_next'];
                    // lastNode.target.style.border = '2px solid #090';
                }
                lastAction["action_type"] = 14;
                lastAction["path"] = lastPath;
                lastAction["fill_value"] = action_data['action_login_next'];
                sendMessage({'iframe_content2': {'5': {"login_action": getAction(lastAction)}}});

                // clear_js();
                lastNode = null;
                lastPath = null;
                // Clearing Action
                action["column_list"] = [];
                action["action_type"] = 0;
                action["fill_value"] = "";
                action["row_column"] = [];
                action["column_name"] = "";
                action["path"] = [];
                action["similar_path"] = [];
                lastAction = action;

                disable_flag = 0;
                action_type  = 14;
            }
            else if(action_data['action_password_confirm'] != undefined){
                disable_flag = 1;
            }
            else if(action_data['action_password_next'] != undefined){
                lastNode.value = action_data['action_password_next'];
                lastNode.style.border = '2px solid #090';
                // lastNode.target.value = action_data['action_password_next'];
                // lastNode.target.style.border = '2px solid #090';

                lastAction["action_type"] = 14;
                lastAction["path"] = lastPath;
                lastAction["fill_value"] = action_data['action_password_next'];
                sendMessage({'iframe_content2': {'5': {"login_action": getAction(lastAction)}}});

                // clear_js();
                lastNode = null;
                lastPath = null;
                
                // Clearing Action
                action["column_list"] = [];
                action["action_type"] = 0;
                action["fill_value"] = "";
                action["row_column"] = [];
                action["column_name"] = "";
                action["path"] = [];
                action["similar_path"] = [];
                lastAction = action;
                
                disable_flag = 0;
                action_type  = 13;
            }
            else if(action_data['finish_login'] != undefined){
                lastAction["action_type"] = 13;
                lastAction["path"] = lastPath;
                sendMessage({'iframe_content2': {'5': {"login_finish": getAction(lastAction)}}});
                clear_js();
                disable_flag = 1;
            }
            else if(action_data['password_click_confirm'] != undefined){
                lastAction["action_type"] = action_type;
                lastAction["path"] = lastPath;
                sendMessage({'iframe_content2': {'5': {"login_second_click": getAction(lastAction)}}});
                clear_js();

                disable_flag = 0;
                action_type  = 14;
            }
            
            else if(action_data['action_nextpage_password'] != undefined){
                clear_js();
                action_type = 13;
            }
            else if(action_data['action_simple_password'] != undefined){
                disable_flag = 1;
            }
        }
        else if(data['back_action_login'] != undefined){
            let state = data['back_action_login'];
            if(state["action_type"] != undefined){
                action_type = state["action_type"];
            }
            else if (state['login_fill'] != undefined){ 
                let state_node = state['login_fill'];
                if (state_node["loginTab"] != -1){
                    let state_action = state_node["action_list"][0];
                    if (state_action["action_type"] == 14){
                        // Remove Fill-in action
                        let xnode = document.evaluate(state_action["path"][1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        xnode.value = '';
                        xnode.style.border = '';    
                    }
                }
            }
            else if (state['login_click'] != undefined){ 
                let state_node = state['login_click'];
                if (state_node["loginTab"] != -1){
                    let state_action = state_node["action_list"][0];
                    if (state_action["action_type"] == 14){
                        // Remove Fill-in action
                        let xnode = document.evaluate(state_action["path"][1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        xnode.value = state_action["fill_value"];
                        xnode.style.border = '2px solid #090';
                    }
                }
                action_type = 13;
            }   
        }
        else if(data['back_action_extraction'] != undefined){
            let state = data['back_action_extraction'];
            if (state["back_fill"] != undefined){
                let state_node = state['back_fill'];
                if (state_node["currentTab"] != -1){
                    let state_action = state_node["action_list"][0];
                    if (state_action["action_type"] == 14){
                        // Remove Fill-in action
                        let xnode = document.evaluate(state_action["path"][1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        xnode.value = '';
                        xnode.style.border = '';    
                    }
                }
                action_type = 14;
            }
            // else if(state["back_click"] != undefined){
            //  let state_node = state['back_click'];
            //  if (state_node["currentTab"] != -1){
            //      let state_action = state_node["action_list"][0];
            //      if (state_action["action_type"] == 14){
            //          // Remove Fill-in action
            //          let xnode = document.evaluate(state_action["path"][1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
            //          xnode.value = state_action["fill_value"];
            //          xnode.style.border = '2px solid #090';
            //      }
            //  }
            //  action_type = 13;
            // }
            else if(state["action_type"] != undefined){
                action_type = state["action_type"];
            }
            else if(state["clear_fill"] != undefined){
                clear_js();
            }
            else if(state["highlights"] != undefined){
                let highlights = state["highlights"];
                let currentTab = highlights["currentTab"];
                let action_node = highlights["action_node"];
                let extra_flag = highlights["extra_flag"];
                // disable_flag = highlights["disable_flag"];
                
                switch(currentTab){
                    case 0:
                        // No Highlights
                        break;
                    case 2:
                        // No Highlights
                        clear_js();
                        disable_flag = 1;
                        action_type = 0;
                        break;
                    case 5:
                        clear_js();
                        action_type = action_node["action_type"];
                        if (action_node["action_type"] > 3 && action_node["action_type"] < 7){
                            action_type = action_node["action_type"] - 3;
                        } 
                        disable_flag = 0;
                        similar_flag = 0;

                        lastPath = action_node["path"];
                        lastNode = document.evaluate(lastPath[1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        lastNode.style.border = '2px solid #ff0001';
                        get_lastVal(lastNode, action_type);
                        break;
                    case 6:
                        clear_js();
                        action_type = action_node["action_type"];
                        if (action_node["action_type"] > 3 && action_node["action_type"] < 7){
                            action_type = action_node["action_type"] - 3;
                        } else {
                            action_type = action_node["action_type"];
                        }
                        disable_flag = 1;
                        lastPath = action_node["path"];
                        lastNode = document.evaluate(lastPath[1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        lastNode.style.border = '2px solid #090';
                        similarPath = [];
                        similarNode = null;
                        get_lastVal(lastNode, action_type);
                        // Stuti
                        let htmlTarget2 = document.getElementsByTagName("html")[0];
                        perform_similar(htmlTarget2, lastPath[0].split(" "), 0, 3);
                        if(similar_data.length > 1){
                            sendMessage({'iframe_content2': {'similarcheck': lastPath, 'last_val': ['#Column name', last_val], 'flag': 0}});
                        }
                        else{
                            sendMessage({'iframe_content2': {'singlecheck': lastPath, 'last_val': ['#Column name', last_val], 'flag': 0}});
                        }
                        similar_data = [];
                        break;
                    case 7:
                        clear_js();
                        action_type = action_node["action_type"];
                        disable_flag = 0;
                        similar_flag = 1;
                        similarPath = action_node["path"];
                        similarNode = document.evaluate(similarPath[1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        similarNode.style.border = '2px solid #090';
                        lastPath = action_node["similar_path"];
                        lastNode = document.evaluate(lastPath[1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        // Dashed Red Border of Path2
                        sim_depth = depth_count(lastPath[0].split(" "), similarPath[0].split(" "), 0);
                        class_attrib = [];
                        get_class_attrib(lastPath[0].split(" "), similarPath[0].split(" "));
                        similar_data = [];
                        let htmlTarget = document.getElementsByTagName("html")[0];
                        pathNode(htmlTarget, similarPath[0].split(" "), 0, 1);
                        similarNode.style.border = '2px solid #090';
                        break;
                    case 8:
                        clear_js();
                        action_type = action_node["action_type"];
                        disable_flag = 1;
                        if (extra_flag == 1){
                            similar_flag = 1;
                            similarPath = action_node["path"];
                            similarNode = document.evaluate(similarPath[1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                            similarNode.style.border = '2px solid #090';
                            
                            lastPath = action_node["similar_path"];
                            lastNode = document.evaluate(lastPath[1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                            // Dashed Red Border of Path2
                            sim_depth = depth_count(lastPath[0].split(" "), similarPath[0].split(" "), 0);
                            class_attrib = [];
                            get_class_attrib(lastPath[0].split(" "), similarPath[0].split(" "));
                            similar_data = [];
                            let htmlTarget = document.getElementsByTagName("html")[0];
                            pathNode(htmlTarget, similarPath[0].split(" "), 0, 1);
                            similarNode.style.border = '2px solid #090';
                        }
                        else {
                            lastPath = action_node["path"];
                            lastNode = document.evaluate(lastPath[1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                            lastNode.style.border = '2px solid #090';
                        }
                        break;
                    case 9:
                        clear_js();
                        disable_flag = 1;
                        action_type = 0;
                        // No highlights
                        break;
                    case 10:
                        clear_js();
                        action_type = 9;
                        disable_flag = 0;
                        lastPath = highlights["pagination"];
                        if (extra_flag == 1){
                            lastNode = document.evaluate(lastPath[1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                            lastNode.style.border = '2px solid #ff0001';
                        }
                        break;
                    case 11:
                        disable_flag = 1;
                        action_type = 0;
                        // No highlights
                        break;
                    case 19:
                        disable_flag = 1;
                        action_type = 9;
                        lastPath = highlights["pagination"];
                        lastNode = document.evaluate(lastPath[1], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        lastNode.style.border = '2px solid #ff0001';
                        break;
                    default:
                        console.log("Highlights is broken!!");
                }   
            }
        }
        else if(data['forward_action'] != undefined){

        }
    });

    
    function cssPath(el) {
        let useNthChild = 0, cssPathStr = '', parentSelectors = [], tagName, cssId, cssClass, tagSelector, vagueMatch, nth, c;
        while ( el ) {
            vagueMatch = 0;
            tagName = el.nodeName.toLowerCase();
            cssId = ( el.id ) ? ( '#' + el.id ) : false;
            // let temp_class = el.className.replace(/\s+/g,".");
            let temp_class = el.className;
            if(typeof temp_class !== "string"){
                temp_class = "";
            }
            else{
                temp_class = temp_class.replace(/\s+/g,".");
            }
            if(temp_class.slice(0, 1)== '.') {
                temp_class = temp_class.slice(1);
            }
            if(temp_class.slice(-1) == '.') {
                temp_class = temp_class.slice(0, -1);
            }
            cssClass = ( el.className ) ? ( '.' + temp_class ) : '';
            if(cssId) {
                tagSelector = tagName + cssId + cssClass;
            } else if(cssClass) {
                tagSelector = tagName + cssClass;
            } else {
                vagueMatch = 1;
                tagSelector = tagName;
            }
            parentSelectors.unshift( tagSelector );
            el = el.parentNode !== document ? el.parentNode : false;
        }
        for (let i = 0; i < parentSelectors.length; i++ ) {
            cssPathStr += ' ' + parentSelectors[i];// + ' ' + cssPathStr;
            if ( useNthChild && !parentSelectors[i].match(/#/) && !parentSelectors[i].match(/^(html|body)$/) ) {
                if ( !parentSelectors[i].match(/\./) || $( cssPathStr ).length > 1 ) {
                    for ( nth = 1, c = el; c.previousElementSibling; c = c.previousElementSibling, nth++ );
                    cssPathStr += ":nth-child(" + nth + ")";
                }
            }
        }
        return cssPathStr.replace(/^[ \t]+|[ \t]+$/, '');
    }

    function getElementXPath(element) {
        let paths = [];
        for (; element && element.nodeType == Node.ELEMENT_NODE; element = element.parentNode)
        {
            if (element.id){
                paths.splice(0, 0, '/*[@id="' + element.id + '"]');
                break;
            }
            else{
                let index = 0;
                let hasFollowingSiblings = false;
                for (let sibling = element.previousSibling; sibling; sibling = sibling.previousSibling)
                {
                    // Ignore document type declaration.
                    if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE)
                        continue;

                    if (sibling.nodeName == element.nodeName)
                        ++index;
                }

                for (let sibling = element.nextSibling; sibling && !hasFollowingSiblings; sibling = sibling.nextSibling)
                {
                    if (sibling.nodeName == element.nodeName)
                        hasFollowingSiblings = true;
                }

                let tagName = (element.prefix ? element.prefix + ":" : "") + element.localName;
                let pathIndex = (index || hasFollowingSiblings ? "[" + (index + 1) + "]" : "");
                paths.splice(0, 0, tagName + pathIndex);
            }
        }

        return paths.length ? "/" + paths.join("/") : null;
    }

    function simplify(str1) {
        let arr = str1.split("#");
        let xarr = [];
        let tagName = '';
        let idVal = '';
        let className = '';
        if(arr.length < 2) {
            let arr2 = arr[0].split(".");
            tagName = arr2[0];
            xarr = arr2;
        }
        else { 
            tagName = arr[0];
            let arr2 = arr[1].split(".");
            idVal = arr2[0];
            xarr = arr2;
        }
        if(xarr.length < 2) {
            className = ' ';
        }
        else if(xarr.length < 3) {
            className = xarr[1] + ' ';
        }
        else {
            for (let k = 1; k < xarr.length; k++) {
                className = className + xarr[k] + ' ';
            }
        }
        className = className.slice(0, -1);
        return [tagName, idVal, className];
    }

    function arraysEqual(arr1, arr2) {
        if(arr1.length !== arr2.length){
            return false;
        }

        for (let i = arr1.length; i--;) {
            let val1 = simplify(arr1[i]);
            let val2 = simplify(arr2[i]);
            if(val1[0].toLowerCase() !== val2[0].toLowerCase()){
                return false;
            }
        }

        return true;
    }

    function getImg(el) {
        let result = ""
        if((el.nodeName.toLowerCase() == "img") && (el.src != "")){
            result = el.src;
        }
        else if(el.querySelector('img')) {
            result = el.querySelector('img').src;
        }
        return result;
    }

    function getURL(el) {
        let result = "";
        if(el.nodeName.toLowerCase() == 'a' && (el.href != "") ) {
            result = el.href;
        }
        else if(el.querySelector('a')) {
            result = el.querySelector('a').href;
        }
        else {
            while(el.nodeName.toLowerCase() !== 'html') {
                if((el.nodeName.toLowerCase() == 'a') && (el.href != "")) {
                    result = el.href;
                    break;
                }
                else {
                    el = el.parentNode;
                }
            }
        }
        return result;
    }

    function class_name(strClass){
        let value = strClass.replace(/\s+/g," ");
        if(value.slice(0, 1)== ' ') {
            value = value.slice(1);
        }
        if(value.slice(-1) == ' ') {
            value = value.slice(0, -1);
        }
        return value;
    }

    function get_class_attrib(path1, path2){
        if (path1.length == path2.length){
            for (let i = 0; i < path1.length; i++){
                let class_name = simplify(path1[i])[2];
                if (class_name == simplify(path2[i])[2]){
                    class_attrib[i] = class_name;
                }
                else{
                    class_attrib[i] = '';
                }
            }
        }
    }

    function depth_count(strPath1, strPath2, depth){
        let val1 = simplify(strPath1[depth]);
        let val2 = simplify(strPath2[depth]);
        if(val1[0].toLowerCase() == val2[0].toLowerCase()){
            if( (val1[1] == val2[1] && val1[2] == val2[2]) || (val1[0].toLowerCase() == 'html') || (val1[0].toLowerCase() == 'body') ){
                if(depth != (strPath1.length - 1)){ 
                    return depth_count(strPath1, strPath2, depth+1);
                }
            }
            else{
                return depth-1;
            }
        }
        else{
            return -1;
        }
        return depth;
    }

    function perform_similar(el, strPath, depth, flag) {
        let val = simplify(strPath[depth]);
        if(el.nodeName.toLowerCase() == val[0].toLowerCase())
        {
            if(depth == (strPath.length - 1) )
            {
                if(flag == 1){
                    el.style.border = '2px solid #ff0001';
                }
                else if(flag == 2){
                    el.style.border = '';
                }
                else if(flag == 3){
                    // el.style.border = '2px solid #090';
                    similar_data.push(el.textContent.replace(/^\s+|\s+$/g, ""));
                }
                else if(flag == 4){
                    let url = mimeval(el);
                    // if(url != '' && url != '#'){
                    //  if(url in urls_dict){
                    //      if(urls_dict[url] == mime_val){
                    //          similar_data.push(url);
                    //          el.style.border = '2px solid #090';
                    //      }
                    //  }
                    // }
                    if(mimetype(el)){
                        if(last_val == mimeres(url)){
                            // similar_data.push(url);
                            el.style.border = '2px solid #090';
                        }
                    }
                
                }
                else {
                    console.log("Pig Data gone mad.");
                }
            }
            else {
                depth = depth + 1;
                let children = el.childNodes;
                for (let i = 0; i < children.length; i++) {
                    if(children[i].nodeType == 1){
                        perform_similar(children[i], strPath, depth, flag);
                    }
                }
            }
        }
    }

    function extract_sim(el, flag){
        if(flag == 1){      // Similar Text, URl, IMG
            el.style.border = '2px dashed #ff0001';
            switch(action_type) {
                case 4:     // Get Text
                    similar_data.push(el.textContent.replace(/^\s+|\s+$/g, ""));
                    break;
                case 5:     // Get URL
                    similar_data.push(getURL(el));
                    break;
                case 6:     // Get Img
                    similar_data.push(getImg(el));
                    break;
                // case 4:     // Get link for Download
                //  similar_data.push(mimeval(el));
                default:
                    console.log("Just kill me dude!!");
            }
        }
        else if(flag == 2){ // Removing Similar Text, URL, IMG
            el.style.border = '';
        }
    }

    function pathNode(el, strPath, depth, flag) {
        let val = simplify(strPath[depth]);
        if(sim_depth >= depth){
            if( (val[0].toLowerCase() == 'html') || (val[0].toLowerCase() == 'body') || ( (el.nodeName.toLowerCase() == val[0].toLowerCase()) && (el.id == val[1]) && (class_name(el.className) == val[2]) ) ){
                if(depth == (strPath.length - 1)){
                    extract_sim(el, flag);
                }
                else {
                    depth = depth + 1;
                    let children = el.childNodes;
                    for (let i = 0; i < children.length; i++) {
                        if(children[i].nodeType == 1){
                            pathNode(children[i], strPath, depth, flag);
                        }
                    }
                }
            }
        }
        else{
            if( (el.nodeName.toLowerCase() == val[0].toLowerCase()))
            {
                if( (class_attrib[depth] == '') || (class_name(el.className) == class_attrib[depth]) ){
                    if(depth == (strPath.length - 1)){
                        extract_sim(el, flag);
                    }
                    else {
                        depth = depth + 1;
                        let children = el.childNodes;
                        for (let i = 0; i < children.length; i++) {
                            if(children[i].nodeType == 1){
                                pathNode(children[i], strPath, depth, flag);
                            }
                        }
                    }
                }
            }
        }
            
    }

    function add_column(){
        let result = new Array (curr_table.rows.length);
        for (let i = 0, row; row = curr_table.rows[i]; i++) {
            let additionSpan = 0;
            for (let j = 0, col; col = row.cells[j]; j++) {
                for (let k = 0; k < col.colSpan; k++){
                    if(index == j+ k + additionSpan){
                        col.style.border = '2px solid #090';
                        result[i] = col.textContent.replace(/^\s+|\s+$/g, "");
                    }
                }
                additionSpan += col.colSpan-1;
            }
        }
        // actionchannel.get_column(index, result);
        // Send the column data
        col_type = 0;
        return result;
    }

    function path_table(res, flag){
        let temp = res;
        while(res.nodeName.toLowerCase() !== 'html') {
            if((res.nodeName.toLowerCase() == 'table')) {
                if(flag == 0){          // Make table border red
                    if((res.style.border != '2px solid rgb(0, 153, 0)') && (res.style.border != '2px solid rgb(255, 0, 1)')){
                        res.style.border = '2px solid #f00';
                    }
                }
                else if(flag == 1){     // Remove table border 
                    if((res.style.border != '2px solid rgb(0, 153, 0)') && (res.style.border != '2px solid rgb(255, 0, 1)')){
                        res.style.border = '';
                    }
                }
                else if(flag == 2){     // On Click, Table becomes red 
                    if(curr_table == undefined || curr_table == null){
                        sendMessage({'iframe_content2': {'2': action_type}});
                    }
                    res.style.border = '2px solid #ff0001';
                    // actionchannel.get_path([cssPath(temp), getElementXPath(temp)]);
                    curr_table = res;
                }
                else if(flag == 3){     // Table column becomes red
                    if((res.style.border == '2px solid rgb(0, 153, 0)')){
                        temp.style.border = '2px solid #f00';
                    }
                }
                else if(flag == 4){
                    if ((res.style.border == '2px solid rgb(0, 153, 0)') ) {
                        // actionchannel.get_path([cssPath(temp), getElementXPath(temp)]);
                        sendMessage({'iframe_content2': {'2': action_type}});
                        let temp_node = temp;
                        while(temp_node.nodeName.toLowerCase() !== 'html') {
                            if ((temp_node.nodeName.toLowerCase() == 'td') || (temp_node.nodeName.toLowerCase() == 'th')) {
                                if(temp_node.style.border != '2px solid rgb(0, 153, 0)'){

                                    if(index != -1) {
                                        for (let i = 0, row; row = res.rows[i]; i++) {
                                            if(col_type != 0){
                                                let additionSpan = 0;
                                                for (let j = 0, col; col = row.cells[j]; j++) {
                                                    for (let k = 0; k < col.colSpan; k++){
                                                        if(index == j+ k + additionSpan){
                                                            col.style.border = '';
                                                        }
                                                    }
                                                    additionSpan += col.colSpan-1;
                                                }

                                            }
                                        }
                                    }
                                    index = temp_node.cellIndex;
                                    temp_node = temp_node.parentNode;
                                
                                    let temp_val = 0;
                                    for (let j = 0, col; col = temp_node.cells[j], j<index; j++){
                                        temp_val += col.colSpan-1;
                                    }
                                    index = index +temp_val;
                                    
                                    for (let i = 0, row; row = res.rows[i]; i++) {
                                        let additionSpan = 0;
                                        for (let j = 0, col; col = row.cells[j]; j++) {
                                            for (let k = 0; k < col.colSpan; k++){
                                                if(index == j+ k + additionSpan){
                                                    col.style.border = '2px solid #ff0001';
                                                }
                                            }
                                            additionSpan += col.colSpan-1;
                                        }
                                    }
                                    col_type = 1;
                                }
                                break;
                            }
                            else{
                                if(temp_node.nodeName == "#document"){
                                    break;
                                }
                                temp_node = temp_node.parentNode;
                            }
                        }
                    }
                }
                break;
            }
            else {
                if(res.nodeName == "#document"){
                    break;
                }
                res = res.parentNode;
            }
        }
    }

    function mimeval(res){
        if(res.getAttribute('src') !== null && res.getAttribute('src') !== undefined){
            return res.getAttribute('src');
        }
        else if(res.getAttribute('href') !== null && res.getAttribute('href') !== undefined){
            return res.getAttribute('href');
        }
        return '';
    }

    function mimetype(node){
        let url = mimeval(node);
        if(url != '' && url != '#'){
            let arr = url.split(".");
            if(arr.length !== 1 && ( arr[0] !== "" || arr.length !== 2)){
                let mime = arr.pop().toLowerCase();   
                if(mime in urls_dict){
                    return 1;
                } 
            }
        }
        return 0;
    }

    function mimeres(url){
        if(url != '' && url != '#'){
            let arr = url.split(".");
            let mime = arr.pop().toLowerCase();
            if(mime in urls_dict){
                return urls_dict[mime];
            }
        }
        return '';
    }

    function preventOnClick(node) {
        node.preventDefault();
        node.stopPropagation();
    }

    function inspectorMouseOver(e) {
        if(disable_flag != 1 && action_type != 0){
            // If it is not green or maron, then change it to red
            if (e.target.style.border == '2px dashed rgb(255, 0, 1)'){
                e.target.style.border = '2px solid #ff0001';
            }
            else if ( (e.target.style.border != '2px solid rgb(0, 153, 0)') && (e.target.style.border != '2px solid rgb(255, 0, 1)') )
            {
                if(similar_flag == 1) {
                    if(arraysEqual(cssPath(e.target).split(" "), similarPath[0].split(" "))){
                        e.target.style.border = '2px solid #ff0000';
                    }
                }else{
                    switch(action_type) {
                        case 1:
                            let data = e.target.textContent.replace(/^\s+|\s+$/g, "");
                            if (data != ''){
                                e.target.style.border = '2px solid #ff0000';
                            }
                            break;
                        case 2:
                            if (e.target.nodeName.toLowerCase() == 'a'){
                                e.target.style.border = '2px solid #ff0000';
                            }
                            else{
                                let el = e.target;
                                while(el.nodeName.toLowerCase() != 'html') {
                                    if((el.nodeName.toLowerCase() == 'a')) {
                                        e.target.style.border = '2px solid #ff0000';
                                        break;
                                    }
                                    else {
                                        el = el.parentNode;
                                    }
                                }
                            }
                            break;
                        case 3:
                            // IMAGE
                            if((e.target.nodeName.toLowerCase() == "img")){
                                e.target.style.border = '2px solid #ff0000';
                            }
                            break;
                        case 7:
                            // TABLE
                            if(table_flag == 0){        // Before selection
                                path_table(e.target, 0);
                            }
                            break;
                        case 8:
                            // COL TABLE
                            if(table_flag == 2){    // Table column extraction
                                path_table(e.target, 3);
                            }
                            break;
                        case 9:
                            // Pagination
                            e.target.style.border = '2px solid #ff0000';
                            break;
                        case 13:
                            // Click
                            if( e.target.style.border != '2px solid rgb(0, 153, 0)' && e.target.style.border != '2px solid rgb(255, 0, 1)' ){   
                                e.target.style.border = '2px solid #ff0000';
                            }
                            break;
                        case 14:
                            // Fill-in
                            if( (e.target.nodeName.toLowerCase() == 'input' || e.target.nodeName.toLowerCase() == 'textarea') && e.target.style.border != '2px solid rgb(0, 153, 0)' && e.target.style.border != '2px solid rgb(255, 0, 1)' ){  
                                e.target.style.border = '2px solid #ff0000';
                            }
                            break; 
                        case 11:
                            if(mimetype(e.target) && e.target.style.border != '2px solid rgb(0, 153, 0)') {
                                e.target.style.border = '2px solid #ff0000';
                            }
                            break;
                    }
                }
            }
        }
    }

    function inspectorMouseOut(e) {
        // xRed solid from hover then make it dashed otherwise remove
        if(action_type == 7 || action_type == 8){
            path_table(e.target, 1);
        }
        if (similar_flag == 1){
            if(e.target.style.border == '2px solid rgb(255, 0, 1)'){
                e.target.style.border = '2px dashed #ff0001';
            }
            else if ( (e.target.style.border != '2px solid rgb(0, 153, 0)') && (e.target.style.border != '2px dashed rgb(255, 0, 1)') )
            {
                e.target.style.border = '';
            }
        }
        else if ( (e.target.style.border != '2px solid rgb(0, 153, 0)') && (e.target.style.border != '2px solid rgb(255, 0, 1)'))
        {
            e.target.style.border = '';
        }
    }
    
    function inspectorOnClick(e) {
        preventOnClick(e);
        console.log("Disable_flag: ", disable_flag);
        console.log("Action_type: ", action_type);

        if(disable_flag != 1 && action_type != 0){
            if(similar_flag == 1){
                if(e.target.style.border == '2px solid rgb(255, 0, 0)' || e.target.style.border == '2px solid rgb(255, 0, 1)'){
                    let path = [cssPath(e.target), getElementXPath(e.target)];
                    let htmlTarget = document.getElementsByTagName("html")[0];
                    if(lastNode !== undefined && lastNode !== null){
                        perform_similar(htmlTarget, lastPath[0].split(" "), 0, 2);
                    }
                    else{
                        sendMessage({'iframe_content2': {'2': action_type}});
                    }
                    sim_depth = depth_count(cssPath(e.target).split(" "), similarPath[0].split(" "), 0);
                    class_attrib = [];
                    get_class_attrib(cssPath(e.target).split(" "), similarPath[0].split(" "));
                    similar_data = [];
                    pathNode(htmlTarget, similarPath[0].split(" "), 0, 1);
                    similarNode.style.border = '2px solid #090';
                    // similarNode.target.style.border = '2px solid #090';
                    lastNode = e.target;
                    lastPath = path;
                }
            }
            else {
                if(action_type != 7 && action_type != 11 && lastNode != undefined && lastNode !== null){
                    if(lastNode.style.border != '2px solid rgb(0, 153, 0)'){
                        lastNode.style.border = '';
                    }
                    // if(lastNode.target.style.border != '2px solid rgb(0, 153, 0)'){
                    //  lastNode.target.style.border = '';
                    // }
                }
                else{
                    if(login_active == 1){
                        sendMessage({'iframe_content2': {'4': action_type}});
                    }
                    else if(login_active == 0){
                        if(action_type != 7 && action_type != 11){  
                            sendMessage({'iframe_content2': {'2': action_type}});
                        }
                    }
                }
                let path = [cssPath(e.target), getElementXPath(e.target)];
                switch(action_type) {
                    case 1:     // TEXT
                        let xdata = e.target.textContent.replace(/^\s+|\s+$/g, "");
                        if (xdata != ''){
                            e.target.style.border = '2px solid #ff0001';
                            lastNode = e.target;
                            lastPath = path;
                            last_val = xdata;
                        }
                        break;
                    case 2:
                        // URL
                        if (e.target.nodeName.toLowerCase() == 'a'){
                            let xdata = getURL(e.target);
                            if (xdata != ''){
                                e.target.style.border = '2px solid #ff0001';
                                lastNode = e.target;
                                lastPath = path;
                                last_val = xdata;
                            }
                        }
                        else{
                            let el = e.target;
                            while(el.nodeName.toLowerCase() != 'html') {
                                if((el.nodeName.toLowerCase() == 'a')) {
                                    let xdata = getURL(el);
                                    if (xdata != ''){
                                        e.target.style.border = '2px solid #ff0001';
                                        // actionchannel.get_path([path, getElementXPath(e.target)]);
                                        lastNode = e.target;
                                        lastPath = path;
                                        last_val = xdata;
                                        break;
                                    }
                                }
                                else {
                                    el = el.parentNode;
                                }
                            }
                        }   
                        break;
                    case 3:
                        // IMAGE
                        if((e.target.nodeName.toLowerCase() == "img")){
                            let xdata = getImg(e.target);
                            if (xdata != ''){
                                e.target.style.border = '2px solid #ff0001';
                                lastNode = e.target;
                                lastPath = path;
                                last_val = xdata;
                            }
                        }
                        break;
                    case 7:
                        // Full Table
                        if(table_flag == 0){
                            if( (curr_table !== undefined && curr_table !== null) ){
                                if((curr_table.style.border != '2px solid rgb(0, 153, 0)')){
                                    curr_table.style.border = '';
                                }
                            }
                            path_table(e.target, 2);
                        }
                        break;
                    case 8:
                        // Column Table
                        if(table_flag == 2){
                            path_table(e.target, 4);
                        }
                        break;
                    case 9:
                        // Pagination
                        e.target.style.border = '2px solid #ff0001';
                        lastNode = e.target;
                        lastPath = path;
                        break;
                    case 13:
                        // Click
                        e.target.style.border = '2px solid #ff0001';
                        lastNode = e.target;
                        lastPath = path;
                        break;
                    case 14:
                        // Fill-in
                        e.target.style.border = '2px solid #ff0001';
                        lastNode = e.target;
                        lastPath = path;
                        break; 
                    case 11:
                        if(mimetype(e.target)) {
                            if( (lastNode != undefined && lastNode !== null) ){
                                if(lastNode.style.border != '2px solid rgb(0, 153, 0)'){
                                    lastNode.style.border = '';
                                }
                                // if(lastNode.target.style.border != '2px solid rgb(0, 153, 0)'){
                                //  lastNode.target.style.border = '';
                                // }
                            }
                            else{
                                sendMessage({'iframe_content2': {'2': action_type}});
                            }
                            if(e.target.style.border == '2px solid rgb(255, 0, 0)'){
                                // actionchannel.get_path([cssPath(e.target), getElementXPath(e.target)]);
                                e.target.style.border = '2px solid #ff0001';
                                last_val = mimeres(mimeval(e.target));
                                lastNode = e.target;
                                lastPath = path;
                            }
                        }
                        break;
                }
            }
        }
        return false;
    }


    if (document.addEventListener) {
        document.addEventListener("mouseover", inspectorMouseOver, true);
        document.addEventListener("mouseout", inspectorMouseOut, true);
        document.addEventListener("click", inspectorOnClick, true);
    } else if (document.attachEvent) {
        document.attachEvent("mouseover", inspectorMouseOver);
        document.attachEvent("mouseout", inspectorMouseOut);
        document.attachEvent("click", inspectorOnClick);
    }

})(document);