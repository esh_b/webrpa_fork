import ast
import json
import os
import random
import re
import requests
import uuid
import xlwt
from api_v1.scraping.models import Project, Execution
from base64 import decodebytes as decode64
from django.conf import settings
from urllib.parse import urlparse

data_folder = settings.SCRAPING_DATA_DIR


class data_uri():
	MIMETYPE_REGEX = r'[\w]+\/[\w\-\+\.]+'
	_MIMETYPE_RE = re.compile('^{}$'.format(MIMETYPE_REGEX))

	CHARSET_REGEX = r'[\w\-\+\.]+'
	_CHARSET_RE = re.compile('^{}$'.format(CHARSET_REGEX))

	DATA_URI_REGEX = (r'data:' + r'(?P<mimetype>{})?'.format(MIMETYPE_REGEX) + r"(?:\;name\=(?P<name>[\w\.\-%!*'~\(\)]+))?" + r'(?:\;charset\=(?P<charset>{}))?'.format(CHARSET_REGEX) + r'(?P<base64>\;base64)?' + r',(?P<data>.*)')

	_DATA_URI_RE = re.compile(r'^{}$'.format(DATA_URI_REGEX), re.DOTALL)

	def get_mimetype(self, uri):
		match = self._DATA_URI_RE.match(uri)
		return match.group('mimetype')

	def get_name(self, uri):
		match = self._DATA_URI_RE.match(uri)
		return match.group('name')

	def get_charset(self, uri):
		match = self._DATA_URI_RE.match(uri)
		return match.group('charset')

	def get_data(self, uri):
		match = self._DATA_URI_RE.match(uri)
		charset = match.group('charset')

		if match.group('base64'):
			_charset = charset or 'utf-8'
			_data = bytes(match.group('data'), _charset)
			data = decode64(_data)
		else:
			data = unquote(match.group('data'))
		return data


class ToExcel():
	def __init__(self):
		self.project_type = 0
		self.row = 1
		self.column = 0
		self.arr = []

	#Print the table values
	def print_tableCols(self, ws, cols, x, y):
		x_temp = x
		#Iterate through all the cols
		for col in cols:
			#Iterate through every value in a column
			for val in col:
				ws.write(x_temp, y, val)
				#Move down a cell (next row)
				x_temp += 1
			#After printing a column, move to the row where we started
			x_temp = x
			#Move right to next column
			y += 1
		#(y - 1) because y += 1 is done at the end of the called iteration
		return (y - 1)

	def making(self, ws, data):
		if len(data) == 1:
			maxi = 1
			data[0][:] = [act for act in data[0] if not self.determine(act)]
			action_list = data[0]

			for action in action_list:
				for key, value in action.items():
					if value is not None:
						if maxi < len(value):
							maxi = len(value)
			x = self.row
			y = self.column
			# print("Array is: ", self.arr)
			for k in range(0, maxi):
				for i in range(0, len(self.arr)):
					# print(x, y-len(self.arr)+i, self.arr[i])
					ws.write(x, y - len(self.arr) + i, self.arr[i])
				x += 1

			# y = self.column
			for action in action_list:
				x = self.row
				for key, value in action.items():
					if value is None:
						for k in range(0, maxi):
							ws.write(x, y, [])
							x += 1
					elif len(value) == 0:
						for k in range(0, maxi):
							ws.write(x, y, [])
							x += 1
					elif(isinstance(value[0], list)):
						y = self.print_tableCols(ws, value, x, y)
					elif len(value) == 1:
						for k in range(0, maxi):
							ws.write(x, y, value[0])
							x += 1
					else:
						for val in value:
							ws.write(x, y, val)
							x += 1
				y += 1
			self.row += maxi

		else:

			dim = []
			for action in data[0]:
				for key in action:
					dim.append(action[key])

			self.column += 2 if self.project_type != 4 and self.project_type != 5 else 1
			for i in range(0, len(data[1])):
				self.arr.append(dim[0][i])
				if self.project_type != 4 and self.project_type != 5:
					self.arr.append(dim[1][i])
				ws = self.making(ws, data[1][i])
				self.arr = self.arr[:-2] if self.project_type != 4 and self.project_type != 5 else self.arr[:-1]
			self.column -= 2 if self.project_type != 4 and self.project_type != 5 else 1

		return ws

	def headline(self, ws, data, y):
		for action in data[0]:
			for key in action:
				if key != "TRWEIDCVEELVET":
					# print("Key is: ", key)
					ws.write(0, y, key)
					y += 1
		if len(data) != 1:
			ws = self.headline(ws, data[1][0], y)
		return ws

	def determine(self, action: dict):
		if "TRWEIDCVEELVET" in action:
			return True
		return False

	def main_json(self, filepath):
		data = open(filepath).read()
		data = json.loads(data)
		#data = ast.literal_eval(data)
		self.project_type = data[0]

		data = data[1]
		book = xlwt.Workbook()

		ws = book.add_sheet("Sheet1")
		ws = self.headline(ws, data, 0)
		ws = self.making(ws, data)
		return book



def process(extraction_type, page, ws, flag):
	length = 1

	for action in range(0, len(page)):

		for key in page[action]:

			if flag == 0 and extraction_type == 5:

				ws.write(0, action + 2, key)

			elif flag == 0 and extraction_type == 4:

				ws.write(0, action + 1, key)

			if len(page[action][key]) > 1:
				length = len(page[action][key])

	return length, ws


def produce_excel(uid, pid, eid):
	project = Project.objects.get(id=pid)

	execution = Execution.objects.get(task_id=eid)

	filepath = os.path.join(data_folder + uid, pid, eid + '.json')

	op = os.path.join(data_folder + str(uid), str(pid), str(project.name) + '.xls')

	return data_to_excel(filepath, op)



def data_to_excel(filepath, op):
	try:
		book = ToExcel().main_json(filepath)
		book.save(op)
		return [book, op]

	except FileNotFoundError:
		print('File Not Found...!!')



class Download():

	def __init__(self):

		self.folder_list = []
		self.data_uri_processor = data_uri()

		self.mime_extension = {
			"audio/aac": "aac",
			"application/octet-stream": "bin",
			"image/bmp": "bmp",
			"application/x-bzip": "bz",
			"application/x-bzip2": "bz2",
			"application/x-csh": "csh",
			"text/css": "css",
			"text/csv": "csv",
			"application/msword": "doc",
			"application/vnd.openxmlformats-officedocument.wordprocessingml.document": "docx",
			"image/gif": "gif",
			"text/html": "html",
			"image/x-icon": "ico",
			"text/calendar": "ics",
			"application/java-archive": "jar",
			"image/jpeg": ".jpeg",
			"application/javascript": "js",
			"application/json": "json",
			"video/mpeg": ".mpeg",
			"image/png": "png",
			"application/pdf": "pdf",
			"application/vnd.ms-powerpoint": "ppt",
			"application/vnd.openxmlformats-officedocument.presentationml.presentation": "pptx",
			"application/x-rar-compressed": "rar",
			"image/svg+xml": "svg",
			"application/x-tar": "tar",
			"image/tiff": "tif",
			"text/plain": ".txt",
			"audio/wav": "wav",
			"audio/webm": "weba",
			"video/webm": "webm",
			"image/webp": "webp",
			"application/xhtml+xml": "xhtml",
			"application/vnd.ms-excel": "xls",
			"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": "xlsx",
			"application/xml": "xml",
			"application/zip": "zip",
			"application/x-7z-compressed": "7z",
		}

	def check_file(self, name, val):

		xname = name.split(".")

		xname = (".").join(xname[:-1]) + '(' + str(val) + ').' + xname[-1]

		xname = name if val == 0 else name if len(name.split(".")) == 1 else xname

		if not os.path.exists(xname):

			return xname

		else:

			return self.check_file(name, val + 1)

	def check_dir(self, name, val):

		xname = name if val == 0 else name + '(' + str(val) + ')'

		if not os.path.exists(xname):

			return xname

		else:

			return self.check_dir(name, val + 1)

	def create_dir(self, folder_name: str):

		folder_name = self.check_dir(folder_name, 0)

		os.makedirs(folder_name)

		return folder_name

	# def get_file_extension(self, mimetype):
	# 	res = mime.Types[mimetype]
	# 	if(res):
	# 		return res[0].extensions[0]
	# 	else:
	# 		return None

	def get_http_content(self, url):
		r = requests.get(url)
		mimetype = r.headers['content-type']        
		filename = uuid.uuid4().hex
		try: 
			ext = self.mime_extension[mimetype]
			return filename + "." + ext, r.content    
		except:
			return filename, r.content

	def get_datauri_content(self, url):
		data = self.data_uri_processor.get_data(url)
		mimetype = self.data_uri_processor.get_mimetype(url)

		filename = uuid.uuid4().hex
		try: 
			ext = self.mime_extension[mimetype]
			return filename + "." + ext, data    
		except:
			return filename, data

	def get_cur_action_dir_name(self, path, url):
		if url.startswith("data:"):
			mimetype = self.data_uri_processor.get_mimetype(url)
		else:
			with requests.get(url) as r:
				mimetype = r.headers['content-type']

		#Check
		if mimetype:
			dir_name = path + mimetype.replace("/", "_")
		else:
			return ""
		return dir_name


	def download_folder(self, data, path):
		if len(data) == 1:
			for act in data[0]:
				if "TRWEIDCVEELVET" in act:
					dir_name = ''
					if len(act["TRWEIDCVEELVET"]) != 0:
						dir_name = self.get_cur_action_dir_name(path, act["TRWEIDCVEELVET"][0])
						self.create_dir(dir_name)
					
					for url in act["TRWEIDCVEELVET"]:
						if(url.startswith("data:")):
							filename, content = self.get_datauri_content(url)
						else:
							filename, content = self.get_http_content(url)                        
						with open(dir_name + '/' + filename, 'wb') as f:
						   f.write(content)
						
		else:
			for key, value in data[0][0]:
				if value is not None:
					for val in value:
						val = path + val
						self.folder_list.append(val)

			for i in range(0, len(data[1])):
				self.download_folder(data[1][i], self.folder_list[i])
