from . import views
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.urls.conf import path
from django.conf.urls.i18n import i18n_patterns


app_name = 'dashboard'

urlpatterns = [
    path(r'home/', views.index, name = 'index'),
    path(r'upgrade/', views.upgrade, name='upgrade'),
    path(r'notification_settings/', views.notification_setting, name='notification_setting'),
    path(r'delete_account/', views.delete_account, name='delete_account'),
    url(r'^change_password/$', views.change_password, name='change_password'),
    url(r'^project/list/$', views.project_list, name='project_list'),
    url(r'^project/list/i18n/', include('django.conf.urls.i18n')),
    #path(r'project/schedule/', views.project_schedule, name = 'project_schedule'),
    path(r'settings/', views.settings, name='settings'),
    path(r'notification/change/', views.notification_change, name='notification_change'),
    url(r'account_setting_basic/$', views.account_setting_basic, name='account_setting_basic'),
    url(r'profile_change/$', views.profile_change, name='profile_change'),
    url(r'profile_change_finish/$', views.profile_change_finish, name='profile_change_finish'),
    url(r'password_change/$', views.PasswordChange.as_view(), name='password_change'),
    url(r'password_change_finish/$', views.password_change_finish, name='password_change_finish'),
    url(r'agreement_payment/$', views.agreement_payment, name='agreement_payment'),
    url(r'payment_change_bank/$', views.payment_change_bank, name='payment_change_bank'),
    url(r'payment_change_credit/$', views.payment_change_credit, name='payment_change_credit'),
    url(r'payment_change_finish/$', views.payment_change_finish, name='payment_change_finish'),
    url(r'plan_change/$', views.plan_change, name='plan_change'),
    url(r'plan_change_confirm/$', views.plan_change_confirm, name='plan_change_confirm'),
    url(r'plan_change_finish/$', views.plan_change_finish, name='plan_change_finish'),
    url(r'product_download/$', views.product_download, name='product_download'),
    url(r'system_error/$', views.system_error, name='system_error'),
    url(r'cancel/$', views.cancel, name='cancel'),
    url(r'cancel_finish/$', views.cancel_finish, name='cancel_finish'),
    path(r'ajax/$', views.managefiletype, name='managefiletype'),
    path(r'ajax1/$', views.edit_project, name='edit_project'),
    path(r'ajax2/$', views.edit_keyword, name='edit_keyword'),
    path(r'update/profile/', views.update_profile, name='update_profile'),
    path(r'project/data_order/', views.data_order, name='data_order'),
    path(r'project/data_order_form/', views.data_order_form, name='data_order_form'),
    path(r'project/data_order_confirm/', views.data_order_confirm, name='data_order_confirm'),
    path(r'project/data_order_done/', views.data_order_done, name='data_order_done'),

    #path(r'ScheduleForm/', views.schedule_form, name = 'schedule_form'),
    url(r'run/(?P<pid>[^/]+)$', views.run, name='run'),
    path(r'zip_data/(?P<pid>[^/]+)$', views.zip_data, name='zip_data'),
    path(r'delete/(?P<str:cname>[^/]+)$', views.delete_category, name='delete_category'),
    path(r'delete/(?p<pid>[^/]+)$', views.delete_project, name='delete_project'),
    path(r'delete_execution/(?P<pid>[^/]+)/(?P<eid>[^/]+)$', views.delete_execution, name='delete_execution'),
    url(r'product/$', views.product_type, name='product_type'),
    url(r'project/details/(?P<pid>[^/]+)$', views.project_details, name='project_details'),
    url(r'get_project/', views.get_project, name='get_project'),
    url(r'auto_refresh/', views.auto_refresh, name='auto_refresh'),

    url(r'email_change/$', views.email_change, name='email_change'),

    #---------Payment-------------------------------------------
    url(r'^cart$', views.ShowCartView.as_view(), name='cart'),
    #url(r'^create-payment$', views.CreatePaymentView.as_view(), name='create-payment'),
    #url(r'^execute-payment$', views.ExecutePaymentView.as_view(), name='execute-payment'),
    url(r'upgrade/payment', views.ExecutePaymentView, name='ExecutePaymentView'),
]
