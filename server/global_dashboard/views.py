import os
import ast
import json
import xlwt
import uuid
import csv
import xlrd
import logging
import shutil
import zipfile
import payjp
import feedparser
from datetime import datetime, timedelta
from time import sleep
from io import BytesIO
from dashboard.convert import produce_excel, data_to_excel
from accounts.models import Profile, Data_Order
from accounts.forms import UserChangeForm, ProfileChangeForm
from sales.models import SubscriptionPlan, UserSubscription
#from api_v1.scheduling.models import Schedule
from api_v1.scraping.models import BehindLogin
from api_v1.scraping.models import Project, Execution
from api_v1.scraping.tasks import *
from collections import namedtuple, OrderedDict
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, PasswordChangeForm
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordChangeDoneView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from accounts.models import User
from django.core import serializers
from django.db import connection
from django.http import JsonResponse, Http404
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, reverse
from django.template.loader import render_to_string
from django.utils import timezone
from django.urls import reverse_lazy
from django.views.static import serve
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import cache_control
from django import forms
from api_v1.feedback.models import Feedback

from schedule.views import update_or_create_schedule
from schedule.views import get_next_run_cron

data_folder = settings.SCRAPING_DATA_DIR
project_folder = settings.SCRAPING_PROJECT_DIR
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def global_home(request):
    projects = Project.objects.filter(user=request.user).order_by("-date_created")[:5]
    num_proj = Project.objects.filter(user=request.user).count()

    totalprojects = num_proj
    totalexecutions = Execution.objects.filter()

    total_size = 0
    prj = Project.objects.filter(user=request.user)
    for project in prj:
        total_size = total_size + int(project.project_size or 0)

    g = 1024 * 1024 * 1024
    m = 1024 * 1024
    k = 1024

    show_size = round(total_size / m, 1)  # MB size
    unit = "MB"

    total_size_limit = 200;  # 200M
    percent = round(show_size / total_size_limit * 100, 3)

    num_proj_limit = 100;  # temp 100 project limit
    storage_ratio = round(totalprojects / num_proj_limit, 1) * 10;
    box_num = range(1, 11)
    blog_num = 1
    max_blog_num = 5
    blog_info = [['お得に買い物をするために！主要ECサイトを一気に比較', 'https://services.sms-datatech.co.jp/pig-data/2018/11/29/try11/',
                  '2018/11/29'], ['インバウンド対策とは？ 厳選インバウンドアプリの事例5選',
                                  'https://services.sms-datatech.co.jp/pig-data/2018/11/22/inboundapp/',
                                  '2018/11/22'],
                 ['企業ゆるキャラランキング2018徹底調査！', 'https://services.sms-datatech.co.jp/pig-data/2018/11/20/try10/',
                  '2018/11/20'], ['無料RPAツール【PigData】 ver.1.0.2リリース',
                                  'https://services.sms-datatech.co.jp/pig-data/2018/11/07/update1/', '2018/11/07'],
                 ['3分でわかる！RPAとAIの違いとは？', 'https://services.sms-datatech.co.jp/pig-data/2018/11/06/rpanai/',
                  '2018/11/06']]

    return render(request, 'global-dashboard.html',
                  {'user': request.user, 'totalprojects': totalprojects, 'unit': unit, 'projects': projects,
                   'num_proj': num_proj, 'percent': percent, 'storage_ratio': storage_ratio, 'box_num': box_num,
                   'total_size_limit': total_size_limit, 'show_size': show_size, 'blog_info': blog_info})


@login_required
def new_project(request):
    return render(request, 'new_project.html')

def get_project_schedule_params(periodic_task):
    """Function to return the schedule params (required to print in templates)
    
    Args:
        periodic_task (TYPE): Description
    
    Returns:
        TYPE: Description
    """
    params = {}
    if(periodic_task is not None):
        params['next_dt'] = get_next_run_cron(periodic_task.crontab)

        if(periodic_task.interval is not None):
            params['sched_type'] = '1'
            params['every'] = periodic_task.interval.every
        elif(periodic_task.crontab is not None):
            if(periodic_task.crontab.day_of_month == '*'):
                params['sched_type'] = '2'
            else:
                params['sched_type'] = '3'
    else:
        params['sched_type'] = '0'
    return params

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def global_project_list(request):
    print (request.body)

    #Update project schedule
    try:
        if(request.POST.get("scheduleType")):
            pid = request.POST.get("pid")
            sched_type = request.POST.get("scheduleType")
            sched_value = request.POST.get('day', None) or request.POST.get('day-name', None) or request.POST.get('month', None)
            sched_time = request.POST.get("appt", None)

            update_or_create_schedule(request.user.id, pid, sched_type, sched_value, sched_time)
        else:
            print("ELSE:", )
    except Exception as e:
        print("EXCEPTION:", e)
        return HttpResponse(status=500)

    # Project
    num_proj = Project.objects.filter(user=request.user).count()
    projects = OrderedDict()

    if request.POST.get("sort"):
        all_proj = Project.objects.filter(user=request.user).order_by(request.POST.get("sort"))
    else:
        all_proj = Project.objects.filter(user=request.user).order_by("-date_created")

    for project in all_proj:
        projects[project.id] = project
        project.executions = []
        # project.schedule = Schedule.objects.filter(project=project)

        #Project schedule string in human-readable format
        project.sched_params = get_project_schedule_params(project.periodic_task)

    for execution in Execution.objects.filter(project__in=projects).order_by('-date_executed'):
        project = projects[execution.project.id]
        project.executions.append(execution)

    # notification
    if request.POST.get("notification"):
        user = User.objects.get(pk=request.user.id)
        user.profile.notification = request.POST.get("notification")
        notification_list = request.POST.getlist("notification")
        pid = request.POST.get("pid")
        project = Project.objects.get(id=pid)
        if request.POST.get("notification"):
            # seleted option
            for noti_no in notification_list:
                noti_no = int(noti_no)
                if noti_no == 1:
                    project.mail_alert = 1
                elif noti_no == 2:
                    project.data_alert = 1
                elif noti_no == 3:
                    project.keyword_alert = 1
            # unseleted some options
            unseleted_list = [1, 2, 3]
            for noti_no in notification_list:
                unseleted_list.remove(int(noti_no))
            if len(unseleted_list) > 0:
                for noti_no in unseleted_list:
                    if noti_no == 1:
                        project.mail_alert = 0
                    elif noti_no == 2:
                        project.data_alert = 0
                    elif noti_no == 3:
                        project.keyword_alert = 0
        # seleted no options
        elif notification_list == []:
            project.mail_alert = 0
            project.data_alert = 0
            project.keyword_alert = 0
        project.save()
        return HttpResponseRedirect('/global/projects/')

    return render(request, 'download_data.html',
                {'user': request.user, 'num_proj': num_proj, 'projects': projects, 
                 'all_projects': all_proj,
                })

@login_required()
@csrf_exempt
def product_type(request: HttpRequest, uid, pid, eid, ft):
    # filestr = request.POST['filetype']
    # uid, pid, eid, ft = filestr.split("#")
    # print(ft)
    print(request.body)
    if ft == '1':
        res = produce_excel(uid, pid, eid)
        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(task_id=eid)
        name = str(project.name) + '_' + str(execution.date_executed).replace(':', '-').replace(' ', '_') + '.xlsx'
        content_str = 'attachment; filename=' + name
        response = HttpResponse(content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = content_str
        res[0].save(response)
        return response
    elif ft == '2':
        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(task_id=eid)
        name = str(project.name) + '_' + str(execution.date_executed).replace(':', '-').replace(' ', '_') + '.csv'
        content_str = 'attachment;filename=' + name

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = content_str
        writer = csv.writer(response)

        res = produce_excel(uid, pid, eid)
        wb = xlrd.open_workbook(res[1], formatting_info=True)
        for s in wb.sheets():
            sh = wb.sheet_by_name(s.name)
            for rownum in range(sh.nrows):
                writer.writerow(sh.row_values(rownum))
        return response
    elif ft == '3':
        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(task_id=eid)
        name = str(project.name) + '_' + str(execution.date_executed).replace(':', '-').replace(' ', '_') + '.txt'
        content_str = 'attachment;filename=' + name

        response = HttpResponse(content_type='text/plain')
        response['Content-Disposition'] = content_str

        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(task_id=eid)
        filepath = data_folder + uid + '/' + pid + '/' + eid + '.json'
        data = json.loads(open(filepath).read())
        response.content = str(data)
        return response
    elif ft == '4':
        # folder_path = os.path.join('/home/nitish/Desktop/pigdata-white/web-rpa/server/data', uid, pid, eid, '')
        folder_path = os.path.join(data_folder, uid, pid, eid, '')
        # folder_path = folder_path + '/'
        folder_name = pid + '_' + eid
        print("folder_path is: ", folder_path)
        s = BytesIO()
        zf = zipfile.ZipFile(s, "w")
        if os.path.exists(folder_path + folder_name):

            for root, dirs, files in os.walk(folder_path + folder_name):
                for file in files:
                    zf.write(os.path.join(root, file), '/'.join(os.path.join(root, file).split('/')[11:]))
            zf.close()
        else:
            print("Error: File not found!!")

        name = folder_name + '.zip'
        content_str = 'attachment;filename=' + name
        response = HttpResponse(s.getvalue(), content_type="application/x-zip-compressed")
        response['Content-Disposition'] = content_str

        total_size = 0
        start_path = data_folder + uid + '/' + pid + '/'
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        print("-------")
        print(total_size)
        prjct = Project.objects.get(id=pid)
        prjct.project_size = total_size
        prjct.save()

        return response
    else:
        print("Type not recognized")
        return HttpResponseRedirect('/global/home/', [pid])

    return HttpResponseRedirect('/global/home/', [pid])

@login_required()
def global_delete_project(request: HttpRequest, pid):
    project = Project.objects.filter(id=pid)
    rows = Execution.objects.filter(project=project[0])
    for r in rows:
        r.delete()

    #Schedule.objects.filter(project=project[0]).delete()
    BehindLogin.objects.filter(project_id=pid).delete()
    Project.objects.filter(id=pid).delete()

    dirname = str(request.user.id)
    filename = str(pid)
    shutil.rmtree(os.path.join(data_folder, dirname, filename))
    os.remove(os.path.join(project_folder, dirname, filename + '.json'))
    return redirect('global_dashboard:all_projects')

@login_required()
def run_now(request: HttpRequest, pid=None):
    task_scrape.delay(request.user.id, pid)
    return HttpResponseRedirect('/global/projects/')

@csrf_exempt
def post_feedback(request: HttpRequest):
    print('\n==========feedback=====\n')
    try:
        start_url = request.POST.get('url_start')
        browser = request.POST.get('web')
        comment = request.POST.get('messag_send')

        try:
            new_feedback = Feedback(user=request.user, start_url=start_url, browser=browser,
                                    comment=comment, timestamp=datetime.now())
            new_feedback.save()
            status_code = 200
        except:
            status_code = 500

        return JsonResponse(status_code, safe=False)
    except HTTPException as ex:
        return ex.as_response()
    except Exception:
        logger = logging.getLogger(__name__)
        logger.exception('Internal server error')
        return HttpResponse(status=500)

@csrf_exempt
def instructions(request: HttpRequest):
    return render(request, 'instructions.html')
