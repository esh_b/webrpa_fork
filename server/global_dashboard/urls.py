from . import views
from . import views as core_views
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.urls import path

app_name = 'global_dashboard'

urlpatterns = [
    url(r'global/home/$', core_views.global_home, name='index'),
    url(r'global/new/project$', core_views.new_project, name='new_project'),
    url(r'global/projects/$', core_views.global_project_list, name='all_projects'),
    url(r'ajax/(?P<uid>[^/]+)/(?P<pid>[^/]+)/(?P<eid>[^/]+)/(?P<ft>[^/]+)$', core_views.product_type, name='product_type'),
    url(r'running/(?P<pid>[^/]+)$', views.run_now, name='run_now'),
    path(r'global/delete/(?p<pid>[^/]+)$', views.global_delete_project, name='global_delete_project'),
    url(r'^project/list/i18n/', include('django.conf.urls.i18n')),
    url(r'global/feedback/$', core_views.post_feedback, name='feedback'),
    url(r'global/instructions/$', core_views.instructions, name='instructions'),
]
