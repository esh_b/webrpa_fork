#PIGDATA

PigData is a scraping tool to extract unstructured data from different website to store in structured format. It has 
mainly two sides: Client Tool and Server Side.   
Client GUI Tool: It is build up on PyQT Framework with intermediate uses of javascript between a browser to client program. 

Server Side: It is design with Django Framework with MySql as backend database. 
Basic assumption at Server: 
- The server is stateless. That is, the command and parameters to the server fully specifies
what to do. No session state held at server side.
- No asynchronous request/notice from the server to a client. Always client calls the server.

### Class Definition
Class Procedure: This is an abstraction of the created procedure or a sequence of actions.
Instances are created by GUI or by the procedure library.
- Project
- Action


### Main Modules
- accounts
- dashboard
- email_sys
- sales
- api_v1: API for communication between client tool and server, which is an equivalent of the server REST API.



#### PigData run @Development Server
- redirect to source directory

`cd /home/nitish/Desktop/aws/developing/web-rpa/server/`
- set the environment variable

`source /home/nitish/Desktop/pigdata-white/pigdata-venv/bin/activate`
- start the server

`python3 manage.py runserver 0.0.0.0:9000`

##### Database settings on Development server
```py
DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'aws-pigdata',
        'HOST': '192.168.20.152',
        'USER': 'root',
        'PASSWORD': 'nitish123',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    }
}

SCRAPING_DATA_DIR = '/home/nitish/Desktop/aws/data/'
SCRAPING_PROJECT_DIR = '/home/nitish/Desktop/aws/projects/'
```
##### Celery
- open new terminal and redirect to source directory.
- run the command
`celery -A web_rpa worker -B -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler`

 

#### PigData Run @AWS
- Key Points:
    - Use 'pig-data-test-template' for any testing of pigdata on aws. 
    - Also use the clone database of pigdata, 'pidata_clone' for any migrating testing. 


##### PigData Server
- [AWS Console](https://843547657088.signin.aws.amazon.com/console)
- ID: ******
- Password: ******

##### SSH PigData

```sh
ssh -i pig-data.pem ec2-user@<instance-id>
```
 
###### Step 1 
- First check if the developing branch working perfectly then merge it with the master branch. 
- Check the web_rpa/settings.py file and do the following changes:

uncomment local settings
    LOCAL SETTINGS
        
        \#try:
        \#from .local_settings import *
        \#except ImportError:
        \#pass
    
          
check all initial urls again
    
    LOGIN_URL = 'accounts:home'
    LOGIN_REDIRECT_URL = 'dashboard:index'
    LOGOUT_REDIRECT_URL = 'accounts:logout'

check the web_rpa/local_settings file

check database name and password. 
##### /srv/www/web-rpa/server/web_rpa/local_settings.py

```py
DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'pig_data',
        'HOST': 'rds-aurora.cw2qkc1hesa1.ap-southeast-2.rds.amazonaws.com',
        'USER': 'pig_data',
        'PASSWORD': 'nbS5htqe41KznS2r',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    }
}
```

check the projects and data url in `web_rpa/settings.py`.
    
    SCRAPING_DATA_DIR  = os.path.join(BASE_DIR, '/var/lib/pigdata/data/')
    SCRAPING_PROJECT_DIR = os.path.join(BASE_DIR, '/var/lib/pigdata/projects/')
check all other hardcoded path if anyone exist. 

###### Step 2
- Create a version tag from updated master branch.
    - git tag (to check all existing tag)
    - git tag -a v1.0.4 -m "pigdata 1.0.4<new changes>" (to create a new tag)

##### From here on either follow step 3 (easy) or step 4 (recommended)
###### Step 3
  - SSH connection to the running pigdataserver using the secret key.
    `ssh -i <pigdata key filepath> ec2-user@<instance id>`
  - Check the global address of the instance from the EC2 management screen.
  - User name is `ec2-user`
  - After login, Become root with the following shell command
    `sh >> sudo -i`
- Redirect to the pigdata source directory
`cd /src/www/web_rpa/server`

- Stop the httpd and celery services
`systemctl stop httpd celery`

- Run the command 
`systemctl start auto-deploy`
    - if the above commad desnt work, have to do it manually. 
    - Update the code from git
`git checkout master`
`git pull`
`git checkout <new version>`

- start the services again
`systemctl start httpd celery`

##### Updating programs.

```sh
systemctl stop httpd celery
systemctl start auto-deploy
systemctl start httpd celery
```

## Django and Apache Configuration
### hierarchic structure
|--- var<br>
    |--- www<br>
        |--- master<br>
        |--- web-rpa-developing<br>

Once you move into /var/www/(branchname), you can modify code in the branch.

(ex)
If you want to modify your code in web-rpa-developing, please move into /var/www/web-rpa-developing/

This is alias. You can type below command-line.
`master` (=`cd /var/www/master`)
`develop`(=`cd /var/www/web-rpa-developing`)



### Apache and Celery

# Apache
`systemctl (status/start/restart/stop) httpd`

configuration file is: /etc/httpd/conf/httpd.conf

if your code doesn't update, please type `systemctl restart httpd`


# Error log
You can see the error log in /var/log/httpd/error_log

Type like this:
`sudo cat /var/log/httpd/error_log` or `sudo tail -f /var/log/httpd/error_log`

# Celery
`systemctl (status/start/restart/stop) celery`

configuration file is:
/etc/systemd/system/celery.service
/etc/conf.d/celery

You can see the celery log in /var/log/celery/
celery log file status and more details can be seen by using `systemctl status celery`
(ex)
`tail -f /var/log/celery/worker1-1.log`


You can check whether worker is working or not using below command:
`systemctl (status/start/restart/stop) rabbitmq-server`



### How to add virtual host

If you want to add your branch, please do like this:

1. Add below code in C:\Windows\System32\drivers\etc\hosts(Windows) OR /etc/hosts(Mac) (Local, not 192.168.20.161's /etc/hosts)

`192.168.20.161 master.pigdata develop.pigdata hanamura.pigdata (branchname).pigdata`

2. Add below code in /etc/httpd/conf.d/django.conf

```
<VirtualHost *:80>
    ServerName (branchname).pigdata
    VirtualDocumentRoot "/var/www/(branchname)/web-rpa/server"

    WSGIDaemonProcess (branchname).pigdata python-home=/var/www/html/web-rpa/env python-path=/var/www/(branchname)/web-rpa/server:/var/www/html/web-rpa/env/lib/python3.6/site-packages
    WSGIProcessGroup (branchname).pigdata
    WSGIScriptAlias / /var/www/(branchname)/web-rpa/server/web_rpa/wsgi.py process-group=(branchname).pigdata
</VirtualHost>
```
Please don't forget to type `systemctl restart httpd`.

3. Add new directory in /var/www and git clone

`mkdir (branchname)`

in /var/www/(branchname):
`git clone -b (branchname) git@gitlab.com:sdt-bizdev/web-rpa.git`

Warning: please use your own user, not use root user.

4. Access http://(branchname).pigdata

Now, you can already see all branches' output page.

[branch]
master:             http://master.pigdata/
web-rpa-developing: http://develop.pigdata/
web-rpa-hanamura:   http://hanamura.pigdata/
(branchname):       http://(branchname).pigdata/

